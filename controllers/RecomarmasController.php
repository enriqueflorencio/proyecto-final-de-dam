<?php

namespace app\controllers;

use Yii;
use app\models\Recomarmas;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RecomarmasController implements the CRUD actions for Recomarmas model.
 */
class RecomarmasController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Recomarmas models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Recomarmas::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Recomarmas model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Recomarmas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Recomarmas();
        $modelUsuario = \app\models\Usuarios::find()->all();
        $modelArma = \app\models\Armas::find()->all();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'modelUsuario' => $modelUsuario,
            'modelArma' => $modelArma,
        ]);
    }

    /**
     * Updates an existing Recomarmas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelUsuario = \app\models\Usuarios::find()->all();
        $modelArma = \app\models\Armas::find()->all();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'modelUsuario' => $modelUsuario,
            'modelArma' => $modelArma,
        ]);
    }

    /**
     * Deletes an existing Recomarmas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['todaspuntuacionesarmas']);
    }

    /**
     * Finds the Recomarmas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Recomarmas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Recomarmas::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
     public function actionTodaspuntuacionesarmas(){
        $dataProvider = new ActiveDataProvider([
            'query' => Recomarmas::find(),
        ]);
        
        return $this->render('todasarmas', [
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionMediaarmas(){
        $dataProvider = new ActiveDataProvider([
            'query' => Recomarmas::find() -> groupBy('id_arma')
                                              -> select('id_arma, avg(puntuación) as mediapuntuación')
                                              -> orderBy(['mediapuntuación' => SORT_DESC]),
        ]);
        
        return $this->render('mediaarmas', [
            'dataProvider' => $dataProvider,
        ]);
    }
}
