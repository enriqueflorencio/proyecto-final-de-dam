<?php

namespace app\controllers;

use Yii;
use app\models\Recomclases;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RecomclasesController implements the CRUD actions for Recomclases model.
 */
class RecomclasesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Recomclases models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Recomclases::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Recomclases model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Recomclases model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Recomclases();
        $modelUsuario = \app\models\Usuarios::find()->all();
        $modelClase = \app\models\Clases::find()->all();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'modelUsuario' => $modelUsuario,
            'modelClase' => $modelClase,
        ]);
    }

    /**
     * Updates an existing Recomclases model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelUsuario = \app\models\Usuarios::find()->all();
        $modelClase = \app\models\Clases::find()->all();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'modelUsuario' => $modelUsuario,
            'modelClase' => $modelClase,
        ]);
    }

    /**
     * Deletes an existing Recomclases model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['todaspuntuacionesclases']);
    }

    /**
     * Finds the Recomclases model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Recomclases the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Recomclases::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
    public function actionTodaspuntuacionesclases(){
        $dataProvider = new ActiveDataProvider([
            'query' => Recomclases::find(),
        ]);
        
        return $this->render('todasclases', [
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionMediaclases(){
        $dataProvider = new ActiveDataProvider([
            'query' => Recomclases::find() -> groupBy('id_clase')
                                              -> select('id_clase, avg(puntuación) as mediapuntuación')
                                              -> orderBy(['mediapuntuación' => SORT_DESC]),
        ]);
        
        return $this->render('mediaclases', [
            'dataProvider' => $dataProvider,
        ]);
    }
}
