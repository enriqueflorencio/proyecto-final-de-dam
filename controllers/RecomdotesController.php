<?php

namespace app\controllers;

use Yii;
use app\models\Recomdotes;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RecomdotesController implements the CRUD actions for Recomdotes model.
 */
class RecomdotesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Recomdotes models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Recomdotes::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Recomdotes model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Recomdotes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Recomdotes();
        $modelUsuario = \app\models\Usuarios::find()->all();
        $modelDote = \app\models\Dotes::find()->all();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'modelUsuario' => $modelUsuario,
            'modelDote' => $modelDote,
        ]);
    }

    /**
     * Updates an existing Recomdotes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelUsuario = \app\models\Usuarios::find()->all();
        $modelDote = \app\models\Dotes::find()->all();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'modelUsuario' => $modelUsuario,
            'modelDote' => $modelDote,
        ]);
    }

    /**
     * Deletes an existing Recomdotes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['todaspuntuacionesdote']);
    }

    /**
     * Finds the Recomdotes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Recomdotes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Recomdotes::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
        public function actionTodaspuntuacionesdotes(){
        $dataProvider = new ActiveDataProvider([
            'query' => Recomdotes::find(),
        ]);
        
        return $this->render('todasdotes', [
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionMediadotes(){
        $dataProvider = new ActiveDataProvider([
            'query' => Recomdotes::find() -> groupBy('id_dote')
                                              -> select('id_dote, avg(puntuación) as mediapuntuación')
                                              -> orderBy(['mediapuntuación' => SORT_DESC]),
        ]);
        
        return $this->render('mediadotes', [
            'dataProvider' => $dataProvider,
        ]);
    }
}
