<?php

namespace app\controllers;

use Yii;
use app\models\Recomobjetos;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RecomobjetosController implements the CRUD actions for Recomobjetos model.
 */
class RecomobjetosController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Recomobjetos models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Recomobjetos::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Recomobjetos model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Recomobjetos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Recomobjetos();
        $modelUsuario = \app\models\Usuarios::find()->all();
        $modelObjeto = \app\models\Objetos::find()->all();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'modelUsuario' => $modelUsuario,
            'modelObjeto' => $modelObjeto,
        ]);
    }

    /**
     * Updates an existing Recomobjetos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelUsuario = \app\models\Usuarios::find()->all();
        $modelObjeto = \app\models\Objetos::find()->all();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'modelUsuario' => $modelUsuario,
            'modelObjeto' => $modelObjeto,
        ]);
    }

    /**
     * Deletes an existing Recomobjetos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['todaspuntuacionesobjetos']);
    }

    /**
     * Finds the Recomobjetos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Recomobjetos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Recomobjetos::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
    public function actionTodaspuntuacionesobjetos(){
        $dataProvider = new ActiveDataProvider([
            'query' => Recomobjetos::find(),
        ]);
        
        return $this->render('todasobjetos', [
            'dataProvider' => $dataProvider,
        ]);
    }
    
        public function actionMediaobjetos(){
            $dataProvider = new ActiveDataProvider([
                'query' => Recomobjetos::find() -> groupBy('id_objeto')
                                                  -> select('id_objeto, avg(puntuación) as mediapuntuación')
                                                  -> orderBy(['mediapuntuación' => SORT_DESC]),
            ]);

            return $this->render('mediaobjetos', [
                'dataProvider' => $dataProvider,
            ]);
        }
}
