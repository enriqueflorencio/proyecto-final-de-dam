<?php

namespace app\controllers;

use Yii;
use app\models\Recomtrasfondos;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RecomtrasfondosController implements the CRUD actions for Recomtrasfondos model.
 */
class RecomtrasfondosController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Recomtrasfondos models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Recomtrasfondos::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Recomtrasfondos model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Recomtrasfondos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Recomtrasfondos();
        $modelUsuario = \app\models\Usuarios::find()->all();
        $modelTrasfondo = \app\models\Trasfondo::find()->all();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'modelUsuario' => $modelUsuario,
            'modelTrasfondo' => $modelTrasfondo,
        ]);
    }

    /**
     * Updates an existing Recomtrasfondos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelUsuario = \app\models\Usuarios::find()->all();
        $modelTrasfondo = \app\models\Trasfondo::find()->all();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'modelUsuario' => $modelUsuario,
            'modelTrasfondo' => $modelTrasfondo,
        ]);
    }

    /**
     * Deletes an existing Recomtrasfondos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['todaspuntuacionestrasfondos']);
    }

    /**
     * Finds the Recomtrasfondos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Recomtrasfondos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Recomtrasfondos::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
    public function actionTodaspuntuacionestrasfondos(){
        $dataProvider = new ActiveDataProvider([
            'query' => Recomtrasfondos::find(),
        ]);
        
        return $this->render('todastrasfondos', [
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionMediatrasfondos(){
        $dataProvider = new ActiveDataProvider([
            'query' => Recomtrasfondos::find() -> groupBy('id_trasfondo')
                                              -> select('id_trasfondo, avg(puntuación) as mediapuntuación')
                                              -> orderBy(['mediapuntuación' => SORT_DESC]),
        ]);
        
        return $this->render('mediatrasfondos', [
            'dataProvider' => $dataProvider,
        ]);
    }
}
