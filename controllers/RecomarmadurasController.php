<?php

namespace app\controllers;

use Yii;
use app\models\Recomarmaduras;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RecomarmadurasController implements the CRUD actions for Recomarmaduras model.
 */
class RecomarmadurasController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Recomarmaduras models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Recomarmaduras::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Recomarmaduras model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Recomarmaduras model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Recomarmaduras();
        $modelUsuario = \app\models\Usuarios::find()->all();
        $modelArmadura = \app\models\Armaduras::find()->all();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'modelUsuario' => $modelUsuario,
            'modelArmadura' => $modelArmadura,
        ]);
    }

    /**
     * Updates an existing Recomarmaduras model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = new Recomarmaduras();
        $modelUsuario = \app\models\Usuarios::find()->all();
        $modelArmadura = \app\models\Armaduras::find()->all();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'modelUsuario' => $modelUsuario,
            'modelArmadura' => $modelArmadura,
        ]);
    }

    /**
     * Deletes an existing Recomarmaduras model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['todaspuntuacionesarmaduras']);
    }

    /**
     * Finds the Recomarmaduras model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Recomarmaduras the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Recomarmaduras::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
    public function actionTodaspuntuacionesarmaduras(){
        $dataProvider = new ActiveDataProvider([
            'query' => Recomarmaduras::find(),
        ]);
        
        return $this->render('todasarmaduras', [
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionMediaarmaduras(){
        $dataProvider = new ActiveDataProvider([
            'query' => Recomarmaduras::find() -> groupBy('id_armadura')
                                              -> select('id_armadura, avg(puntuación) as mediapuntuación')
                                              -> orderBy(['mediapuntuación' => SORT_DESC]),
        ]);
        
        return $this->render('mediaarmaduras', [
            'dataProvider' => $dataProvider,
        ]);
    }
}
