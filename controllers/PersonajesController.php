<?php

namespace app\controllers;

use Yii;
use app\models\Personajes;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * PersonajesController implements the CRUD actions for Personajes model.
 */
class PersonajesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Personajes models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Personajes::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Personajes model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Personajes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Personajes();
        $modelUsuario = \app\models\Usuarios::find()->all();
        $modelArmadura = \app\models\Armaduras::find()->all();
        $modelRaza = \app\models\Razas::find()->all();
        $modelTrasfondo = \app\models\Trasfondos::find()->all();
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_personaje]);
        }

        return $this->render('create', [
            'model' => $model,
            'modelUsuario' => $modelUsuario,
            'modelArmadura' => $modelArmadura,
            'modelRaza' => $modelRaza,
            'modelTrasfondo' => $modelTrasfondo,
        ]);
    }
    public function actionCreatepersonajepasos()
    {
        $model = new Personajes();
        $modelUsuario = \app\models\Usuarios::find()->all();
        $modelArmadura = \app\models\Armaduras::find()->all();
        $modelRaza = \app\models\Razas::find()->all();
        $modelTrasfondo = \app\models\Trasfondos::find()->all();
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['clasesdelospersonajes/create', 'id_personaje' => $model->id_personaje]);
        }
         if (Yii::$app->request->isPost) {
                $model->imagen = UploadedFile::getInstance($model, 'imagen');
                if ($model->imagen = UploadedFile::getInstance($model, 'imagen')) {
                    
                    if ($model->validate()) {
                        $model->imagen->saveAs('../web/img/' . $model->nombre . '.' . $model->imagen->extension);
                    }
                }
            }
        return $this->render('create', [
            'model' => $model,
            'modelUsuario' => $modelUsuario,
            'modelArmadura' => $modelArmadura,
            'modelRaza' => $modelRaza,
            'modelTrasfondo' => $modelTrasfondo,
        ]);
    }

    /**
     * Updates an existing Personajes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelUsuario = \app\models\Usuarios::find()->all();
        $modelArmadura = \app\models\Armaduras::find()->all();
        $modelRaza = \app\models\Razas::find()->all();
        $modelTrasfondo = \app\models\Trasfondos::find()->all();
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_personaje]);
        }

        return $this->render('update', [
            'model' => $model,
            'modelUsuario' => $modelUsuario,
            'modelArmadura' => $modelArmadura,
            'modelRaza' => $modelRaza,
            'modelTrasfondo' => $modelTrasfondo,
        ]);
    }

    /**
     * Deletes an existing Personajes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['listapersonajes']);
    }

    /**
     * Finds the Personajes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Personajes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Personajes::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
    public function actionListapersonajes(){
        $dataProvider = new ActiveDataProvider([
            'query' => Personajes::find(),
        ]);
                
        return $this->render("personajes", [
             'dataProvider' => $dataProvider,
        ]);
    }
    
        public function actionListapersonajesusuarios($id_usuario){
        $dataProvider = new ActiveDataProvider([
            'query' => Personajes::find()->where("id_usuario = $id_usuario"),
        ]);
                
        return $this->render("personajes", [
             'dataProvider' => $dataProvider,
        ]);
    }
    public function actionPersonajesrecomendados(){
        $dataProvider = new ActiveDataProvider([
            'query' => Personajes::find()->where("recomendado = True"),
        ]);
                
        return $this->render("personajes", [
             'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionPersonajesnorecomendados(){
        $dataProvider = new ActiveDataProvider([
            'query' => Personajes::find()->where("recomendado = False"),
        ]);
                
        return $this->render("personajes", [
             'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionPersonaje($id){
        $dataProvider = new ActiveDataProvider([
            'query' => Personajes::find()->where("id_personaje = $id"),
        ]);
                
        return $this->render("personajecompleto", [
             'dataProvider' => $dataProvider,
        ]);
    }
   
}
