<?php

namespace app\controllers;

use Yii;
use app\models\Recomrazas;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RecomrazasController implements the CRUD actions for Recomrazas model.
 */
class RecomrazasController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Recomrazas models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Recomrazas::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Recomrazas model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Recomrazas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Recomrazas();
        $modelUsuario = \app\models\Usuarios::find()->all();
        $modelRaza = \app\models\Razas::find()->all();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'modelUsuario' => $modelUsuario,
            'modelRaza' => $modelRaza,
        ]);
    }

    /**
     * Updates an existing Recomrazas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelUsuario = \app\models\Usuarios::find()->all();
        $modelRaza = \app\models\Razas::find()->all();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'modelUsuario' => $modelUsuario,
            'modelRaza' => $modelRaza,
        ]);
    }

    /**
     * Deletes an existing Recomrazas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['todaspuntuacionesrazas']);
    }

    /**
     * Finds the Recomrazas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Recomrazas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Recomrazas::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
        public function actionTodaspuntuacionesrazas(){
        $dataProvider = new ActiveDataProvider([
            'query' => Recomrazas::find(),
        ]);
        
        return $this->render('todasrazas', [
            'dataProvider' => $dataProvider,
        ]);
    }
    
        public function actionMediarazas(){
            $dataProvider = new ActiveDataProvider([
                'query' => Recomrazas::find() -> groupBy('id_raza')
                                                  -> select('id_raza, avg(puntuación) as mediapuntuación')
                                                  -> orderBy(['mediapuntuación' => SORT_DESC]),
            ]);

            return $this->render('mediarazas', [
                'dataProvider' => $dataProvider,
            ]);
        }
}
