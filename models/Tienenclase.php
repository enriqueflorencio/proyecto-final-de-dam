<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tienenclase".
 *
 * @property int $id
 * @property int $id_clase
 * @property int $id_habilidadesclase
 * @property int $nivel_desbloque
 *
 * @property Clases $clase
 * @property Habilidadesclase $habilidadesclase
 */
class Tienenclase extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tienenclase';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_clase', 'id_habilidadesclase', 'nivel_desbloque'], 'required'],
            [['id_clase', 'id_habilidadesclase', 'nivel_desbloque'], 'integer'],
            [['id_clase', 'id_habilidadesclase'], 'unique', 'targetAttribute' => ['id_clase', 'id_habilidadesclase']],
            [['id_clase'], 'exist', 'skipOnError' => true, 'targetClass' => Clases::className(), 'targetAttribute' => ['id_clase' => 'id_clase']],
            [['id_habilidadesclase'], 'exist', 'skipOnError' => true, 'targetClass' => Habilidadesclase::className(), 'targetAttribute' => ['id_habilidadesclase' => 'id_habilidadesclase']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_clase' => 'Id Clase',
            'id_habilidadesclase' => 'Id Habilidades que se consigue las clases',
            'nivel_desbloque' => 'Nivel de Desbloque',
        ];
    }

    /**
     * Gets query for [[Clase]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getClase()
    {
        return $this->hasOne(Clases::className(), ['id_clase' => 'id_clase']);
    }

    /**
     * Gets query for [[Habilidadesclase]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getHabilidadesclase()
    {
        return $this->hasOne(Habilidadesclase::className(), ['id_habilidadesclase' => 'id_habilidadesclase']);
    }
}
