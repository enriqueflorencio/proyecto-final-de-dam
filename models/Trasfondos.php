<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "trasfondos".
 *
 * @property int $id_trasfondo
 * @property string $nombre
 * @property string|null $descripcion
 * @property string|null $equipo
 * @property string|null $herramientas
 *
 * @property Comphabilidades[] $comphabilidades
 * @property Idiomastrasfondos[] $idiomastrasfondos
 * @property Personajes[] $personajes
 * @property Recomtrasfondos[] $recomtrasfondos
 * @property Usuarios[] $usuarios
 */
class Trasfondos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trasfondos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['descripcion', 'equipo', 'herramientas'], 'string'],
            [['nombre'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_trasfondo' => 'Id Trasfondo',
            'nombre' => 'Nombre',
            'descripcion' => 'Descripcion',
            'equipo' => 'Equipo',
            'herramientas' => 'Herramientas',
        ];
    }

    /**
     * Gets query for [[Comphabilidades]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComphabilidades()
    {
        return $this->hasMany(Comphabilidades::className(), ['id_trasfondo' => 'id_trasfondo']);
    }

    /**
     * Gets query for [[Idiomastrasfondos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdiomastrasfondos()
    {
        return $this->hasMany(Idiomastrasfondos::className(), ['id_trasfondo' => 'id_trasfondo']);
    }

    /**
     * Gets query for [[Personajes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPersonajes()
    {
        return $this->hasMany(Personajes::className(), ['id_trasfondo' => 'id_trasfondo']);
    }

    /**
     * Gets query for [[Recomtrasfondos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRecomtrasfondos()
    {
        return $this->hasMany(Recomtrasfondos::className(), ['id_trasfondo' => 'id_trasfondo']);
    }

    /**
     * Gets query for [[Usuarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarios()
    {
        return $this->hasMany(Usuarios::className(), ['id_usuario' => 'id_usuario'])->viaTable('recomtrasfondos', ['id_trasfondo' => 'id_trasfondo']);
    }
}
