<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "objetos".
 *
 * @property int $id_objeto
 * @property string $nombre
 * @property string|null $imagen
 * @property string|null $descripcion
 * @property string $tipo
 * @property int $mágico
 * @property int|null $oro
 *
 * @property Objetosdelpersonaje[] $objetosdelpersonajes
 * @property Personajes[] $personajes
 * @property Recomobjetos[] $recomobjetos
 * @property Usuarios[] $usuarios
 */
class Objetos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'objetos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'tipo'], 'required'],
            [['descripcion'], 'string'],
            [['mágico', 'oro', 'plata', 'cobre'], 'integer'],
            [['nombre'], 'string', 'max' => 100],
            [['tipo'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_objeto' => 'Id Objeto',
            'nombre' => 'Nombre',
            'descripcion' => 'Descripción',
            'tipo' => 'Tipo',
            'mágico' => 'Mágico',
            'oro' => 'Oro',
            'plata' => 'Plata',
            'cobre' => 'Cobre',
        ];
    }

    /**
     * Gets query for [[Objetosdelpersonajes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getObjetosdelpersonajes()
    {
        return $this->hasMany(Objetosdelpersonaje::className(), ['id_objeto' => 'id_objeto']);
    }

    /**
     * Gets query for [[Personajes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPersonajes()
    {
        return $this->hasMany(Personajes::className(), ['id_personaje' => 'id_personaje'])->viaTable('objetosdelpersonaje', ['id_objeto' => 'id_objeto']);
    }

    /**
     * Gets query for [[Recomobjetos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRecomobjetos()
    {
        return $this->hasMany(Recomobjetos::className(), ['id_objeto' => 'id_objeto']);
    }

    /**
     * Gets query for [[Usuarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarios()
    {
        return $this->hasMany(Usuarios::className(), ['id_usuario' => 'id_usuario'])->viaTable('recomobjetos', ['id_objeto' => 'id_objeto']);
    }
}
