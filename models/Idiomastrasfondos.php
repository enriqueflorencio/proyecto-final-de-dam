<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "idiomastrasfondos".
 *
 * @property int $id
 * @property int $id_trasfondo
 * @property string|null $idioma
 *
 * @property Trasfondos $trasfondo
 */
class Idiomastrasfondos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'idiomastrasfondos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_trasfondo'], 'required'],
            [['id_trasfondo'], 'integer'],
            [['idioma'], 'string', 'max' => 100],
            [['id_trasfondo', 'idioma'], 'unique', 'targetAttribute' => ['id_trasfondo', 'idioma']],
            [['id_trasfondo'], 'exist', 'skipOnError' => true, 'targetClass' => Trasfondos::className(), 'targetAttribute' => ['id_trasfondo' => 'id_trasfondo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_trasfondo' => 'Id Trasfondo',
            'idioma' => 'Idioma',
        ];
    }

    /**
     * Gets query for [[Trasfondo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTrasfondo()
    {
        return $this->hasOne(Trasfondos::className(), ['id_trasfondo' => 'id_trasfondo']);
    }
}
