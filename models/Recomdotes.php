<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "recomdotes".
 *
 * @property int $id
 * @property int $id_usuario
 * @property int $id_dote
 * @property float $puntuación
 * @property string|null $reseña
 *
 * @property Dotes $dote
 * @property Usuarios $usuario
 */
class Recomdotes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'recomdotes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_usuario', 'id_dote', 'puntuación'], 'required'],
            [['id_usuario', 'id_dote'], 'integer'],
            [['puntuación'], 'number'],
            [['reseña'], 'string'],
            [['id_usuario', 'id_dote'], 'unique', 'targetAttribute' => ['id_usuario', 'id_dote']],
            [['id_dote'], 'exist', 'skipOnError' => true, 'targetClass' => Dotes::className(), 'targetAttribute' => ['id_dote' => 'id_dote']],
            [['id_usuario'], 'exist', 'skipOnError' => true, 'targetClass' => Usuarios::className(), 'targetAttribute' => ['id_usuario' => 'id_usuario']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_usuario' => 'Id Usuario',
            'id_dote' => 'Id Dote',
            'puntuación' => 'Puntuación',
            'reseña' => 'Reseña',
        ];
    }

    /**
     * Gets query for [[Dote]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDote()
    {
        return $this->hasOne(Dotes::className(), ['id_dote' => 'id_dote']);
    }

    /**
     * Gets query for [[Usuario]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(Usuarios::className(), ['id_usuario' => 'id_usuario']);
    }
    
    public $mediapuntuación;
}
