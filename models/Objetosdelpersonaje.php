<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "objetosdelpersonaje".
 *
 * @property int $id
 * @property int $id_personaje
 * @property int $id_objeto
 *
 * @property Objetos $objeto
 * @property Personajes $personaje
 */
class Objetosdelpersonaje extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'objetosdelpersonaje';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_personaje', 'id_objeto'], 'required'],
            [['id_personaje', 'id_objeto'], 'integer'],
            [['id_personaje', 'id_objeto'], 'unique', 'targetAttribute' => ['id_personaje', 'id_objeto']],
            [['id_objeto'], 'exist', 'skipOnError' => true, 'targetClass' => Objetos::className(), 'targetAttribute' => ['id_objeto' => 'id_objeto']],
            [['id_personaje'], 'exist', 'skipOnError' => true, 'targetClass' => Personajes::className(), 'targetAttribute' => ['id_personaje' => 'id_personaje']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_personaje' => 'Id Personaje',
            'id_objeto' => 'Id Objeto',
        ];
    }

    /**
     * Gets query for [[Objeto]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getObjeto()
    {
        return $this->hasOne(Objetos::className(), ['id_objeto' => 'id_objeto']);
    }

    /**
     * Gets query for [[Personaje]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPersonaje()
    {
        return $this->hasOne(Personajes::className(), ['id_personaje' => 'id_personaje']);
    }
}
