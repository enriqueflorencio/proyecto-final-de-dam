<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tienenrazas".
 *
 * @property int $id
 * @property int $id_raza
 * @property int $id_habilidadesraza
 *
 * @property Habilidadesraza $habilidadesraza
 * @property Razas $raza
 */
class Tienenrazas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tienenrazas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_raza', 'id_habilidadesraza'], 'required'],
            [['id_raza', 'id_habilidadesraza'], 'integer'],
            [['id_raza', 'id_habilidadesraza'], 'unique', 'targetAttribute' => ['id_raza', 'id_habilidadesraza']],
            [['id_habilidadesraza'], 'exist', 'skipOnError' => true, 'targetClass' => Habilidadesraza::className(), 'targetAttribute' => ['id_habilidadesraza' => 'id_habilidadraza']],
            [['id_raza'], 'exist', 'skipOnError' => true, 'targetClass' => Razas::className(), 'targetAttribute' => ['id_raza' => 'id_raza']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_raza' => 'Id Raza',
            'id_habilidadesraza' => 'Id Habilidadesraza',
        ];
    }

    /**
     * Gets query for [[Habilidadesraza]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getHabilidadesraza()
    {
        return $this->hasOne(Habilidadesraza::className(), ['id_habilidadraza' => 'id_habilidadesraza']);
    }

    /**
     * Gets query for [[Raza]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRaza()
    {
        return $this->hasOne(Razas::className(), ['id_raza' => 'id_raza']);
    }
}
