<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "habilidadesclase".
 *
 * @property int $id_habilidadesclase
 * @property string $nombre
 * @property string|null $descripción
 *
 * @property Tienenclase[] $tienenclases
 * @property Clases[] $clases
 */
class Habilidadesclase extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'habilidadesclase';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['descripción'], 'string'],
            [['nombre'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_habilidadesclase' => 'Id Habilidadesclase',
            'nombre' => 'Nombre',
            'descripción' => 'Descripción',
        ];
    }

    /**
     * Gets query for [[Tienenclases]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTienenclases()
    {
        return $this->hasMany(Tienenclase::className(), ['id_habilidadesclase' => 'id_habilidadesclase']);
    }

    /**
     * Gets query for [[Clases]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getClases()
    {
        return $this->hasMany(Clases::className(), ['id_clase' => 'id_clase'])->viaTable('tienenclase', ['id_habilidadesclase' => 'id_habilidadesclase']);
    }
}
