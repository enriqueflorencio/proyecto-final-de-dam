<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "recommagias".
 *
 * @property int $id
 * @property int $id_usuario
 * @property int $id_magia
 * @property float $puntuación
 * @property string|null $reseña
 *
 * @property Magias $magia
 * @property Usuarios $usuario
 */
class Recommagias extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'recommagias';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_usuario', 'id_magia', 'puntuación'], 'required'],
            [['id_usuario', 'id_magia'], 'integer'],
            [['puntuación'], 'number'],
            [['reseña'], 'string'],
            [['id_usuario', 'id_magia'], 'unique', 'targetAttribute' => ['id_usuario', 'id_magia']],
            [['id_magia'], 'exist', 'skipOnError' => true, 'targetClass' => Magias::className(), 'targetAttribute' => ['id_magia' => 'id_magia']],
            [['id_usuario'], 'exist', 'skipOnError' => true, 'targetClass' => Usuarios::className(), 'targetAttribute' => ['id_usuario' => 'id_usuario']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_usuario' => 'Id Usuario',
            'id_magia' => 'Id Magia',
            'puntuación' => 'Puntuación',
            'reseña' => 'Reseña',
        ];
    }

    /**
     * Gets query for [[Magia]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMagia()
    {
        return $this->hasOne(Magias::className(), ['id_magia' => 'id_magia']);
    }

    /**
     * Gets query for [[Usuario]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(Usuarios::className(), ['id_usuario' => 'id_usuario']);
    }
    
    public $mediapuntuación;
}
