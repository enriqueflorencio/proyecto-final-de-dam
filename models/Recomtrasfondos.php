<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "recomtrasfondos".
 *
 * @property int $id
 * @property int $id_usuario
 * @property int $id_trasfondo
 * @property float $puntuación
 * @property string|null $reseña
 *
 * @property Trasfondos $trasfondo
 * @property Usuarios $usuario
 */
class Recomtrasfondos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'recomtrasfondos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_usuario', 'id_trasfondo', 'puntuación'], 'required'],
            [['id_usuario', 'id_trasfondo'], 'integer'],
            [['puntuación'], 'number'],
            [['reseña'], 'string'],
            [['id_usuario', 'id_trasfondo'], 'unique', 'targetAttribute' => ['id_usuario', 'id_trasfondo']],
            [['id_trasfondo'], 'exist', 'skipOnError' => true, 'targetClass' => Trasfondos::className(), 'targetAttribute' => ['id_trasfondo' => 'id_trasfondo']],
            [['id_usuario'], 'exist', 'skipOnError' => true, 'targetClass' => Usuarios::className(), 'targetAttribute' => ['id_usuario' => 'id_usuario']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_usuario' => 'Id Usuario',
            'id_trasfondo' => 'Id Trasfondo',
            'puntuación' => 'Puntuación',
            'reseña' => 'Reseña',
        ];
    }

    /**
     * Gets query for [[Trasfondo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTrasfondo()
    {
        return $this->hasOne(Trasfondos::className(), ['id_trasfondo' => 'id_trasfondo']);
    }

    /**
     * Gets query for [[Usuario]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(Usuarios::className(), ['id_usuario' => 'id_usuario']);
    }
    
    public $mediapuntuación;
}
