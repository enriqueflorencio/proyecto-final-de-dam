<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "recomrazas".
 *
 * @property int $id
 * @property int $id_usuario
 * @property int $id_raza
 * @property float $puntuación
 * @property string|null $reseña
 *
 * @property Razas $raza
 * @property Usuarios $usuario
 */
class Recomrazas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'recomrazas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_usuario', 'id_raza', 'puntuación'], 'required'],
            [['id_usuario', 'id_raza'], 'integer'],
            [['puntuación'], 'number'],
            [['reseña'], 'string'],
            [['id_raza', 'id_usuario'], 'unique', 'targetAttribute' => ['id_raza', 'id_usuario']],
            [['id_raza'], 'exist', 'skipOnError' => true, 'targetClass' => Razas::className(), 'targetAttribute' => ['id_raza' => 'id_raza']],
            [['id_usuario'], 'exist', 'skipOnError' => true, 'targetClass' => Usuarios::className(), 'targetAttribute' => ['id_usuario' => 'id_usuario']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_usuario' => 'Id Usuario',
            'id_raza' => 'Id Raza',
            'puntuación' => 'Puntuación',
            'reseña' => 'Reseña',
        ];
    }

    /**
     * Gets query for [[Raza]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRaza()
    {
        return $this->hasOne(Razas::className(), ['id_raza' => 'id_raza']);
    }

    /**
     * Gets query for [[Usuario]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(Usuarios::className(), ['id_usuario' => 'id_usuario']);
    }
    
    public $mediapuntuación;
}
