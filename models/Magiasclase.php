<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "magiasclase".
 *
 * @property int $id
 * @property int $id_clase
 * @property int $id_magia
 * @property int $nivel_desbloqueo
 *
 * @property Clases $clase
 * @property Magias $magia
 */
class Magiasclase extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'magiasclase';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_clase', 'id_magia', 'nivel_desbloqueo'], 'required'],
            [['id_clase', 'id_magia', 'nivel_desbloqueo'], 'integer'],
            [['id_clase', 'id_magia'], 'unique', 'targetAttribute' => ['id_clase', 'id_magia']],
            [['id_clase'], 'exist', 'skipOnError' => true, 'targetClass' => Clases::className(), 'targetAttribute' => ['id_clase' => 'id_clase']],
            [['id_magia'], 'exist', 'skipOnError' => true, 'targetClass' => Magias::className(), 'targetAttribute' => ['id_magia' => 'id_magia']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_clase' => 'Id Clase',
            'id_magia' => 'Id Magia',
            'nivel_desbloqueo' => 'Nivel de desbloqueo',
        ];
    }

    /**
     * Gets query for [[Clase]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getClase()
    {
        return $this->hasOne(Clases::className(), ['id_clase' => 'id_clase']);
    }

    /**
     * Gets query for [[Magia]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMagia()
    {
        return $this->hasOne(Magias::className(), ['id_magia' => 'id_magia']);
    }
}
