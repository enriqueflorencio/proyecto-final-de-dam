<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "clases".
 *
 * @property int $id_clase
 * @property string $nombre
 * @property string|null $descripcion
 * @property string|null $imagen
 * @property string $carateristica_principal
 * @property string|null $competencias
 * @property string $dado_golpe
 * @property string|null $tipo
 * @property string|null $aptitud_mágica
 * @property int $nivel_minimo
 * @property int $fuerza
 * @property int $destreza
 * @property int $constitucion
 * @property int $inteligencia
 * @property int $sabiduria
 * @property int $carisma
 *
 * @property Clasecomphabilidades[] $clasecomphabilidades
 * @property Clasesdelospersonajes[] $clasesdelospersonajes
 * @property Personajes[] $personajes
 * @property Magiasclase[] $magiasclases
 * @property Magias[] $magias
 * @property Recomclases[] $recomclases
 * @property Usuarios[] $usuarios
 * @property Salvaciones[] $salvaciones
 * @property Tienenclase[] $tienenclases
 * @property Habilidadesclase[] $habilidadesclases
 */
class Clases extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clases';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'carateristica_principal', 'dado_golpe', 'fuerza', 'destreza', 'constitucion', 'inteligencia', 'sabiduria', 'carisma'], 'required'],
            [['descripcion', 'competencias', 'aptitud_mágica'], 'string'],
            [['nivel_minimo', 'fuerza', 'destreza', 'constitucion', 'inteligencia', 'sabiduria', 'carisma'], 'integer'],
            [['nombre'], 'string', 'max' => 100],
            [['imagen', 'tipo'], 'string', 'max' => 200],
            [['carateristica_principal'], 'string', 'max' => 50],
            [['dado_golpe'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_clase' => 'Id Clase',
            'nombre' => 'Nombre',
            'descripcion' => 'Descripcion',
            'imagen' => 'Imagen',
            'carateristica_principal' => 'Carateristica Principal',
            'competencias' => 'Competencias',
            'dado_golpe' => 'Dado Golpe',
            'tipo' => 'Tipo',
            'aptitud_mágica' => 'Aptitud Mágica',
            'nivel_minimo' => 'Nivel Minimo',
            'fuerza' => 'Fuerza',
            'destreza' => 'Destreza',
            'constitucion' => 'Constitucion',
            'inteligencia' => 'Inteligencia',
            'sabiduria' => 'Sabiduria',
            'carisma' => 'Carisma',
        ];
    }

    /**
     * Gets query for [[Clasecomphabilidades]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getClasecomphabilidades()
    {
        return $this->hasMany(Clasecomphabilidades::className(), ['id_clase' => 'id_clase']);
    }

    /**
     * Gets query for [[Clasesdelospersonajes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getClasesdelospersonajes()
    {
        return $this->hasMany(Clasesdelospersonajes::className(), ['id_clase' => 'id_clase']);
    }

    /**
     * Gets query for [[Personajes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPersonajes()
    {
        return $this->hasMany(Personajes::className(), ['id_personaje' => 'id_personaje'])->viaTable('clasesdelospersonajes', ['id_clase' => 'id_clase']);
    }

    /**
     * Gets query for [[Magiasclases]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMagiasclases()
    {
        return $this->hasMany(Magiasclase::className(), ['id_clase' => 'id_clase']);
    }

    /**
     * Gets query for [[Magias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMagias()
    {
        return $this->hasMany(Magias::className(), ['id_magia' => 'id_magia'])->viaTable('magiasclase', ['id_clase' => 'id_clase']);
    }

    /**
     * Gets query for [[Recomclases]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRecomclases()
    {
        return $this->hasMany(Recomclases::className(), ['id_clase' => 'id_clase']);
    }

    /**
     * Gets query for [[Usuarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarios()
    {
        return $this->hasMany(Usuarios::className(), ['id_usuario' => 'id_usuario'])->viaTable('recomclases', ['id_clase' => 'id_clase']);
    }

    /**
     * Gets query for [[Salvaciones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSalvaciones()
    {
        return $this->hasMany(Salvaciones::className(), ['id_clase' => 'id_clase']);
    }

    /**
     * Gets query for [[Tienenclases]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTienenclases()
    {
        return $this->hasMany(Tienenclase::className(), ['id_clase' => 'id_clase']);
    }

    /**
     * Gets query for [[Habilidadesclases]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getHabilidadesclases()
    {
        return $this->hasMany(Habilidadesclase::className(), ['id_habilidadesclase' => 'id_habilidadesclase'])->viaTable('tienenclase', ['id_clase' => 'id_clase']);
    }
}
