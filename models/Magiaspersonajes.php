<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "magiaspersonajes".
 *
 * @property int $id
 * @property int $id_magia
 * @property int $id_personaje
 *
 * @property Magias $magia
 * @property Personajes $personaje
 */
class Magiaspersonajes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'magiaspersonajes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_magia', 'id_personaje'], 'required'],
            [['id_magia', 'id_personaje'], 'integer'],
            [['id_personaje', 'id_magia'], 'unique', 'targetAttribute' => ['id_personaje', 'id_magia']],
            [['id_magia'], 'exist', 'skipOnError' => true, 'targetClass' => Magias::className(), 'targetAttribute' => ['id_magia' => 'id_magia']],
            [['id_personaje'], 'exist', 'skipOnError' => true, 'targetClass' => Personajes::className(), 'targetAttribute' => ['id_personaje' => 'id_personaje']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_magia' => 'Id Magia',
            'id_personaje' => 'Id Personaje',
        ];
    }

    /**
     * Gets query for [[Magia]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMagia()
    {
        return $this->hasOne(Magias::className(), ['id_magia' => 'id_magia']);
    }

    /**
     * Gets query for [[Personaje]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPersonaje()
    {
        return $this->hasOne(Personajes::className(), ['id_personaje' => 'id_personaje']);
    }
}
