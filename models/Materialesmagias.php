<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "materialesmagias".
 *
 * @property int $id
 * @property int $id_magia
 * @property string $material
 *
 * @property Magias $magia
 */
class Materialesmagias extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'materialesmagias';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_magia', 'material'], 'required'],
            [['id_magia'], 'integer'],
            [['material'], 'string'],
            [['material', 'id_magia'], 'unique', 'targetAttribute' => ['material', 'id_magia']],
            [['id_magia'], 'exist', 'skipOnError' => true, 'targetClass' => Magias::className(), 'targetAttribute' => ['id_magia' => 'id_magia']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_magia' => 'Id Magia',
            'material' => 'Material',
        ];
    }

    /**
     * Gets query for [[Magia]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMagia()
    {
        return $this->hasOne(Magias::className(), ['id_magia' => 'id_magia']);
    }
}
