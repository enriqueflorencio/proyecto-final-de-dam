<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "habilidadesraza".
 *
 * @property int $id_habilidadraza
 * @property string $nombre
 * @property string|null $descripcion
 *
 * @property Tienenrazas[] $tienenrazas
 * @property Razas[] $razas
 */
class Habilidadesraza extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'habilidadesraza';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['descripcion'], 'string'],
            [['nombre'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_habilidadraza' => 'Id Habilidadraza',
            'nombre' => 'Nombre',
            'descripcion' => 'Descripción',
        ];
    }

    /**
     * Gets query for [[Tienenrazas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTienenrazas()
    {
        return $this->hasMany(Tienenrazas::className(), ['id_habilidadesraza' => 'id_habilidadraza']);
    }

    /**
     * Gets query for [[Razas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRazas()
    {
        return $this->hasMany(Razas::className(), ['id_raza' => 'id_raza'])->viaTable('tienenrazas', ['id_habilidadesraza' => 'id_habilidadraza']);
    }
}
