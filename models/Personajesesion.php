<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "personajesesion".
 *
 * @property int $id
 * @property int $id_personaje
 * @property int $id_sesion
 *
 * @property Personajes $personaje
 * @property Sesiones $sesion
 */
class Personajesesion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'personajesesion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_personaje', 'id_sesion'], 'required'],
            [['id_personaje', 'id_sesion'], 'integer'],
            [['id_personaje', 'id_sesion'], 'unique', 'targetAttribute' => ['id_personaje', 'id_sesion']],
            [['id_personaje'], 'exist', 'skipOnError' => true, 'targetClass' => Personajes::className(), 'targetAttribute' => ['id_personaje' => 'id_personaje']],
            [['id_sesion'], 'exist', 'skipOnError' => true, 'targetClass' => Sesiones::className(), 'targetAttribute' => ['id_sesion' => 'id_sesion']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_personaje' => 'Id Personaje',
            'id_sesion' => 'Id Sesión',
        ];
    }

    /**
     * Gets query for [[Personaje]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPersonaje()
    {
        return $this->hasOne(Personajes::className(), ['id_personaje' => 'id_personaje']);
    }

    /**
     * Gets query for [[Sesion]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSesion()
    {
        return $this->hasOne(Sesiones::className(), ['id_sesion' => 'id_sesion']);
    }
}
