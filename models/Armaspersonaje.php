<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "armaspersonaje".
 *
 * @property int $id
 * @property int $id_personaje
 * @property int $id_arma
 * @property int $bonificador
 *
 * @property Armas $arma
 * @property Personajes $personaje
 */
class Armaspersonaje extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'armaspersonaje';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_personaje', 'id_arma', 'bonificador'], 'required'],
            [['id_personaje', 'id_arma', 'bonificador'], 'integer'],
            [['id_arma', 'id_personaje'], 'unique', 'targetAttribute' => ['id_arma', 'id_personaje']],
            [['id_arma'], 'exist', 'skipOnError' => true, 'targetClass' => Armas::className(), 'targetAttribute' => ['id_arma' => 'id_arma']],
            [['id_personaje'], 'exist', 'skipOnError' => true, 'targetClass' => Personajes::className(), 'targetAttribute' => ['id_personaje' => 'id_personaje']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'id',
            'id_personaje' => 'Personajes',
            'id_arma' => 'Armas',
            'bonificador' => 'Bonificador',
        ];
    }

    /**
     * Gets query for [[Arma]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getArma()
    {
        return $this->hasOne(Armas::className(), ['id_arma' => 'id_arma']);
    }

    /**
     * Gets query for [[Personaje]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPersonaje()
    {
        return $this->hasOne(Personajes::className(), ['id_personaje' => 'id_personaje']);
    }
}
