<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "clasesdelospersonajes".
 *
 * @property int $id
 * @property int $id_clase
 * @property int $id_personaje
 * @property int $nivel_clase
 *
 * @property Clases $clase
 * @property Personajes $personaje
 */
class Clasesdelospersonajes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clasesdelospersonajes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_clase', 'id_personaje', 'nivel_clase'], 'required'],
            [['id_clase', 'id_personaje', 'nivel_clase'], 'integer'],
            [['id_clase', 'id_personaje'], 'unique', 'targetAttribute' => ['id_clase', 'id_personaje']],
            [['id_clase'], 'exist', 'skipOnError' => true, 'targetClass' => Clases::className(), 'targetAttribute' => ['id_clase' => 'id_clase']],
            [['id_personaje'], 'exist', 'skipOnError' => true, 'targetClass' => Personajes::className(), 'targetAttribute' => ['id_personaje' => 'id_personaje']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_clase' => 'id_clase',
            'id_personaje' => 'id_personaje',
            'nivel_clase' => 'Nivel que tiene la Clase',
        ];
    }

    /**
     * Gets query for [[Clase]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getClase()
    {
        return $this->hasOne(Clases::className(), ['id_clase' => 'id_clase']);
    }

    /**
     * Gets query for [[Personaje]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPersonaje()
    {
        return $this->hasOne(Personajes::className(), ['id_personaje' => 'id_personaje']);
    }
}
