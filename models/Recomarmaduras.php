<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "recomarmaduras".
 *
 * @property int $id
 * @property int $id_usuario
 * @property int $id_armadura
 * @property float $puntuación
 * @property string|null $reseña
 *
 * @property Armaduras $armadura
 * @property Usuarios $usuario
 */
class Recomarmaduras extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'recomarmaduras';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_usuario', 'id_armadura', 'puntuación'], 'required'],
            [['id_usuario', 'id_armadura'], 'integer'],
            [['puntuación'], 'number'],
            [['reseña'], 'string'],
            [['id_usuario', 'id_armadura'], 'unique', 'targetAttribute' => ['id_usuario', 'id_armadura']],
            [['id_armadura'], 'exist', 'skipOnError' => true, 'targetClass' => Armaduras::className(), 'targetAttribute' => ['id_armadura' => 'id_armaduras']],
            [['id_usuario'], 'exist', 'skipOnError' => true, 'targetClass' => Usuarios::className(), 'targetAttribute' => ['id_usuario' => 'id_usuario']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_usuario' => 'Id Usuario',
            'id_armadura' => 'Id Armadura',
            'puntuación' => 'Puntuación',
            'reseña' => 'Reseña',
        ];
    }

    /**
     * Gets query for [[Armadura]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getArmadura()
    {
        return $this->hasOne(Armaduras::className(), ['id_armaduras' => 'id_armadura']);
    }

    /**
     * Gets query for [[Usuario]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(Usuarios::className(), ['id_usuario' => 'id_usuario']);
    }
    
    public $mediapuntuación;
}
