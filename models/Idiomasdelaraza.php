<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "idiomasdelaraza".
 *
 * @property int $id
 * @property int $id_raza
 * @property string|null $idioma
 *
 * @property Razas $raza
 */
class Idiomasdelaraza extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'idiomasdelaraza';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_raza'], 'required'],
            [['id_raza'], 'integer'],
            [['idioma'], 'string', 'max' => 50],
            [['id_raza', 'idioma'], 'unique', 'targetAttribute' => ['id_raza', 'idioma']],
            [['id_raza'], 'exist', 'skipOnError' => true, 'targetClass' => Razas::className(), 'targetAttribute' => ['id_raza' => 'id_raza']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_raza' => 'Id Raza',
            'idioma' => 'Idioma',
        ];
    }

    /**
     * Gets query for [[Raza]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRaza()
    {
        return $this->hasOne(Razas::className(), ['id_raza' => 'id_raza']);
    }
}
