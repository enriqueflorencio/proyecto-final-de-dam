<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "armaduras".
 *
 * @property int $id_armaduras
 * @property string $nombre
 * @property string|null $imagen
 * @property string|null $descripcion
 * @property string $tipo
 * @property int|null $ca
 * @property int|null $oro
 * @property int $mágico
 * @property int $desventaja_sigilo
 *
 * @property Personajes[] $personajes
 * @property Recomarmaduras[] $recomarmaduras
 * @property Usuarios[] $usuarios
 */
class Armaduras extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'armaduras';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'tipo', 'desventaja_sigilo'], 'required'],
            [['descripcion'], 'string'],
            [['ca', 'oro', 'mágico', 'desventaja_sigilo'], 'integer'],
            [['nombre'], 'string', 'max' => 100],
            [['imagen'], 'string', 'max' => 200],
            [['tipo'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_armaduras' => 'Id Armaduras',
            'nombre' => 'Nombre',
            'imagen' => 'Imagen',
            'descripcion' => 'Descripcion',
            'tipo' => 'Tipo',
            'ca' => 'CA',
            'oro' => 'Oro',
            'mágico' => 'Mágico',
            'desventaja_sigilo' => 'Desventaja ante el sigilo',
        ];
    }

    /**
     * Gets query for [[Personajes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPersonajes()
    {
        return $this->hasMany(Personajes::className(), ['id_armadura' => 'id_armaduras']);
    }

    /**
     * Gets query for [[Recomarmaduras]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRecomarmaduras()
    {
        return $this->hasMany(Recomarmaduras::className(), ['id_armadura' => 'id_armaduras']);
    }

    /**
     * Gets query for [[Usuarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarios()
    {
        return $this->hasMany(Usuarios::className(), ['id_usuario' => 'id_usuario'])->viaTable('recomarmaduras', ['id_armadura' => 'id_armaduras']);
    }
    
}
