<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "clasecomphabilidades".
 *
 * @property int $id
 * @property int $id_clase
 * @property string $habilidad
 *
 * @property Clases $clase
 */
class Clasecomphabilidades extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clasecomphabilidades';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_clase', 'habilidad'], 'required'],
            [['id_clase'], 'integer'],
            [['habilidad'], 'string', 'max' => 20],
            [['id_clase', 'habilidad'], 'unique', 'targetAttribute' => ['id_clase', 'habilidad']],
            [['id_clase'], 'exist', 'skipOnError' => true, 'targetClass' => Clases::className(), 'targetAttribute' => ['id_clase' => 'id_clase']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_clase' => 'Clase',
            'habilidad' => 'Habilidad',
        ];
    }

    /**
     * Gets query for [[Clase]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getClase()
    {
        return $this->hasOne(Clases::className(), ['id_clase' => 'id_clase']);
    }
}
