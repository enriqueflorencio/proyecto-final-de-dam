<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "recomarmas".
 *
 * @property int $id
 * @property int $id_usuario
 * @property int $id_arma
 * @property float $puntuación
 * @property string|null $reseña
 *
 * @property Armas $arma
 * @property Usuarios $usuario
 */
class Recomarmas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'recomarmas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_usuario', 'id_arma', 'puntuación'], 'required'],
            [['id_usuario', 'id_arma'], 'integer'],
            [['puntuación'], 'number'],
            [['reseña'], 'string'],
            [['id_usuario', 'id_arma'], 'unique', 'targetAttribute' => ['id_usuario', 'id_arma']],
            [['id_arma'], 'exist', 'skipOnError' => true, 'targetClass' => Armas::className(), 'targetAttribute' => ['id_arma' => 'id_arma']],
            [['id_usuario'], 'exist', 'skipOnError' => true, 'targetClass' => Usuarios::className(), 'targetAttribute' => ['id_usuario' => 'id_usuario']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_usuario' => 'Id Usuario',
            'id_arma' => 'Id Arma',
            'puntuación' => 'Puntuación',
            'reseña' => 'Reseña',
        ];
    }

    /**
     * Gets query for [[Arma]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getArma()
    {
        return $this->hasOne(Armas::className(), ['id_arma' => 'id_arma']);
    }

    /**
     * Gets query for [[Usuario]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(Usuarios::className(), ['id_usuario' => 'id_usuario']);
    }
    
    public $mediapuntuación;
}
