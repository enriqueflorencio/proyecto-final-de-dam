<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dotesdelospersonajes".
 *
 * @property int $id
 * @property int $id_dote
 * @property int $id_personaje
 *
 * @property Dotes $dote
 * @property Personajes $personaje
 */
class Dotesdelospersonajes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dotesdelospersonajes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_dote', 'id_personaje'], 'required'],
            [['id_dote', 'id_personaje'], 'integer'],
            [['id_personaje', 'id_dote'], 'unique', 'targetAttribute' => ['id_personaje', 'id_dote']],
            [['id_dote'], 'exist', 'skipOnError' => true, 'targetClass' => Dotes::className(), 'targetAttribute' => ['id_dote' => 'id_dote']],
            [['id_personaje'], 'exist', 'skipOnError' => true, 'targetClass' => Personajes::className(), 'targetAttribute' => ['id_personaje' => 'id_personaje']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_dote' => 'Id Dote',
            'id_personaje' => 'Id Personaje',
        ];
    }

    /**
     * Gets query for [[Dote]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDote()
    {
        return $this->hasOne(Dotes::className(), ['id_dote' => 'id_dote']);
    }

    /**
     * Gets query for [[Personaje]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPersonaje()
    {
        return $this->hasOne(Personajes::className(), ['id_personaje' => 'id_personaje']);
    }
}
