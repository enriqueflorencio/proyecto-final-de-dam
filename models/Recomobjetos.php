<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "recomobjetos".
 *
 * @property int $id
 * @property int $id_usuario
 * @property int $id_objeto
 * @property float $puntuación
 * @property string|null $reseña
 *
 * @property Objetos $objeto
 * @property Usuarios $usuario
 */
class Recomobjetos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'recomobjetos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_usuario', 'id_objeto', 'puntuación'], 'required'],
            [['id_usuario', 'id_objeto'], 'integer'],
            [['puntuación'], 'number'],
            [['reseña'], 'string'],
            [['id_usuario', 'id_objeto'], 'unique', 'targetAttribute' => ['id_usuario', 'id_objeto']],
            [['id_objeto'], 'exist', 'skipOnError' => true, 'targetClass' => Objetos::className(), 'targetAttribute' => ['id_objeto' => 'id_objeto']],
            [['id_usuario'], 'exist', 'skipOnError' => true, 'targetClass' => Usuarios::className(), 'targetAttribute' => ['id_usuario' => 'id_usuario']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_usuario' => 'Id Usuario',
            'id_objeto' => 'Id Objeto',
            'puntuación' => 'Puntuación',
            'reseña' => 'Reseña',
        ];
    }

    /**
     * Gets query for [[Objeto]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getObjeto()
    {
        return $this->hasOne(Objetos::className(), ['id_objeto' => 'id_objeto']);
    }

    /**
     * Gets query for [[Usuario]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(Usuarios::className(), ['id_usuario' => 'id_usuario']);
    }
    
    public $mediapuntuación;
}
