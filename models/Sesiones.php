<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sesiones".
 *
 * @property int $id_sesion
 * @property string|null $duracion
 * @property int $num_miembros
 * @property int $nivel_medio
 * @property string $fecha
 * @property int $cod_master
 *
 * @property Personajesesion[] $personajesesions
 * @property Personajes[] $personajes
 * @property Masters $codMaster
 */
class Sesiones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sesiones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha',], 'required'],
            [['num_miembros', 'nivel_medio'], 'integer'],
            [['fecha'], 'safe'],
            [['duracion'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_sesion' => 'Id Sesion',
            'duracion' => 'Duración (en minutos)',
            'num_miembros' => 'Número de miembros de la sesión',
            'nivel_medio' => 'Nivel medio del equipo',
            'fecha' => 'Fecha'
        ];
    }

    /**
     * Gets query for [[Personajesesions]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPersonajesesions()
    {
        return $this->hasMany(Personajesesion::className(), ['id_sesion' => 'id_sesion']);
    }

    /**
     * Gets query for [[Personajes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPersonajes()
    {
        return $this->hasMany(Personajes::className(), ['id_personaje' => 'id_personaje'])->viaTable('personajesesion', ['id_sesion' => 'id_sesion']);
    }

    
}
