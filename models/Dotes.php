<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dotes".
 *
 * @property int $id_dote
 * @property string $titulo
 * @property string|null $descripcion
 * @property int|null $fuerza
 * @property int|null $destreza
 * @property int|null $constitucion
 * @property int|null $inteligencia
 * @property int|null $sabiduria
 * @property int|null $carisma
 *
 * @property Dotesdelospersonajes[] $dotesdelospersonajes
 * @property Personajes[] $personajes
 * @property Recomdotes[] $recomdotes
 * @property Usuarios[] $usuarios
 */
class Dotes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dotes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['titulo'], 'required'],
            [['descripcion'], 'string'],
            [['fuerza', 'destreza', 'constitucion', 'inteligencia', 'sabiduria', 'carisma'], 'integer'],
            [['titulo'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_dote' => 'Id Dote',
            'titulo' => 'Titulo',
            'descripcion' => 'Descripcion',
            'fuerza' => 'Fuerza',
            'destreza' => 'Destreza',
            'constitucion' => 'Constitucion',
            'inteligencia' => 'Inteligencia',
            'sabiduria' => 'Sabiduria',
            'carisma' => 'Carisma',
        ];
    }

    /**
     * Gets query for [[Dotesdelospersonajes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDotesdelospersonajes()
    {
        return $this->hasMany(Dotesdelospersonajes::className(), ['id_dote' => 'id_dote']);
    }

    /**
     * Gets query for [[Personajes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPersonajes()
    {
        return $this->hasMany(Personajes::className(), ['id_personaje' => 'id_personaje'])->viaTable('dotesdelospersonajes', ['id_dote' => 'id_dote']);
    }

    /**
     * Gets query for [[Recomdotes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRecomdotes()
    {
        return $this->hasMany(Recomdotes::className(), ['id_dote' => 'id_dote']);
    }

    /**
     * Gets query for [[Usuarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarios()
    {
        return $this->hasMany(Usuarios::className(), ['id_usuario' => 'id_usuario'])->viaTable('recomdotes', ['id_dote' => 'id_dote']);
    }
}
