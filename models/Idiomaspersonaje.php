<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "idiomaspersonaje".
 *
 * @property int $id
 * @property int $id_personaje
 * @property string|null $idioma
 *
 * @property Personajes $personaje
 */
class Idiomaspersonaje extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'idiomaspersonaje';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_personaje'], 'required'],
            [['id_personaje'], 'integer'],
            [['idioma'], 'string', 'max' => 50],
            [['id_personaje', 'idioma'], 'unique', 'targetAttribute' => ['id_personaje', 'idioma']],
            [['id_personaje'], 'exist', 'skipOnError' => true, 'targetClass' => Personajes::className(), 'targetAttribute' => ['id_personaje' => 'id_personaje']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_personaje' => 'Id Personaje',
            'idioma' => 'Idioma',
        ];
    }

    /**
     * Gets query for [[Personaje]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPersonaje()
    {
        return $this->hasOne(Personajes::className(), ['id_personaje' => 'id_personaje']);
    }
}
