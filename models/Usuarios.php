<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "usuarios".
 *
 * @property int $id_usuario
 * @property string $nombre
 * @property string|null $icono
 * @property string $email
 * @property string $contraseña
 *
 * @property Personajes[] $personajes
 * @property Recomarmaduras[] $recomarmaduras
 * @property Armaduras[] $armaduras
 * @property Recomarmas[] $recomarmas
 * @property Armas[] $armas
 * @property Recomclases[] $recomclases
 * @property Clases[] $clases
 * @property Recomdotes[] $recomdotes
 * @property Dotes[] $dotes
 * @property Recommagias[] $recommagias
 * @property Magias[] $magias
 * @property Recomobjetos[] $recomobjetos
 * @property Objetos[] $objetos
 * @property Recomrazas[] $recomrazas
 * @property Razas[] $razas
 * @property Recomtrasfondos[] $recomtrasfondos
 * @property Trasfondos[] $trasfondos
 */
class Usuarios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'usuarios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'email', 'contraseña'], 'required'],
            [['nombre'], 'string', 'max' => 100],
            [['icono'], 'string', 'max' => 200],
            [['email'], 'string', 'max' => 1000],
            [['contraseña'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_usuario' => 'Id Usuario',
            'nombre' => 'Nombre',
            'icono' => 'Icono',
            'email' => 'Email',
            'contraseña' => 'Contraseña',
        ];
    }

    /**
     * Gets query for [[Personajes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPersonajes()
    {
        return $this->hasMany(Personajes::className(), ['id_usuario' => 'id_usuario']);
    }

    /**
     * Gets query for [[Recomarmaduras]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRecomarmaduras()
    {
        return $this->hasMany(Recomarmaduras::className(), ['id_usuario' => 'id_usuario']);
    }

    /**
     * Gets query for [[Armaduras]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getArmaduras()
    {
        return $this->hasMany(Armaduras::className(), ['id_armaduras' => 'id_armadura'])->viaTable('recomarmaduras', ['id_usuario' => 'id_usuario']);
    }

    /**
     * Gets query for [[Recomarmas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRecomarmas()
    {
        return $this->hasMany(Recomarmas::className(), ['id_usuario' => 'id_usuario']);
    }

    /**
     * Gets query for [[Armas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getArmas()
    {
        return $this->hasMany(Armas::className(), ['id_arma' => 'id_arma'])->viaTable('recomarmas', ['id_usuario' => 'id_usuario']);
    }

    /**
     * Gets query for [[Recomclases]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRecomclases()
    {
        return $this->hasMany(Recomclases::className(), ['id_usuario' => 'id_usuario']);
    }

    /**
     * Gets query for [[Clases]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getClases()
    {
        return $this->hasMany(Clases::className(), ['id_clase' => 'id_clase'])->viaTable('recomclases', ['id_usuario' => 'id_usuario']);
    }

    /**
     * Gets query for [[Recomdotes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRecomdotes()
    {
        return $this->hasMany(Recomdotes::className(), ['id_usuario' => 'id_usuario']);
    }

    /**
     * Gets query for [[Dotes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDotes()
    {
        return $this->hasMany(Dotes::className(), ['id_dote' => 'id_dote'])->viaTable('recomdotes', ['id_usuario' => 'id_usuario']);
    }

    /**
     * Gets query for [[Recommagias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRecommagias()
    {
        return $this->hasMany(Recommagias::className(), ['id_usuario' => 'id_usuario']);
    }

    /**
     * Gets query for [[Magias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMagias()
    {
        return $this->hasMany(Magias::className(), ['id_magia' => 'id_magia'])->viaTable('recommagias', ['id_usuario' => 'id_usuario']);
    }

    /**
     * Gets query for [[Recomobjetos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRecomobjetos()
    {
        return $this->hasMany(Recomobjetos::className(), ['id_usuario' => 'id_usuario']);
    }

    /**
     * Gets query for [[Objetos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getObjetos()
    {
        return $this->hasMany(Objetos::className(), ['id_objeto' => 'id_objeto'])->viaTable('recomobjetos', ['id_usuario' => 'id_usuario']);
    }

    /**
     * Gets query for [[Recomrazas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRecomrazas()
    {
        return $this->hasMany(Recomrazas::className(), ['id_usuario' => 'id_usuario']);
    }

    /**
     * Gets query for [[Razas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRazas()
    {
        return $this->hasMany(Razas::className(), ['id_raza' => 'id_raza'])->viaTable('recomrazas', ['id_usuario' => 'id_usuario']);
    }

    /**
     * Gets query for [[Recomtrasfondos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRecomtrasfondos()
    {
        return $this->hasMany(Recomtrasfondos::className(), ['id_usuario' => 'id_usuario']);
    }

    /**
     * Gets query for [[Trasfondos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTrasfondos()
    {
        return $this->hasMany(Trasfondos::className(), ['id_trasfondo' => 'id_trasfondo'])->viaTable('recomtrasfondos', ['id_usuario' => 'id_usuario']);
    }
}
