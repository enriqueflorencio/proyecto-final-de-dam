<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "personajes".
 *
 * @property int $id_personaje
 * @property int|null $id_usuario
 * @property int|null $id_armadura
 * @property int|null $id_raza
 * @property int|null $id_trasfondo
 * @property string $nombre
 * @property string|null $descripción
 * @property string|null $imagen
 * @property int $vivo
 * @property string|null $biografia
 * @property int $edad
 * @property int $nivel
 * @property string|null $alineamineto
 * @property int $fuerza
 * @property int $destreza
 * @property int $constitucion
 * @property int $inteligencia
 * @property int $sabiduria
 * @property int $carisma
 * @property int|null $modfuerza
 * @property int|null $moddestreza
 * @property int|null $modconstitucion
 * @property int|null $modinteligencia
 * @property int|null $modsabiduria
 * @property int|null $modcarisma
 * @property int|null $vida
 * @property int|null $iniciativa
 * @property int|null $inspiracion
 * @property int|null $ca
 * @property int $escudo
 * @property int|null $oro
 * @property int|null $plata
 * @property int|null $cobre
 * @property int|null $velocidad
 * @property int|null $salvfuerza
 * @property int|null $salvdestreza
 * @property int|null $salvconstitucion
 * @property int|null $salvinteligencia
 * @property int|null $salvsabiduria
 * @property int|null $salvcarisma
 * @property int|null $bono_competencia
 * @property int|null $acrobacias
 * @property int|null $atletismo
 * @property int|null $conoarcano
 * @property int|null $engaño
 * @property int|null $historia
 * @property int|null $interpretacion
 * @property int|null $intimidacion
 * @property int|null $investigacion
 * @property int|null $juego_de_manos
 * @property int|null $medicina
 * @property int|null $naturaleza
 * @property int|null $percepcion
 * @property int|null $perspicacia
 * @property int|null $persuasión
 * @property int|null $religion
 * @property int|null $sigilo
 * @property int|null $supervivencia
 * @property int|null $trato_animales
 * @property int|null $percepción_pasiva
 * @property int|null $salv_conjuto
 * @property int|null $mod_ataque_conjuro
 * @property string|null $notas
 * @property int|null $recomendado
 *
 * @property Armaspersonaje[] $armaspersonajes
 * @property Armas[] $armas
 * @property Clasesdelospersonajes[] $clasesdelospersonajes
 * @property Clases[] $clases
 * @property Dotesdelospersonajes[] $dotesdelospersonajes
 * @property Dotes[] $dotes
 * @property Idiomaspersonaje[] $idiomaspersonajes
 * @property Magiaspersonajes[] $magiaspersonajes
 * @property Magias[] $magias
 * @property Objetosdelpersonaje[] $objetosdelpersonajes
 * @property Objetos[] $objetos
 * @property Armaduras $armadura
 * @property Razas $raza
 * @property Trasfondos $trasfondo
 * @property Usuarios $usuario
 * @property Personajesesion[] $personajesesions
 * @property Sesiones[] $sesions
 */
class Personajes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'personajes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_usuario', 'id_armadura', 'id_raza', 'id_trasfondo', 'vivo', 'edad', 'fuerza', 'destreza', 'constitucion', 'inteligencia', 'sabiduria', 'carisma', 'modfuerza', 'moddestreza', 'modconstitucion', 'modinteligencia', 'modsabiduria', 'modcarisma', 'vida', 'iniciativa', 'inspiracion', 'ca', 'escudo', 'oro', 'plata', 'cobre', 'velocidad', 'salvfuerza', 'salvdestreza', 'salvconstitucion', 'salvinteligencia', 'salvsabiduria', 'salvcarisma', 'bono_competencia', 'acrobacias', 'atletismo', 'conoarcano', 'engaño', 'historia', 'interpretacion', 'intimidacion', 'investigacion', 'juego_de_manos', 'medicina', 'naturaleza', 'percepcion', 'perspicacia', 'persuasión', 'religion', 'sigilo', 'supervivencia', 'trato_animales', 'percepción_pasiva', 'salv_conjuto', 'mod_ataque_conjuro', 'recomendado'], 'integer'],
            [['nivel'], 'integer', 'min'=>1,'max'=>20],
            [['nombre', 'edad', 'fuerza', 'destreza', 'constitucion', 'inteligencia', 'sabiduria', 'carisma'], 'required'],
            [['fuerza', 'destreza', 'constitucion', 'inteligencia', 'sabiduria', 'carisma'],'integer', 'min'=>1,'max'=>20,],
            [['descripción', 'biografia', 'alineamineto', 'notas'], 'string'],
            [['nombre'], 'string', 'max' => 100],
            [['id_armadura'], 'exist', 'skipOnError' => true, 'targetClass' => Armaduras::className(), 'targetAttribute' => ['id_armadura' => 'id_armaduras']],
            [['id_raza'], 'exist', 'skipOnError' => true, 'targetClass' => Razas::className(), 'targetAttribute' => ['id_raza' => 'id_raza']],
            [['id_trasfondo'], 'exist', 'skipOnError' => true, 'targetClass' => Trasfondos::className(), 'targetAttribute' => ['id_trasfondo' => 'id_trasfondo']],
            [['id_usuario'], 'exist', 'skipOnError' => true, 'targetClass' => Usuarios::className(), 'targetAttribute' => ['id_usuario' => 'id_usuario']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_personaje' => 'Id Personaje',
            'id_usuario' => 'Usuario',
            'id_armadura' => 'Armadura',
            'id_raza' => 'Raza',
            'id_trasfondo' => 'Trasfondo',
            'nombre' => 'Nombre',
            'descripción' => 'Descripción',
            'vivo' => 'Vivo',
            'biografia' => 'Biografía',
            'edad' => 'Edad',
            'nivel' => 'Nivel',
            'alineamineto' => 'Alineamineto',
            'fuerza' => 'Fuerza',
            'destreza' => 'Destreza',
            'constitucion' => 'Constitucion',
            'inteligencia' => 'Inteligencia',
            'sabiduria' => 'Sabiduria',
            'carisma' => 'Carisma',
            'modfuerza' => 'Modificar de fuerza',
            'moddestreza' => 'Modificador de destreza',
            'modconstitucion' => 'Modificador de constitución',
            'modinteligencia' => 'Modificador de inteligencia',
            'modsabiduria' => 'Modificador de sabiduria',
            'modcarisma' => 'Modificador de carisma',
            'vida' => 'Vida',
            'iniciativa' => 'Iniciativa',
            'inspiracion' => 'Inspiracion',
            'ca' => 'Clase de Armadura',
            'escudo' => 'Escudo',
            'oro' => 'Oro',
            'plata' => 'Plata',
            'cobre' => 'Cobre',
            'velocidad' => 'Velocidad',
            'salvfuerza' => 'Salvación de fuerza',
            'salvdestreza' => 'Salvación de destreza',
            'salvconstitucion' => 'Salvación de constitución',
            'salvinteligencia' => 'Salvación de inteligencia',
            'salvsabiduria' => 'Salvación de sabiduría',
            'salvcarisma' => 'Salvación de carisma',
            'bono_competencia' => 'Bono por Competencia',
            'acrobacias' => 'Acrobacias',
            'atletismo' => 'Atletismo',
            'conoarcano' => 'Conocimiento Arcano',
            'engaño' => 'Engaño',
            'historia' => 'Historia',
            'interpretacion' => 'Interpretación',
            'intimidacion' => 'Intimidación',
            'investigacion' => 'Investigacion',
            'juego_de_manos' => 'Juego De Manos',
            'medicina' => 'Medicina',
            'naturaleza' => 'Naturaleza',
            'percepcion' => 'Percepción',
            'perspicacia' => 'Perspicacia',
            'persuasión' => 'Persuasión',
            'religion' => 'Religión',
            'sigilo' => 'Sigilo',
            'supervivencia' => 'Supervivencia',
            'trato_animales' => 'Trato con Animales',
            'percepción_pasiva' => 'Percepción Pasiva',
            'salv_conjuto' => 'Salvación de Conjuro',
            'mod_ataque_conjuro' => 'Modificador de Ataque Conjuro',
            'notas' => 'Notas',
            'recomendado' => 'Recomendado',
        ];
    }

    /**
     * Gets query for [[Armaspersonajes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getArmaspersonajes()
    {
        return $this->hasMany(Armaspersonaje::className(), ['id_personaje' => 'id_personaje']);
    }

    /**
     * Gets query for [[Armas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getArmas()
    {
        return $this->hasMany(Armas::className(), ['id_arma' => 'id_arma'])->viaTable('armaspersonaje', ['id_personaje' => 'id_personaje']);
    }

    /**
     * Gets query for [[Clasesdelospersonajes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getClasesdelospersonajes()
    {
        return $this->hasMany(Clasesdelospersonajes::className(), ['id_personaje' => 'id_personaje']);
    }

    /**
     * Gets query for [[Clases]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getClases()
    {
        return $this->hasMany(Clases::className(), ['id_clase' => 'id_clase'])->viaTable('clasesdelospersonajes', ['id_personaje' => 'id_personaje']);
    }

    /**
     * Gets query for [[Dotesdelospersonajes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDotesdelospersonajes()
    {
        return $this->hasMany(Dotesdelospersonajes::className(), ['id_personaje' => 'id_personaje']);
    }

    /**
     * Gets query for [[Dotes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDotes()
    {
        return $this->hasMany(Dotes::className(), ['id_dote' => 'id_dote'])->viaTable('dotesdelospersonajes', ['id_personaje' => 'id_personaje']);
    }

    /**
     * Gets query for [[Idiomaspersonajes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdiomaspersonajes()
    {
        return $this->hasMany(Idiomaspersonaje::className(), ['id_personaje' => 'id_personaje']);
    }

    /**
     * Gets query for [[Magiaspersonajes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMagiaspersonajes()
    {
        return $this->hasMany(Magiaspersonajes::className(), ['id_personaje' => 'id_personaje']);
    }

    /**
     * Gets query for [[Magias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMagias()
    {
        return $this->hasMany(Magias::className(), ['id_magia' => 'id_magia'])->viaTable('magiaspersonajes', ['id_personaje' => 'id_personaje']);
    }

    /**
     * Gets query for [[Objetosdelpersonajes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getObjetosdelpersonajes()
    {
        return $this->hasMany(Objetosdelpersonaje::className(), ['id_personaje' => 'id_personaje']);
    }

    /**
     * Gets query for [[Objetos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getObjetos()
    {
        return $this->hasMany(Objetos::className(), ['id_objeto' => 'id_objeto'])->viaTable('objetosdelpersonaje', ['id_personaje' => 'id_personaje']);
    }

    /**
     * Gets query for [[Armadura]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getArmadura()
    {
        return $this->hasOne(Armaduras::className(), ['id_armaduras' => 'id_armadura']);
    }

    /**
     * Gets query for [[Raza]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRaza()
    {
        return $this->hasOne(Razas::className(), ['id_raza' => 'id_raza']);
    }

    /**
     * Gets query for [[Trasfondo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTrasfondo()
    {
        return $this->hasOne(Trasfondos::className(), ['id_trasfondo' => 'id_trasfondo']);
    }

    /**
     * Gets query for [[Usuario]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(Usuarios::className(), ['id_usuario' => 'id_usuario']);
    }

    /**
     * Gets query for [[Personajesesions]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPersonajesesions()
    {
        return $this->hasMany(Personajesesion::className(), ['id_personaje' => 'id_personaje']);
    }

    /**
     * Gets query for [[Sesions]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSesiones()
    {
        return $this->hasMany(Sesiones::className(), ['id_sesion' => 'id_sesion'])->viaTable('personajesesion', ['id_personaje' => 'id_personaje']);
    }
    
    public function getNombrearmas()

    {

        $nombreAr = [];
        $daño = [];

        foreach($this->armas as $armas) {

            $nombreAr[] = $armas->nombre;
            $daño[] = $armas->daño;

        }

        return implode(", ",$nombreAr);

    }
    public function getNombreobjetos()

    {

        $nombreObj = [];

        foreach($this->objetos as $objetos) {

            $nombreObj[] = $objetos->nombre;

        }

        return implode(", ",$nombreObj);

    }
    public function getNombreclase()

    {

        $nombreCl = [];

        foreach($this->clases as $clase) {

            $nombreCl[] = $clase->nombre;

        }
        

        return implode(", ",$nombreCl);

    }
    public function getNombremagia()

    {

        $nombreMg = [];

        foreach($this->magias as $magia) {

            $nombreMg[] = $magia->nombre;

        }
        

        return implode(", ",$nombreMg);

    }
    public function getNombredote()

    {

        $nombreD = [];

        foreach($this->dotes as $dotes) {

            $nombreD[] = $dotes->titulo;

        }
        

        return implode(", ",$nombreD);

    }
    public function getNombreidiomas()

    {

        $nombreI = [];

        foreach($this->idiomaspersonajes as $idiomas) {

            $nombreI[] = $idiomas->idioma;

        }
        

        return implode(", ",$nombreI);

    }
}
