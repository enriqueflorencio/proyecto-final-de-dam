<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "magias".
 *
 * @property int $id_magia
 * @property string $nombre
 * @property string|null $descripcion
 * @property string $nivel_conjuro
 * @property string $escuela
 * @property string $rango
 * @property string $tiempo_casteo
 * @property string $duración
 * @property string|null $concentracion_ritual
 *
 * @property Magiasclase[] $magiasclases
 * @property Clases[] $clases
 * @property Magiaspersonajes[] $magiaspersonajes
 * @property Personajes[] $personajes
 * @property Materialesmagias[] $materialesmagias
 * @property Recommagias[] $recommagias
 * @property Usuarios[] $usuarios
 */
class Magias extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'magias';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'nivel_conjuro', 'escuela', 'rango', 'tiempo_casteo', 'duración'], 'required'],
            [['descripcion', 'nivel_conjuro', 'escuela', 'concentracion_ritual'], 'string'],
            [['nombre', 'rango', 'tiempo_casteo', 'duración'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_magia' => 'Id Magia',
            'nombre' => 'Nombre',
            'descripcion' => 'Descripcion',
            'nivel_conjuro' => 'Nivel Conjuro',
            'escuela' => 'Escuela',
            'rango' => 'Rango',
            'tiempo_casteo' => 'Tiempo Casteo',
            'duración' => 'Duración',
            'concentracion_ritual' => 'Concentracion Ritual',
        ];
    }

    /**
     * Gets query for [[Magiasclases]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMagiasclases()
    {
        return $this->hasMany(Magiasclase::className(), ['id_magia' => 'id_magia']);
    }

    /**
     * Gets query for [[Clases]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getClases()
    {
        return $this->hasMany(Clases::className(), ['id_clase' => 'id_clase'])->viaTable('magiasclase', ['id_magia' => 'id_magia']);
    }

    /**
     * Gets query for [[Magiaspersonajes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMagiaspersonajes()
    {
        return $this->hasMany(Magiaspersonajes::className(), ['id_magia' => 'id_magia']);
    }

    /**
     * Gets query for [[Personajes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPersonajes()
    {
        return $this->hasMany(Personajes::className(), ['id_personaje' => 'id_personaje'])->viaTable('magiaspersonajes', ['id_magia' => 'id_magia']);
    }

    /**
     * Gets query for [[Materialesmagias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMaterialesmagias()
    {
        return $this->hasMany(Materialesmagias::className(), ['id_magia' => 'id_magia']);
    }

    /**
     * Gets query for [[Recommagias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRecommagias()
    {
        return $this->hasMany(Recommagias::className(), ['id_magia' => 'id_magia']);
    }

    /**
     * Gets query for [[Usuarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarios()
    {
        return $this->hasMany(Usuarios::className(), ['id_usuario' => 'id_usuario'])->viaTable('recommagias', ['id_magia' => 'id_magia']);
    }
}
