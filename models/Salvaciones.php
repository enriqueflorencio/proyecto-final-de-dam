<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "salvaciones".
 *
 * @property int $id
 * @property int $id_clase
 * @property string $salvaciones
 *
 * @property Clases $clase
 */
class Salvaciones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'salvaciones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_clase', 'salvaciones'], 'required'],
            [['id_clase'], 'integer'],
            [['salvaciones'], 'string', 'max' => 50],
            [['id_clase', 'salvaciones'], 'unique', 'targetAttribute' => ['id_clase', 'salvaciones']],
            [['id_clase'], 'exist', 'skipOnError' => true, 'targetClass' => Clases::className(), 'targetAttribute' => ['id_clase' => 'id_clase']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_clase' => 'Id Clase',
            'salvaciones' => 'Salvaciones',
        ];
    }

    /**
     * Gets query for [[Clase]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getClase()
    {
        return $this->hasOne(Clases::className(), ['id_clase' => 'id_clase']);
    }
}
