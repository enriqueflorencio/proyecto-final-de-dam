<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "armas".
 *
 * @property int $id_arma
 * @property string $nombre
 * @property string|null $imagen
 * @property string|null $descripcion
 * @property string $tipo
 * @property int|null $oro
 * @property string|null $daño
 * @property int|null $magico
 *
 * @property Armaspersonaje[] $armaspersonajes
 * @property Personajes[] $personajes
 * @property Recomarmas[] $recomarmas
 * @property Usuarios[] $usuarios
 */
class Armas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'armas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'tipo'], 'required'],
            [['descripcion'], 'string'],
            [['oro', 'plata', 'cobre', 'magico'], 'integer'],
            [['nombre', 'daño'], 'string', 'max' => 100],
            [['tipo'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_arma' => 'Id Arma',
            'nombre' => 'Nombre',
            'descripcion' => 'Descripción',
            'tipo' => 'Tipo',
            'oro' => 'Oro',
            'plata' => 'Plata',
            'cobre' => 'Cobre',
            'daño' => 'Daño',
            'magico' => 'Mágico',
        ];
    }

    /**
     * Gets query for [[Armaspersonajes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getArmaspersonajes()
    {
        return $this->hasMany(Armaspersonaje::className(), ['id_arma' => 'id_arma']);
    }

    /**
     * Gets query for [[Personajes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPersonajes()
    {
        return $this->hasMany(Personajes::className(), ['id_personaje' => 'id_personaje'])->viaTable('armaspersonaje', ['id_arma' => 'id_arma']);
    }

    /**
     * Gets query for [[Recomarmas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRecomarmas()
    {
        return $this->hasMany(Recomarmas::className(), ['id_arma' => 'id_arma']);
    }

    /**
     * Gets query for [[Usuarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarios()
    {
        return $this->hasMany(Usuarios::className(), ['id_usuario' => 'id_usuario'])->viaTable('recomarmas', ['id_arma' => 'id_arma']);
    }
}
