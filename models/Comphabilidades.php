<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "comphabilidades".
 *
 * @property int $id
 * @property int $id_trasfondo
 * @property string $habilidad
 *
 * @property Trasfondos $trasfondo
 */
class Comphabilidades extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'comphabilidades';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_trasfondo', 'habilidad'], 'required'],
            [['id_trasfondo'], 'integer'],
            [['habilidad'], 'string', 'max' => 20],
            [['id_trasfondo', 'habilidad'], 'unique', 'targetAttribute' => ['id_trasfondo', 'habilidad']],
            [['id_trasfondo'], 'exist', 'skipOnError' => true, 'targetClass' => Trasfondos::className(), 'targetAttribute' => ['id_trasfondo' => 'id_trasfondo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_trasfondo' => 'id trasfondo',
            'habilidad' => 'Habilidad',
        ];
    }

    /**
     * Gets query for [[Trasfondo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTrasfondo()
    {
        return $this->hasOne(Trasfondos::className(), ['id_trasfondo' => 'id_trasfondo']);
    }
}
