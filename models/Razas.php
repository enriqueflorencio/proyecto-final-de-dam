<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "razas".
 *
 * @property int $id_raza
 * @property string $nombre
 * @property string|null $descripcion
 * @property string|null $imagen
 * @property int $velocidad
 * @property int|null $bonofuerza
 * @property int|null $bonodestreza
 * @property int|null $bonoconstitucion
 * @property int|null $bonointeligencia
 * @property int|null $bonosabiduria
 * @property int|null $bonocarisma
 *
 * @property Idiomasdelaraza[] $idiomasdelarazas
 * @property Personajes[] $personajes
 * @property Recomrazas[] $recomrazas
 * @property Usuarios[] $usuarios
 * @property Tienenrazas[] $tienenrazas
 * @property Habilidadesraza[] $habilidadesrazas
 */
class Razas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'razas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['descripcion'], 'string'],
            [['velocidad', 'bonofuerza', 'bonodestreza', 'bonoconstitucion', 'bonointeligencia', 'bonosabiduria', 'bonocarisma'], 'integer'],
            [['nombre'], 'string', 'max' => 100],
            [['imagen'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_raza' => 'Id Raza',
            'nombre' => 'Nombre',
            'descripcion' => 'Descripción',
            'imagen' => 'Imagen',
            'velocidad' => 'Velocidad',
            'bonofuerza' => 'Bono de fuerza',
            'bonodestreza' => 'Bono de destreza',
            'bonoconstitucion' => 'Bono de constitucion',
            'bonointeligencia' => 'Bono de inteligencia',
            'bonosabiduria' => 'Bono de sabiduria',
            'bonocarisma' => 'Bono de carisma',
        ];
    }

    /**
     * Gets query for [[Idiomasdelarazas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdiomasdelarazas()
    {
        return $this->hasMany(Idiomasdelaraza::className(), ['id_raza' => 'id_raza']);
    }

    /**
     * Gets query for [[Personajes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPersonajes()
    {
        return $this->hasMany(Personajes::className(), ['id_raza' => 'id_raza']);
    }

    /**
     * Gets query for [[Recomrazas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRecomrazas()
    {
        return $this->hasMany(Recomrazas::className(), ['id_raza' => 'id_raza']);
    }

    /**
     * Gets query for [[Usuarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarios()
    {
        return $this->hasMany(Usuarios::className(), ['id_usuario' => 'id_usuario'])->viaTable('recomrazas', ['id_raza' => 'id_raza']);
    }

    /**
     * Gets query for [[Tienenrazas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTienenrazas()
    {
        return $this->hasMany(Tienenrazas::className(), ['id_raza' => 'id_raza']);
    }

    /**
     * Gets query for [[Habilidadesrazas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getHabilidadesrazas()
    {
        return $this->hasMany(Habilidadesraza::className(), ['id_habilidadraza' => 'id_habilidadesraza'])->viaTable('tienenrazas', ['id_raza' => 'id_raza']);
    }
}
