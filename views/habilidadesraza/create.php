<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Habilidadesraza */

$this->title = 'Añadir rasgo';
$this->params['breadcrumbs'][] = ['label' => 'Razas', 'url' => ['razas/listarazas']];
$this->params['breadcrumbs'][] = ['label' => 'Rasgos que tienen las razas', 'url' => ['tienenrazas/index']];
$this->params['breadcrumbs'][] = ['label' => 'Rasgos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="habilidadesraza-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
