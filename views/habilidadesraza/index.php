<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Rasgos';
$this->params['breadcrumbs'][] = ['label' => 'Razas', 'url' => ['razas/listarazas']];
$this->params['breadcrumbs'][] = ['label' => 'Rasgos que tienen las razas', 'url' => ['tienenrazas/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="habilidadesraza-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Añadir rasgos', ['create'], ['class' => 'btn btn-default']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            
            ['class' => 'yii\grid\ActionColumn'],
            
            'nombre',
            'descripcion:ntext',

            
        ],
    ]); ?>


</div>
