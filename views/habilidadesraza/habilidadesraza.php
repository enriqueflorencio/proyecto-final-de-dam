<?php
    use yii\helpers\Html;
    use yii\widgets\ListView;
    use yii\grid\GridView;
    
    $this->title = 'Rasgos';
    $this->params['breadcrumbs'][] = ['label' => 'Razas', 'url' => ['razas/listarazas']];
    
    $this->params['breadcrumbs'][] = $this->title;
     
?>
<?php if(Yii::$app->user->isGuest): ?>
<div class="view">
    
    <?= ListView::widget([
               'dataProvider' => $dataProvider,
               'itemView' => '_habilidadesraza',
               'layout' => "\n{pager}\n{items}",
            ]);
     ?>       
</div>

<?php else: ?>
<div class="habilidadesraza-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Añadir rasgos', ['create'], ['class' => 'btn btn-default']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            
            ['class' => 'yii\grid\ActionColumn'],
            
            'nombre',
            'descripcion:ntext',

            
        ],
    ]); ?>


</div>

<?php endif; ?>
