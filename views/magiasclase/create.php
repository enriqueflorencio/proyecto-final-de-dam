<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Magiasclase */

$this->title = 'Establecer que magia pertenece a que clase';
$this->params['breadcrumbs'][] = ['label' => 'Magias', 'url' => ['magias/listamagias']];
$this->params['breadcrumbs'][] = ['label' => 'Magias que puede utilizar una clase', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="magiasclase-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelClase' => $modelClase,
        'modelMagias' => $modelMagias,
    ]) ?>

</div>
