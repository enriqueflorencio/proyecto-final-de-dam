<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Magiasclase */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="magiasclase-form">

    <?php $form = ActiveForm::begin(); ?>
    <?php $modelClase = ArrayHelper::map($modelClase, 'id_clase', 'nombre'); ?>
    <?php $modelMagias = ArrayHelper::map($modelMagias, 'id_magia', 'nombre'); ?>
    
    <?= $form->field($model, 'id_clase')->dropDownList($modelClase, ['prompt' => ''])->label("Clase") ?>

    <?= $form->field($model, 'id_magia')->dropDownList($modelMagias, ['prompt' => ''])->label("Magia") ?>

    <?= $form->field($model, 'nivel_desbloqueo')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
