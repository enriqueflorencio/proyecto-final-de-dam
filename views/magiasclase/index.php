<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Magias que puede utilizar una clase';
$this->params['breadcrumbs'][] = ['label' => 'Magias', 'url' => ['magias/listamagias']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="magiasclase-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Establecer que magia pertenece a que clase', ['create'], ['class' => 'btn btn-default']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\ActionColumn'],
            [
                'attribute' => 'id_clase',
                'label' => 'Clase',
                'value' => 'clase.nombre',
            ],
            [
                'attribute' => 'id_magia',
                'label' => 'Magia',
                'value' => 'magia.nombre',
            ],
            'nivel_desbloqueo',

        ],
    ]); ?>


</div>
