<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Magiasclase */

$this->title = 'Modificar que magia pertenece a que clase: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Magias', 'url' => ['magias/listamagias']];
$this->params['breadcrumbs'][] = ['label' => 'Magias que puede utilizar una clase', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Modificar';
?>
<div class="magiasclase-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelClase' => $modelClase,
        'modelMagias' => $modelMagias,
    ]) ?>

</div>
