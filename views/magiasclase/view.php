<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Magiasclase */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Magias', 'url' => ['magias/listamagias']];
$this->params['breadcrumbs'][] = ['label' => 'Magias que puede utilizar una clase', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="magiasclase-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Modificar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estás segura?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'id_clase',
                'label' => 'Clase',
                'value' => $model->clase->nombre,
            ],
            [
                'attribute' => 'id_magia',
                'label' => 'Magia',
                'value' => $model->magia->nombre,
            ],
            'nivel_desbloqueo',
        ],
    ]) ?>

</div>
