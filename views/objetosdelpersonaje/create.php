<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Objetosdelpersonaje */

$this->title = 'Añadir un objeto a un personaje';
$this->params['breadcrumbs'][] = ['label' => 'Personajes', 'url' => ['personajes/listapersonajes']];
$this->params['breadcrumbs'][] = ['label' => 'Objetos de los personajes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="objetosdelpersonaje-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelPersonaje' => $modelPersonaje,
        'modelObjeto' => $modelObjeto,
    ]) ?>

</div>
