<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Objetosdelpersonaje */

$this->title = 'Modificar un objeto de un personaje: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Personajes', 'url' => ['personajes/listapersonajes']];
$this->params['breadcrumbs'][] = ['label' => 'Objetos de los personajes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Modificar';
?>
<div class="objetosdelpersonaje-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelPersonaje' => $modelPersonaje,
        'modelObjeto' => $modelObjeto,
    ]) ?>

</div>
