<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Objetosdelpersonaje */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="objetosdelpersonaje-form">

    <?php $form = ActiveForm::begin(); ?>
    <?php $modelPersonaje = ArrayHelper::map($modelPersonaje, 'id_personaje', 'nombre'); ?>
    <?php $modelObjeto = ArrayHelper::map($modelObjeto, 'id_objeto', 'nombre'); ?>

    <?= $form->field($model, 'id_personaje')->dropDownList($modelPersonaje, ['promot' => ''])->label("Personaje") ?>

    <?= $form->field($model, 'id_objeto')->dropDownList($modelObjeto, ['promot' => ''])->label("Objetos") ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
