<?php
    use kartik\rating\StarRating;
?>

<div class="col-sm-6">
    <div class="thumbnail recomarmaduras">
        <h4><b><?= $model->usuario->nombre ?></b> ha puntuado <b><?= $model->trasfondo->nombre ?></b> y le ha puesto: </h4>
        
        <p><?= StarRating::widget([
            'name' => 'rating_33',
            'value' => $model->puntuación,
            'language' => 'es',
            'pluginOptions' => ['displayOnly' => true],
            ]); ?></p>
        <?php if($model->reseña): ?>
            <p>Motivos: <i><?= $model->reseña ?></i></p>
        <?php else: ?>
            <p>Sin motivos</p>
        <?php endif; ?>
    </div>
</div>

