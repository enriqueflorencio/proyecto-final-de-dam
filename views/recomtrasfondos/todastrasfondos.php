<?php
    
    use yii\grid\GridView;
    use yii\widgets\ListView;
    use yii\helpers\Html;
    
    $this->title = 'Reseña Trasfondos';
    $this->params['breadcrumbs'][] = ['label' => 'Recomendaciones', 'url' => ['site/recomendaciones']];
    $this->params['breadcrumbs'][] = ['label' => 'Media Trasfondos', 'url' => ['mediatrasfondos']];
    $this->params['breadcrumbs'][] = $this->title;
?>

<?php if(Yii::$app->user->isGuest): ?>

<div class="view">
    
    <p>
        <?= Html::a('Añadir reseña a un trasfondos', ['create'], ['class' => 'btn btn-default']) ?>
    </p>
    
    <?= ListView::widget([
               'dataProvider' => $dataProvider,
               'itemView' => '_todastrasfondos',
               'layout' => "\n{pager}\n{items}",
            ]);
     ?>       
</div>

<?php else: ?>
<div class="recomarmas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Añadir reseña a un trasfondo', ['create'], ['class' => 'btn btn-default']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            
               ['class' => 'yii\grid\ActionColumn'],
            
                [
                    'attribute' => 'id_usuario',
                    'label' => 'Usuario',
                    'value' => 'usuario.nombre',
                ],
                [
                    'attribute' => 'id_trasfondo',
                    'label' => 'Trasfondo',
                    'value' => 'trasfondo.nombre',
                ],
            'puntuación',
            'reseña:ntext',

            
        ],
    ]); ?>


</div>
<?php endif; ?>

