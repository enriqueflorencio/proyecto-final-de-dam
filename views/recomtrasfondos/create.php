<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Recomtrasfondos */

$this->title = 'Añadir una reseña a un trasfondo';
$this->params['breadcrumbs'][] = ['label' => 'Recomendaciones', 'url' => ['site/recomendaciones']];
$this->params['breadcrumbs'][] = ['label' => 'Media Trasfondos', 'url' => ['mediatrasfondos']];
$this->params['breadcrumbs'][] = ['label' => 'Reseña Trasfondos', 'url' => ['todaspuntuacionestrasfondos']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="recomtrasfondos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelUsuario' => $modelUsuario,
        'modelRaza' => $modelRaza,
    ]) ?>

</div>
