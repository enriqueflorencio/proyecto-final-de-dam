<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Recomtrasfondos */

$this->title = 'Modificar reseña de una trasfondo: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Recomendaciones', 'url' => ['site/recomendaciones']];
$this->params['breadcrumbs'][] = ['label' => 'Media Razas', 'url' => ['mediatrasfondos']];
$this->params['breadcrumbs'][] = ['label' => 'Reseña Razas', 'url' => ['todaspuntuacionestrasfondos']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Modificar';
?>
<div class="recomtrasfondos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelUsuario' => $modelUsuario,
        'modelRaza' => $modelRaza,
    ]) ?>

</div>
