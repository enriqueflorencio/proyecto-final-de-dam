<?php
    use yii\helpers\Html;
    use yii\widgets\ListView;
    use yii\grid\GridView;
    
    $this->title = 'Dotes';
    $this->params['breadcrumbs'][] = $this->title;
?>
<?php if(Yii::$app->user->isGuest): ?>

<div class="view">
    
    <div class="well well-lg"><h2 style="text-align: center">
        <?= Html::img('@web/images/flecha.png', ['class' => 'iconosheader'], ['alt' => 'dote']); ?> 
            Dotes
        <?= Html::img('@web/images/flecha.png', ['class' => 'iconosheader'], ['alt' => 'dote']); ?> 
        </h2>
    </div>
    
    <?= ListView::widget([
               'dataProvider' => $dataProvider,
               'itemView' => '_dotes',
               'layout' => "\n{pager}\n{items}",
            ]);
     ?>       
</div>

<?php else: ?>

<div class="dotes-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Añadir Dotes', ['create'], ['class' => 'btn btn-default']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\ActionColumn'],
            
            'titulo',
            'descripcion:ntext',
            'fuerza',
            'destreza',
            'constitucion',
            'inteligencia',
            'sabiduria',
            'carisma',

            
        ],
    ]); ?>


</div>


<?php endif; ?>

