<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Dotes */

$this->title = $model->titulo;
$this->params['breadcrumbs'][] = ['label' => 'Dotes', 'url' => ['dotes/listadotes']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="dotes-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Modificar', ['update', 'id' => $model->id_dote], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id_dote], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estás seguro?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            
            'titulo',
            'descripcion:ntext',
            'fuerza',
            'destreza',
            'constitucion',
            'inteligencia',
            'sabiduria',
            'carisma',
        ],
    ]) ?>

</div>
