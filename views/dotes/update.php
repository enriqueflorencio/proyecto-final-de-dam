<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Dotes */

$this->title = 'Modificar dote: ' . $model->titulo;
$this->params['breadcrumbs'][] = ['label' => 'Dotes', 'url' => ['dotes/listadotes']];
$this->params['breadcrumbs'][] = ['label' => $model->titulo, 'url' => ['view', 'id' => $model->id_dote]];
$this->params['breadcrumbs'][] = 'Modificar';
?>
<div class="dotes-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
