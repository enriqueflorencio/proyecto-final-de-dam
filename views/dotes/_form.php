<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Dotes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dotes-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'titulo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'descripcion')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'fuerza')->textInput() ?>

    <?= $form->field($model, 'destreza')->textInput() ?>

    <?= $form->field($model, 'constitucion')->textInput() ?>

    <?= $form->field($model, 'inteligencia')->textInput() ?>

    <?= $form->field($model, 'sabiduria')->textInput() ?>

    <?= $form->field($model, 'carisma')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
