<?php

?>

<div class="col-sm-6">
    <div class="thumbnail dotes">
            <h5><b>Requerimientos:</b>  
                
                    <?php if($model->fuerza != 0): ?>
                        <i>Fuerza: <?= $model->fuerza ?></i>
                    
                    <?php elseif($model->destreza != 0): ?>
                        <i>Destreza: <?= $model->destreza ?></i>
                    
                    <?php elseif($model->constitucion != 0): ?>
                        <i>Constitución: <?= $model->constitucion ?></i>
                   
                    <?php elseif($model->inteligencia != 0): ?>
                        <i>Inteligencia: <?= $model->inteligencia ?></i>
                    
                    <?php elseif($model->sabiduria != 0): ?>
                        <i>Sabiduría: <?= $model->sabiduria ?></i>
                    
                    <?php elseif($model->carisma != 0): ?>
                        <i>Carisma: <?= $model->carisma ?></i>
                     <?php else: ?>
                        No hay requerimiento
                    <?php endif; ?>                
            </h5>
            <h3><u><?= $model->titulo ?></u></h3>
            <h5 class="texto-magia">
                <?= $model->descripcion ?>          
            </h5>
    </div>
</div>
