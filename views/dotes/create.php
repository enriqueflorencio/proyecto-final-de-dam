<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Dotes */

$this->title = 'Añadir Dotes';
$this->params['breadcrumbs'][] = ['label' => 'Dotes', 'url' => ['dotes/listadotes']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dotes-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
