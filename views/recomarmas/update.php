<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Recomarmas */

$this->title = 'Modificar reseña de un arma: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Recomendaciones', 'url' => ['site/recomendaciones']];
$this->params['breadcrumbs'][] = ['label' => 'Reseña Armas', 'url' => ['todaspuntuacionesarmas']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Modificar';
?>
<div class="recomarmas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelUsuario' => $modelUsuario,
        'modelArma' => $modelArma,
    ]) ?>

</div>
