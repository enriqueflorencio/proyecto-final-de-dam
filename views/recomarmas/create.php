<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Recomarmas */

$this->title = 'Añadir reseña a un arma';
$this->params['breadcrumbs'][] = ['label' => 'Recomendaciones', 'url' => ['site/recomendaciones']];
$this->params['breadcrumbs'][] = ['label' => 'Reseña Armas', 'url' => ['todaspuntuacionesarmas']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="recomarmas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelUsuario' => $modelUsuario,
        'modelArma' => $modelArma,
        
    ]) ?>

</div>
