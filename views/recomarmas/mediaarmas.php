<?php
    
    use yii\widgets\ListView;
    use yii\helpers\Html;
    
    $this->title = 'Reseña Armaduras';
    $this->params['breadcrumbs'][] = ['label' => 'Recomendaciones', 'url' => ['site/recomendaciones']];
    $this->params['breadcrumbs'][] = $this->title;
?>

<div class="view">
    <p>
        <?= Html::a('Todas las reseñas', ['todaspuntuacionesarmas'], ['class' => 'btn btn-default']) ?>
    </p>
    <?= ListView::widget([
               'dataProvider' => $dataProvider,
               'itemView' => '_mediaarmas',
               'layout' => "\n{pager}\n{items}",
            ]);
     ?>       
</div>

