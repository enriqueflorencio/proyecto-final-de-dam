<?php
    
    use yii\grid\GridView;
    use yii\widgets\ListView;
    use yii\helpers\Html;
    
    $this->title = 'Reseña Armas';
    $this->params['breadcrumbs'][] = ['label' => 'Recomendaciones', 'url' => ['site/recomendaciones']];
    $this->params['breadcrumbs'][] = $this->title;
?>

<?php if(Yii::$app->user->isGuest): ?>

<div class="view">
    
    <?= ListView::widget([
               'dataProvider' => $dataProvider,
               'itemView' => '_todasarmas',
               'layout' => "\n{pager}\n{items}",
            ]);
     ?>       
</div>

<?php else: ?>
<div class="recomarmas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Añadir reseña a un arma', ['create'], ['class' => 'btn btn-default']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            
               ['class' => 'yii\grid\ActionColumn'],
            
                [
                    'attribute' => 'id_usuario',
                    'label' => 'Usuario',
                    'value' => 'usuario.nombre',
                ],
                [
                    'attribute' => 'id_arma',
                    'label' => 'Arma',
                    'value' => 'arma.nombre',
                ],
            'puntuación',
            'reseña:ntext',

            
        ],
    ]); ?>


</div>
<?php endif; ?>

