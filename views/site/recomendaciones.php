<?php

/* @var $this yii\web\View */
use yii\helpers\Html;

$this->title = 'Pantalla de inicio';
?>
<div class="site-index">

   <div class="jumbotron">
       <h1>Recomendaciones</h1>
    </div>
    <div class="body-content">
        <div class ="col-sm-6 col-md-3">
            <div class ="thumbnail">
                <div class="caption">
                    <h3>Armaduras</h3>
                    <p>

                        <?= Html::a('Mostrar', ['recomarmaduras/mediaarmaduras'], ['class' => 'btn btn-default'])?>
                    </p>
                </div>
            </div>
        </div>
        <div class ="col-sm-6 col-md-3">
            <div class ="thumbnail">
                <div class="caption">
                    <h3>Armas</h3>
                    <p>
                        <?= Html::a('Mostrar', ['recomarmas/mediaarmas'], ['class' => 'btn btn-default'])?>
                    </p>
                </div>
            </div>
        </div>
        <div class ="col-sm-6 col-md-3">
            <div class ="thumbnail">
                <div class="caption">
                    <h3>Clases</h3>
                    <p>
                        <?= Html::a('Mostrar', ['recomclases/mediaclases'], ['class' => 'btn btn-default'])?>
                    </p>
                </div>
            </div>
        </div>
        <div class ="col-sm-6 col-md-3">
            <div class ="thumbnail">
                <div class="caption">
                    <h3>Dotes</h3>
                    <p>
                        <?= Html::a('Mostrar', ['recomdotes/mediadotes'], ['class' => 'btn btn-default'])?>
                    </p>
                </div>
            </div>
        </div>
        <div class ="col-sm-6 col-md-3">
            <div class ="thumbnail">
                <div class="caption">
                    <h3>Magias</h3>
                    <p>
                        <?= Html::a('Mostrar', ['recommagias/mediamagias'], ['class' => 'btn btn-default'])?>
                    </p>
                </div>
            </div>
        </div>
        <div class ="col-sm-6 col-md-3">
            <div class ="thumbnail">
                <div class="caption">
                    <h3>Objetos</h3>
                    <p>
                        <?= Html::a('Mostrar', ['recomobjetos/mediaobjetos'], ['class' => 'btn btn-default'])?>
                    </p>
                </div>
            </div>
        </div>
        <div class ="col-sm-6 col-md-3">
            <div class ="thumbnail">
                <div class="caption">
                    <h3>Razas</h3>
                    <p>
                        <?= Html::a('Mostrar', ['recomrazas/mediarazas'], ['class' => 'btn btn-default'])?>
                    </p>
                </div>
            </div>
        </div>
        <div class ="col-sm-6 col-md-3">
            <div class ="thumbnail">
                <div class="caption">
                    <h3>Trasfondos</h3>
                    <p>
                        <?= Html::a('Mostrar', ['recomtrasfondos/mediatrasfondos'], ['class' => 'btn btn-default'])?>
                    </p>
                </div>
            </div>
        </div>
</div>

