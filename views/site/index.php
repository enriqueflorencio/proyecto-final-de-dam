<?php

/* @var $this yii\web\View */
use yii\helpers\Html;

$this->title = 'Pantalla de inicio';
?>

   <div class="jumbotron">
       <img src="https://upload.wikimedia.org/wikipedia/en/thumb/8/8e/Dungeons_%26_Dragons_5th_Edition_logo.svg/1920px-Dungeons_%26_Dragons_5th_Edition_logo.svg.png" alt="Imagen prueba" width="400" height="142,348"/>
    </div>
    <div class="container">
        <div class=" row">
            <div class="col-md-8">
                <div class="card">
                    <?= Html::img('@web/images/portada.png',['class' => 'card-img-top'], ['alt' => 'Imagen portada']); ?>
                    <div class="card-body">
                        <h2 class="card-title"><u>¿Qué es Dungeon and Dragons</u></h2>
                        <p class="card-text">
                           Es un juego de aventuras de fantasía. Creas un personaje, que se une a otros personajes (tus amigos) para explorar un mundo y luchar contra monstruos. Aunque D&D utiliza dados y miniaturas, la acción tiene lugar en nuestra imaginación, donde todos tenemos la libertad para crear cualquier cosa que podamos imaginar.
                        </p>
                        <p class="card-text">  Para jugar a D&D una persona debe adoptar el papel de Dungeon Master (DM), que es la persona que se ocupará de narrar la historia y arbitrar el juego. El DM crea aventuras para los personajes y narra la acción para los jugadores. El Dm preprara el escenario, pero nadie sabe qué va a suceder hasta que los personajes hacen algo y entonces suceden cosas. Puedes explorar un oscuro subterráneo, una ciudad en ruinas, un templo perdido en las profundidaes de la jungla o una caverna llena de lava bajo una misteriosa montaña. Resuelves acertijos, hablas con otros personajes, combates contra toda clase de monstruos fantásticos, y descubres fabulosos objetos mágicos.
                        </p>    
                        <p class="card-text">  D&D es un juego cooperativo, en el cual tus amigos y tú trabajáis juntos para completar cada aventura y divertiros. Es un juego narrativo, en el que los únicos límites son vuestra imaginación. Tus acciones pueden funcionar o fracasar espectacularmente, pero en cualquier caso habrás contribuido a la historia que se está desarrollando en la aventura, y probablemente te hayas divertido en el proceso.
                        </p>
                        <p class="card-text">  El juego no tiene un auténtico final: cuando terminais una historia o gesta, podéis empezar otra. Mucha gente que juega a D&D prolonga sus partidas durante meses o años, reuniéndose con sus amigos cada semana para retomar la historia en el punto donde la dejaron.
                        </p>
                        <p class="card-text">  Tus personajes crecen a medida que el juego continúa.
                        </p>
                        <p class="card-text">  Cada monstruo derrotado, cada aventura completada y cada tesoro recuperado no sólo es un añadido a la historia que está avanzando, sino que proporciona nuevas habilidades a tu personaje. Este aumento de poder se refleja mediante el nivel de tu personaje: conforme sigues jugando, tu personaje gana más experiencia, aumentando de nivel y dominando aptitudes nuevas y más poderosas.
                        </p>   
                        <p class="card-text">  De vez en cuando, tu personaje puede encontrarse con un terrible final, desgarrado por feroces monstruos o ejecutado por un villano. Pero ni siquiera cuando tu personaje es derrotado, realmente “pierdes”. Tus compañeros puede emplear poderosa magia para revivir a tu personaje, o quizás prefieras empezar uno nuevo para continuar desde donde cayó el personaje anterior. podéis fracasar completamente en una aventura, pero si habéis pasado un buen rato y habéis creado una historia que todos recordarán durante mucho tiempo, todo el grupo gana. Lo que importa en D&D no es ganar niveles y ser el mejor sinó pasar buenos ratos con tus amigos y colaborar entre todos ya que cada personaje tiene un rol diferente, un elfo picaro será más sutil a la hora de pasar inadvertido por delante de un grupo de goblins que si lo hace por ejemplo un enano guerrero, ya que no tiene desarrollada la habilidad de ocultarse.
                         </p>
                    </div>
                </div>
           </div>
            <div class="col-md-4">
                <div class="boton">
                    <div class="boton-texto" style="text-align: center;">
                       INFORMACIÓN
                    </div>
                </div>
                <p> </p>
                       <?= Html::a('Armas', ['armas/listaarmas'], ['class' => "btn btn-default btn-lg btn-block"]) ?>                                
                <p> </p>
                
                    
                       <?= Html::a('Armaduras', ['armaduras/listaarmaduras'], ['class' => "btn btn-default btn-lg btn-block"]) ?>
                    
                
                <p> </p>
                
                    
                       <?= Html::a('Clases', ['clases/listaclases'], ['class' => "btn btn-default btn-lg btn-block"]) ?>
                   
                
                <p> </p>
                
                    
                       <?= Html::a('Dotes', ['dotes/listadotes'], ['class' => "btn btn-default btn-lg btn-block"]) ?>
                   
                
                <p> </p>
                
                    
                       <?= Html::a('Magias', ['magias/listamagias'], ['class' => "btn btn-default btn-lg btn-block"]) ?>
                    
               
                <p> </p>               
                
                    
                       <?= Html::a('Objetos', ['objetos/listaobjetos'], ['class' => "btn btn-default btn-lg btn-block"]) ?>
                    
                
                <p> </p>
                
                    
                       <?= Html::a('Razas', ['razas/listarazas'], ['class' => "btn btn-default btn-lg btn-block"]) ?>
                   
                
                <p> </p>
                
                    
                       <?= Html::a('Trasfondos', ['trasfondos/listatrasfondos'], ['class' => "btn btn-default btn-lg btn-block"]) ?>
                    
                
                <p class="invisible"> d</p>
                <div class="boton">
                    <div class="boton-texto" style="text-align: center;">
                       PERSONAJES
                    </div>
                </div>
                <p> </p>
                       <?= Html::a('Crear Personajes', ['personajes/createpersonajepasos'], ['class' => "btn btn-default btn-lg btn-block"]) ?>
                <p> </p>
                       <?= Html::a('Ver Personajes', ['personajes/listapersonajes'], ['class' => "btn btn-default btn-lg btn-block"]) ?>
                 <p class="invisible"> d</p>
                <div class="boton">
                    <div class="boton-texto" style="text-align: center;">
                       RECOMENDACIONES
                    </div>
                </div>
                 <p> </p>
                    <?= Html::a('Recomendaciones', ['site/recomendaciones'], ['class' => "btn btn-default btn-lg btn-block"]) ?>
                <p> </p>
                <p class="invisible"> d</p>
                <div class="poderosocarusel">
                    <div id="myCarousel" class="carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                          <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                          <li data-target="#myCarousel" data-slide-to="1"></li>
                          <li data-target="#myCarousel" data-slide-to="2"></li>
                        </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner">
                          <div class="item active">
                            <?= Html::img('@web/images/Manual_1.jpeg', ['class' => 'imagenescarusel'], ['alt' => 'jester1']); ?>
                          </div>

                          <div class="item">
                            <?= Html::img('@web/images/Manual_2.jpeg', ['class' => 'imagenescarusel'], ['alt' => 'jester2']); ?>
                          </div>

                          <div class="item">
                            <?= Html::img('@web/images/Manual_3.jpeg', ['class' => 'imagenescarusel'], ['alt' => 'jester3']); ?>
                          </div>
                        </div>

                        <!-- Left and right controls -->
                        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                          <span class="glyphicon glyphicon-chevron-left"></span>
                          <span class="sr-only">Anterior</span>
                        </a>
                        <a class="right carousel-control" href="#myCarousel" data-slide="next">
                          <span class="glyphicon glyphicon-chevron-right"></span>
                          <span class="sr-only">Siguiente</span>
                        </a>
                      </div>
                    <div class="descripcion">
                        <p> Mira los manuales desde la página oficial </p>
                        <a href="https://www.dndbeyond.com/sources#Sourcebooks" class="btn btn-default" role="button">Manuales</a>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
    
