<?php
    use yii\helpers\ArrayHelper;
?>

<div class="col-sm-6">
    <div class="thumbnail habilidadesclase">
            <p>Se desbloquea a nivel <b><?= implode('</b><b>', ArrayHelper::getColumn($model->tienenclases, 'nivel_desbloque'))?></b </p>
            <h3><u><?= $model->nombre ?></u></h3>
            <div class="descripcion">
                <h5>
                    <?= $model->descripción ?>           
                </h5>
            </div>
            
    </div>
</div>
