<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Habilidadesclase */

$this->title = 'Añadir habilidad a una clase';
$this->params['breadcrumbs'][] = ['label' => 'Clases', 'url' => ['clases/listaclases']];
$this->params['breadcrumbs'][] = ['label' => 'Habilidades propias de las clases', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="habilidadesclase-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
