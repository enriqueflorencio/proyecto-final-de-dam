<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Habilidades propias de las clases';
$this->params['breadcrumbs'][] = ['label' => 'Clases', 'url' => ['clases/listaclases']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="habilidadesclase-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Añadir habilidad a una clase', ['create'], ['class' => 'btn btn-default']) ?>
        <?= Html::a('Nivel de desbloqueo dependiendo de la clase', ['tienenclase/index'], ['class' => 'btn btn-info']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [

            ['class' => 'yii\grid\ActionColumn'],
            'nombre',
            'descripción:ntext',

            
        ],
    ]); ?>


</div>
