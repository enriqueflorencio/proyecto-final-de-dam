<?php
    use yii\helpers\Html;
    use yii\widgets\ListView;
    use yii\grid\GridView;
    
    $this->title = 'Habilidades';
    $this->params['breadcrumbs'][] = ['label' => 'Clases', 'url' => ['clases/listaclases']];
    
    $this->params['breadcrumbs'][] = $this->title;
     
?>
<?php if(Yii::$app->user->isGuest): ?>
<div class="view">
    
    <?= ListView::widget([
               'dataProvider' => $dataProvider,
               'itemView' => '_habilidadesclase',
               'layout' => "\n{pager}\n{items}",
            ]);
     ?>       
</div>

<?php else: ?>
<div class="habilidadesclase-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Añadir habilidad', ['create'], ['class' => 'btn btn-default']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            
            ['class' => 'yii\grid\ActionColumn'],
            
            'nombre',
            'descripcion:ntext',

            
        ],
    ]); ?>


</div>

<?php endif; ?>
