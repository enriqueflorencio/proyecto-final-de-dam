<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Habilidadesclase */

$this->title = 'Modificar una habilidad pertenecienta a una clase: ' . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Clases', 'url' => ['clases/listaclases']];
$this->params['breadcrumbs'][] = ['label' => 'Habilidades propias de las clases', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nombre, 'url' => ['view', 'id' => $model->id_habilidadesclase]];
$this->params['breadcrumbs'][] = 'Modificar';
?>
<div class="habilidadesclase-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
