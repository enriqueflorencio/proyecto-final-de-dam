<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Clases de los personajes';
$this->params['breadcrumbs'][] = ['label' => 'Personajes', 'url' => ['personajes/listapersonajes']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="clasesdelospersonajes-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Añadir clases a un personajes', ['create'], ['class' => 'btn btn-default']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            

            ['class' => 'yii\grid\ActionColumn'],
            
            [
                'attribute' => 'id_clase',
                'label' => 'Clase',
                'value' => 'clase.nombre'
            ],
            [
                'attribute' => 'id_personaje',
                'label' => 'Personaje',
                'value' => 'personaje.nombre'
            ],
            'nivel_clase',
        ],
    ]); ?>


</div>
