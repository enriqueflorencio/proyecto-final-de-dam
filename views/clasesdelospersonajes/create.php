<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Clasesdelospersonajes */

$this->title = 'Añadir clase a un personaje';
$this->params['breadcrumbs'][] = ['label' => 'Personajes', 'url' => ['personajes/listapersonajes']];
$this->params['breadcrumbs'][] = ['label' => 'Clases de los personajes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="clasesdelospersonajes-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelClase' => $modelClase,
        'modelPersonaje' => $modelPersonaje,
        'id_personaje' => $id_personaje,
          
    ]) ?>

</div>
