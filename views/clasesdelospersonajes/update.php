<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Clasesdelospersonajes */

$this->title = 'Modificar clase del personajes: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Personajes', 'url' => ['personajes/listapersonajes']];
$this->params['breadcrumbs'][] = ['label' => 'Clases de los personajes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Modificar';
?>
<div class="clasesdelospersonajes-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelClase' => $modelClase,
        'modelPersonaje' => $modelPersonaje,
    ]) ?>

</div>
