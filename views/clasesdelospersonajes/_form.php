<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\models\Clasesdelospersonajes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="clasesdelospersonajes-form">

    <?php $form = ActiveForm::begin(); ?>
    
   <?php $modelClase = ArrayHelper::map($modelClase, 'id_clase', 'nombre') ?>
   <?php $modelPersonaje = ArrayHelper::map($modelPersonaje, 'id_personaje', 'nombre') ?>
     
    <?= $form->field($model, 'id_clase')->dropDownList($modelClase, ['prompt'=> ''])->label('Clases')?>
    
    <?= $form->field($model, 'id_personaje')->textInput(['readOnly' => true,'value' => $id_personaje])->label('Personaje')?>
    
    <?= $form->field($model, 'nivel_clase')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-default']) ?>
        <?= Html::a('Terminar', ['personajes/personaje','id' => $id_personaje],['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
