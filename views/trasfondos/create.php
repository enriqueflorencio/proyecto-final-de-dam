<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Trasfondos */

$this->title = 'Añadir trasfondo';
$this->params['breadcrumbs'][] = ['label' => 'Trasfondos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trasfondos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
