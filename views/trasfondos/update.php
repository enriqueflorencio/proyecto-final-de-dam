<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Trasfondos */

$this->title = 'Modificar trasfondo: ' . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Trasfondos', 'url' => ['trasfondos/listatrasfondos']];
$this->params['breadcrumbs'][] = ['label' => $model->nombre, 'url' => ['view', 'id' => $model->id_trasfondo]];
$this->params['breadcrumbs'][] = 'Modificar';
?>
<div class="trasfondos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
