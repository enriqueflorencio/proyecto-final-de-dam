<?php
    use yii\helpers\Html;
    use yii\widgets\ListView;
    use yii\grid\GridView;
    
    $this->title = 'Trasfondos';
    $this->params['breadcrumbs'][] = $this->title;
?>
<?php if(Yii::$app->user->isGuest): ?>

<div class="view">
    
    <div class="well well-lg">
        <h2 style="text-align: center">
            <?= Html::img('@web/images/libro.png', ['class' => 'iconosheader'], ['alt' => 'libro']); ?> 
                Trasfondos
            <?= Html::img('@web/images/libro.png', ['class' => 'iconosheader'], ['alt' => 'libro']); ?> 
            </h2>
        </div>
    
    <?= ListView::widget([
               'dataProvider' => $dataProvider,
               'itemView' => '_trasfondos',
               'layout' => "\n{pager}\n{items}",
            ]);
     ?>       
</div>

<?php else: ?>

<div class="trasfondos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Añadir trasfondo', ['create'], ['class' => 'btn btn-default']) ?>
        <?= Html::a('Ver habilidades que te da los trasfondos', ['comphabilidades/index'], ['class' => 'btn btn-info']) ?>
        <?= Html::a('Idiomas que te da los trasfondos', ['idiomastrasfondos/index'], ['class' => 'btn btn-info']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [

            ['class' => 'yii\grid\ActionColumn'],
            'nombre',
            'descripcion:ntext',
            'equipo:ntext',
            'herramientas:ntext',

            
        ],
    ]); ?>


</div>

<?php endif; ?>

