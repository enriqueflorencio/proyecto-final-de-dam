<?php
?>

<div class="col-sm-4">
    <div class="thumbnail trasfondos">
            
            <h3><u><?= $model->nombre ?></u></h3>
            
            <h6 class="texto-magia">
                <?= $model->descripcion ?>          
            </h6>
            <?php if($model->equipo): ?>
            <h5 style="text-align: justify"><b>Equipo: </b> <?= $model->equipo ?></h5>
            <?php endif; ?>
            <?php if($model->herramientas): ?>
            <h5 style="text-align: justify"><b>Herramientas: </b> <?= $model->herramientas ?></h5>
            <?php endif; ?>
    </div>
</div>