<?php

use yii\helpers\Html;

?>

<div class="col-sm-6 col-md-3">
    <div class="thumbnail usuarios">
        
            <?php if ($model->icono): ?>
                <img src="<?= $model->icono ?>" alt="icono usuario" class = >
            <?php else: ?>
                <?= Html::img('@web/images/imagen_prueba.png', ['alt' => 'imagenes por defecto'], ['class' => 'imagenespersonajesyusuarios ']) ?>
            <?php endif; ?>
            <h2><?= $model->nombre?></h2>
            <p><?= Html::a('Personajes', ['personajes/listapersonajesusuarios', 'id_usuario' => $model->id_usuario], ['class' => 'btn btn-default']) ?></p>
        
    </div>
</div>

