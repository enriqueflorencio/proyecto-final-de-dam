<?php
    use yii\helpers\Html;
    use yii\helpers\HtmlPurifier;
    use yii\widgets\ListView;
    use yii\grid\GridView;
    
    $this->title = 'Usuarios';
    $this->params['breadcrumbs'][] = $this->title;
?>
<?php if(Yii::$app->user->isGuest): ?>

    <div class="well well-lg"><h2 style="text-align: center">
        <?= Html::img('@web/images/usuario.png', ['class' => 'iconosheader'], ['alt' => 'usuario']); ?> 
            Usuarios
        <?= Html::img('@web/images/usuario.png', ['class' => 'iconosheader'], ['alt' => 'usuario']); ?> 
        </h2>
    </div>

    <div class="view">

        <?= ListView::widget([
                   'dataProvider' => $dataProvider,
                   'itemView' => '_usuario',
                   'layout' => "\n{pager}\n{items}",
                ]);
         ?>       
    </div>

<?php else: ?>

    <div class="usuarios-index">

        <h1><?= Html::encode($this->title) ?></h1>

        <p>
            <?= Html::a('Añadir usuario', ['create'], ['class' => 'btn btn-default']) ?>
        </p>


        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [

                ['class' => 'yii\grid\ActionColumn'],
                'nombre',
                [

                    'attribute' => 'icono',

                    'format' => 'html',

                    'label' => 'Icono',

                    'value' => function ($data) {

                        return Html::img($data['icono'],

                            ['width' => '100px']);

                    },

                ],
                'email:email',
                'contraseña',

            ],
        ]); ?>


    </div>

<?php endif; ?>


