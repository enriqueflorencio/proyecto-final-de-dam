<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Clasecomphabilidades */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="clasecomphabilidades-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?php $modelClase = ArrayHelper::map($modelClase, 'id_clase', 'nombre') ?>

    <?= $form->field($model, 'id_clase')->dropDownList($modelClase, ['prompt'=> ''])->label('Clases') ?>

    <?= $form->field($model, 'habilidad')->dropDownList(['Atletismo', 'Conociemiento Arcano', 'Engaño', 'Historia', 'Interpretación', 
        'Intimidacion', 'Investigación', 'Juego de manos', 'Medicina', 'Naturaleza', 'Percepción', 'Perspicacia', 'Persuasión', 'Religión',
        'Sigilo', 'Supervivencia', 'Trato con animales'],['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
