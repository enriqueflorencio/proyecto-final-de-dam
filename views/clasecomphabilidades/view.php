<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Clasecomphabilidades */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Clases', 'url' => ['clases/listaclases']];
$this->params['breadcrumbs'][] = ['label' => 'Competencia con habilidades que te da una clase', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="clasecomphabilidades-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Modificar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estás seguro?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'id_clase',
                'label' => 'Clase',
                'value' => $model->clase->nombre,
            ],
            'habilidad',
        ],
    ]) ?>

</div>
