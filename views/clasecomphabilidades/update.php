<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Clasecomphabilidades */

$this->title = 'Modificar que habilidad tiene competencia una clase: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Clases', 'url' => ['clases/listaclases']];
$this->params['breadcrumbs'][] = ['label' => 'Competencia con habilidades que te da una clase', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Modificar';
?>
<div class="clasecomphabilidades-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
