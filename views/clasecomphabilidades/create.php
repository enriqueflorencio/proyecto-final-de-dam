<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Clasecomphabilidades */

$this->title = 'Añadir que habilidad tiene competencia una clase';
$this->params['breadcrumbs'][] = ['label' => 'Clases', 'url' => ['clases/listaclases']];
$this->params['breadcrumbs'][] = ['label' => 'Competencia con habilidades que te da una clase', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="clasecomphabilidades-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelClase' => $modelClase,
    ]) ?>

</div>
