<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use kartik\rating\StarRating;

/* @var $this yii\web\View */
/* @var $model app\models\Recomrazas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="recomrazas-form">

    <?php $form = ActiveForm::begin(); ?>
    <?php $modelUsuario = ArrayHelper::map($modelUsuario, 'id_usuario', 'nombre'); ?>
    
    <?php $modelRaza = ArrayHelper::map($modelRaza, 'id_raza', 'nombre'); ?>
    
    <?= $form->field($model, 'id_usuario')->dropDownList($modelUsuario, ['prompt' => ''])->label('Usuario') ?>

    <?= $form->field($model, 'id_raza')->dropDownList($modelRaza, ['prompt' => ''])->label('Raza') ?>

    <?= $form->field($model, 'puntuación')->widget(StarRating::classname(), [
    'pluginOptions' => ['step' => 0.1]
]); ?>

    <?= $form->field($model, 'reseña')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
