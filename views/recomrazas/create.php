<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Recomrazas */

$this->title = 'Añadir una reseña a una raza';
$this->params['breadcrumbs'][] = ['label' => 'Recomendaciones', 'url' => ['site/recomendaciones']];
$this->params['breadcrumbs'][] = ['label' => 'Media Razas', 'url' => ['mediarazas']];
$this->params['breadcrumbs'][] = ['label' => 'Reseña Razas', 'url' => ['todaspuntuacionesrazas']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="recomrazas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelUsuario' => $modelUsuario,
        'modelRaza' => $modelRaza,
    ]) ?>

</div>
