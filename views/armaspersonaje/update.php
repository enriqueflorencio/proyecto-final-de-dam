<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Armaspersonaje */

$this->title = 'Modificar el arma a un personaje: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Personajes', 'url' => ['personajes/listapersonajes']];
$this->params['breadcrumbs'][] = ['label' => 'Armas de los personajes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Modificar';
?>
<div class="armaspersonaje-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelArmas' => $modelArmas,
        'modelPersonaje' => $modelPersonaje,
    ]) ?>

</div>
