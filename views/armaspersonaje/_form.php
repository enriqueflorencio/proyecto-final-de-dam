<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Armaspersonaje */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="armaspersonaje-form">

    <?php $form = ActiveForm::begin(); ?>
    <?php $modelArmas = ArrayHelper::map($modelArmas, 'id_arma', 'nombre'); ?>
    <?php $modelPersonaje = ArrayHelper::map($modelPersonaje, 'id_personaje', 'nombre') ?>
    <?= $form->field($model, 'id_personaje')->dropDownList($modelPersonaje, ['prompt'=> ''])->label('Personajes') ?>

    <?= $form->field($model, 'id_arma')->dropDownList($modelArmas, ['prompt'=> ''])->label('Armas') ?>

    <?= $form->field($model, 'bonificador')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
