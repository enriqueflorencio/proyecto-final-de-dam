<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Armaspersonaje */

$this->title = 'Añadir un arma a un personaje';
$this->params['breadcrumbs'][] = ['label' => 'Personajes', 'url' => ['personajes/index']];
$this->params['breadcrumbs'][] = ['label' => 'Armas de los personajes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="armaspersonaje-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelArmas' => $modelArmas,
        'modelPersonaje' => $modelPersonaje,
    ]) ?>

</div>
