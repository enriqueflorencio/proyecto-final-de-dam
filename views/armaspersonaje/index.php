<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Armas de los personajes';
$this->params['breadcrumbs'][] = ['label' => 'Personajes', 'url' => ['personajes/listapersonajes']];
$this->params['breadcrumbs'][] = $this->title; //La barrita de navegación
?>
<div class="armaspersonaje-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Añadir un arma a un personaje', ['create'], ['class' => 'btn btn-default']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            
            ['class' => 'yii\grid\ActionColumn'],
            [
                'attribute' => 'id_personaje',
                'value' => 'personaje.nombre'
             ],
            [
                'attribute' => 'id_arma',
                'value' => 'arma.nombre'
             ],
            'bonificador',
        ],
    ]); ?>


</div>
