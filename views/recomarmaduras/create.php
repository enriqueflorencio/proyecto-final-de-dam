<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Recomarmaduras */

$this->title = 'Añadir reseña de una armadura';
$this->params['breadcrumbs'][] = ['label' => 'Recomendaciones', 'url' => ['site/recomendaciones']];
$this->params['breadcrumbs'][] = ['label' => 'Reseña Armaduras', 'url' => ['todaspuntuacionesarmaduras']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="recomarmaduras-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelUsuario' => $modelUsuario,
        'modelArmaduras' => $modelArmadura,
        
    ]) ?>

</div>
