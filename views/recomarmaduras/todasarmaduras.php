<?php
    
    use yii\grid\GridView;
    use yii\widgets\ListView;
    use yii\helpers\Html;
    
    $this->title = 'Reseña Armaduras';
    $this->params['breadcrumbs'][] = ['label' => 'Recomendaciones', 'url' => ['site/recomendaciones']];
    $this->params['breadcrumbs'][] = $this->title;
?>

<?php if(Yii::$app->user->isGuest): ?>

<div class="view">
    
    <?= ListView::widget([
               'dataProvider' => $dataProvider,
               'itemView' => '_todasarmaduras',
               'layout' => "\n{pager}\n{items}",
            ]);
     ?>       
</div>

<?php else: ?>
<div class="recomarmaduras-index">
    
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Añadir una reseña', ['create'], ['class' => 'btn btn-default']) ?>
    </p>
    
    <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\ActionColumn'],

                [
                    'attribute' => 'id_usuario',
                    'label' => 'Usuario',
                    'value' => 'usuario.nombre',
                ],
                [
                    'attribute' => 'id_armadura',
                    'label' => 'Armadura',
                    'value' => 'armadura.nombre',
                ],
                'puntuación',
                'reseña:ntext',


            ],
        ]); ?>

</div>
<?php endif; ?>

