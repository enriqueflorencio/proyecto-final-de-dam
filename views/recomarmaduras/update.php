<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Recomarmaduras */

$this->title = 'Modificar reseña de una armadura: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Recomendaciones', 'url' => ['site/recomendaciones']];
$this->params['breadcrumbs'][] = ['label' => 'Reseñas Armaduras', 'url' => ['todaspuntuacionesarmaduras']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Modificar';
?>
<div class="recomarmaduras-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelUsuario' => $modelUsuario,
        'modelArmadura' => $modelArmadura,
    ]) ?>

</div>
