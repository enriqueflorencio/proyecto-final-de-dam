<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Recomdotes */

$this->title = 'Añadir reseña a una dote';
$this->params['breadcrumbs'][] = ['label' => 'Recomendaciones', 'url' => ['site/recomendaciones']];
$this->params['breadcrumbs'][] = ['label' => 'Reseña Dotes', 'url' => ['todaspuntuacionesdotes']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="recomdotes-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelUsuario' => $modelUsuario,
        'modelDote' => $modelDote,
    ]) ?>

</div>
