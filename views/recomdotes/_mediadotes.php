<?php
    use kartik\rating\StarRating;
?>

<div class="col-sm-12">
    <div class="thumbnail recomarmaduras">
        <h3><u><?= $model->dote->titulo ?></u></h3>
            
            <p> <?= StarRating::widget([
                                        'name' => 'rating_33',
                                        'value' => $model->mediapuntuación,
                                        'language' => 'es',
                                        'pluginOptions' => ['displayOnly' => true],
                                    ]); ?></p>
    </div>
</div>
