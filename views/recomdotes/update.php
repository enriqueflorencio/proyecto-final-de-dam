<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Recomdotes */

$this->title = 'Modificar reseña a una dote: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Recomendaciones', 'url' => ['site/recomendaciones']];
$this->params['breadcrumbs'][] = ['label' => 'Reseña Dotes', 'url' => ['todaspuntuacionesdotes']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Modificar';
?>
<div class="recomdotes-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
