<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use kartik\rating\StarRating;

/* @var $this yii\web\View */
/* @var $model app\models\Recomdotes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="recomdotes-form">

    <?php $form = ActiveForm::begin(); ?>
    <?php $modelUsuario = ArrayHelper::map($modelUsuario, 'id_usuario', 'nombre'); ?>
    
    <?php $modelDote = ArrayHelper::map($modelDote, 'id_dote', 'titulo'); ?>
    
    <?= $form->field($model, 'id_usuario')->dropDownList($modelUsuario, ['prompt' => ''])->label('Usuario') ?>

    <?= $form->field($model, 'id_dote')->dropDownList($modelDote, ['prompt' => ''])->label('Dote') ?>

    <?= $form->field($model, 'puntuación')->widget(StarRating::classname(), [
    'pluginOptions' => ['step' => 0.1]
]); ?>

    <?= $form->field($model, 'reseña')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
