<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Armaduras */

$this->title = 'Añadir Armadura';
$this->params['breadcrumbs'][] = ['label' => 'Armaduras', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="armaduras-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
