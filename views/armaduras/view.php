<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Armaduras */

$this->params['breadcrumbs'][] = ['label' => 'Armaduras', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->nombre;
\yii\web\YiiAsset::register($this);
?>
<div class="armaduras-view">

    <h1><?= Html::encode($model->nombre) ?></h1>

    <p>
        <?= Html::a('Modificar', ['update', 'id' => $model->id_armaduras], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id_armaduras], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estás seguro?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'nombre',
            'imagen',
            'descripcion:ntext',
            'tipo',
            'ca',
            'oro',
            [
                    'attribute' => 'mágico',
                    'label'=>'¿Arma Mágica?',
                    'format'=>'raw',
                    'value' => function($model) { return $model->mágico == 0 ? 'No' : 'Sí';},
            ],
            [
                    'attribute' => 'desventaja_sigilo',
                    'label'=>'¿Desventaja ante el sigilo?',
                    'format'=>'raw',
                    'value' => function($model) { return $model->desventaja_sigilo == 0 ? 'No' : 'Sí';},
            ],
        ],
    ]) ?>

</div>
