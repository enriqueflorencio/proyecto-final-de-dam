<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Armaduras */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="armaduras-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'imagen')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'descripcion')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'tipo')->widget(Select2::classname(), [
    'data' => ['ligera', 'media', 'pesada'],
    'options' => ['placeholder' => 'Tipo de armadura ...'],
    'pluginOptions' => [
        'allowClear' => true
    ],
]); ?>

    <?= $form->field($model, 'ca')->textInput() ?>

    <?= $form->field($model, 'oro')->textInput() ?>

    <?= $form->field($model, 'mágico')->checkbox()?>

    <?= $form->field($model, 'desventaja_sigilo')->checkbox()?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
