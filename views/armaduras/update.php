<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Armaduras */

$this->title = 'Modificar Armadura: ' . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Armaduras', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nombre, 'url' => ['view', 'id' => $model->id_armaduras]];
$this->params['breadcrumbs'][] = 'Modificar';
?>
<div class="armaduras-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
