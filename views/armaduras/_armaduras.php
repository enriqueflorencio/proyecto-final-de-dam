<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

?>

<div class="col-sm-4">
    <div class="thumbnail armaduras">
        
            <h4><u><?= $model->nombre ?></u></h4>
            <h5>
                <i><?= $model->descripcion ?></i>
            
            </h5>
            <h5><b>Clase de armadura:</b> <?= $model->ca ?>
                <?= Html::img('@web/images/escudo.png', ['class' => 'iconos'], ['alt' => 'escudo']); ?>
            </h5>
            <h5><b>Tipo de armadura: <?= $model->tipo ?></b></h5>
            <?php if ($model->desventaja_sigilo == 1): ?>
                <h5><b>Desventaja ante el sigilo:</b> 
                    <font color="red">Sí</font>
                </h5>
            <?php else: ?>
                <h5><b>Desventaja ante el sigilo:</b>
                    <font color="green">No</font>
                </h5>
            <?php endif; ?>
            <?php if ($model->mágico == 1): ?>
                <h5><b>Mágico:</b> 
                    <font color="green">Sí</font>
                </h5>
            <?php else: ?>
                <h5><b>Mágico:</b>
                    <font color="red">No</font>
                </h5>
            <?php endif; ?>
            <p><b>Valor:</b> <?= $model->oro ?> 
                <?= Html::img('@web/images/moneda-oro.png', ['class' => 'iconos'], ['alt' => 'moneda de oro']); ?>
            </p>

    </div>
</div>

