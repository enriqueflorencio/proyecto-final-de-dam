<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Salvaciones */

$this->title = 'Añadir Salvaciones';
$this->params['breadcrumbs'][] = ['label' => 'Clases', 'url' => ['clases/listaclases']];
$this->params['breadcrumbs'][] = ['label' => 'Salvaciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="salvaciones-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelClase' => $modelClase,
    ]) ?>

</div>
