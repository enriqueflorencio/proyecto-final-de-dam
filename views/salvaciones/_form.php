<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Salvaciones */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="salvaciones-form">

    <?php $form = ActiveForm::begin(); ?>
    <?php $modelClase = ArrayHelper::map($modelClase, 'id_clase', 'nombre'); ?>
    <?= $form->field($model, 'id_clase')->dropDownList($modelClase, ['prompt' => ''])->label("Clase") ?>

    <?= $form->field($model, 'salvaciones')->dropDownList(['Fuerza', 'Destreza', 'Constitución', 'Inteligencia', 'Sabíduria', 'Carisma'],['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
