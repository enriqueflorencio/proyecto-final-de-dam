<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Salvaciones */

$this->title = 'Update Salvaciones: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Clases', 'url' => ['clases/listaclases']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="salvaciones-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelClase' => $modelClase,
    ]) ?>

</div>
