<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Recommagias */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Recomendaciones', 'url' => ['site/recomendaciones']];
$this->params['breadcrumbs'][] = ['label' => 'Media Magias', 'url' => ['mediamagias']];
$this->params['breadcrumbs'][] = ['label' => 'Reseña Magias', 'url' => ['todaspuntuacionesmagias']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="recommagias-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Modificar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estás seguro?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'id_usuario',
                'label' => 'Usuario',
                'value' => $model->usuario->nombre,
            ],
            [
                'attribute' => 'id_magia',
                'label' => 'Magia',
                'value' => $model->magia->nombre,
            ],
            'puntuación',
            'reseña:ntext',
        ],
    ]) ?>

</div>
