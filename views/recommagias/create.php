<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Recommagias */

$this->title = 'Añadir reseña a una magia';
$this->params['breadcrumbs'][] = ['label' => 'Recomendaciones', 'url' => ['site/recomendaciones']];
$this->params['breadcrumbs'][] = ['label' => 'Media Magias', 'url' => ['mediamagias']];
$this->params['breadcrumbs'][] = ['label' => 'Reseña Magias', 'url' => ['todaspuntuacionesmagias']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="recommagias-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelUsuario' => $modelUsuario,
        'modelMagia' => $modelMagia,
    ]) ?>

</div>
