<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

?>

<div class="col-sm-4">
    <div class="thumbnail razas" style="text-align: left">
            
            <?php if ($model->imagen): ?>
                <img class="img-thumbnail" align="center" src=<?= $model->imagen ?>>
            <?php endif; ?>
            <h4 style="text-align: center">
                <u><?= $model->nombre ?></u>
                
            </h4>
            <h5 style="text-align: center"><?= $model->descripcion ?></h5>
            <h5><b>Velocidad:</b> <?= $model->velocidad ?>'
                <?= Html::img('@web/images/piernas.png', ['class' => 'iconos'], ['alt' => 'piernas']); ?>
            </h5>
            <h5><b>Bonos:</b>  
                <ul>
                    <?php if($model->bonofuerza != 0): ?>
                        <li>+<?= $model->bonofuerza ?> en fuerza</li>
                    <?php endif; ?>
                    <?php if($model->bonodestreza != 0): ?>
                        <li>+<?= $model->bonodestreza ?> en destreza</li>
                    <?php endif; ?>
                    <?php if($model->bonoconstitucion != 0): ?>
                        <li>+<?= $model->bonoconstitucion ?> en constitución</i>
                    <?php endif; ?>
                    <?php if($model->bonointeligencia != 0): ?>
                        <li>+<?= $model->bonointeligencia ?> en inteligencia</li>
                    <?php endif; ?>
                    <?php if($model->bonosabiduria != 0): ?>
                        <li>+<?= $model->bonosabiduria ?> en sabiduría</li>
                    <?php endif; ?>
                    <?php if($model->bonocarisma != 0): ?>
                        <li>+<?= $model->bonocarisma ?> en carisma</li>
                    <?php endif; ?>
                </ul>
            </h5>
            <h5><b>Idiomas:</b>
                <ul>
                    <li><?= implode('</li><li>', ArrayHelper::getColumn($model->idiomasdelarazas, 'idioma'))?></li>
                </ul>
            </h5>
            <h5><b>Rasgos:</b>
                    <ul>
                        <li><?= implode('</li><li>', ArrayHelper::getColumn($model->habilidadesrazas, 'nombre'))?></li>
                    </ul>
            </h5>
            <p class="botonrazas">
            <?= Html::a('Rasgos', ['habilidadesraza/listarasgossegunraza', 'id' => $model->id_raza], ['class' => 'btn btn-default']) ?>
            </p>
    </div>
</div>

