<?php
    use yii\helpers\Html;
    use yii\widgets\ListView;
    use yii\grid\GridView;
    
   $this->title = 'Razas';
   $this->params['breadcrumbs'][] = $this->title;
?>

<?php if(Yii::$app->user->isGuest): ?>

    <div class="view">
        
        <div class="well well-lg"><h2 style="text-align: center">
            <?= Html::img('@web/images/espada.png', ['class' => 'iconosheader'], ['alt' => 'espada']); ?> 
                Razas
            <?= Html::img('@web/images/espada.png', ['class' => 'iconosheader'], ['alt' => 'espada']); ?> 
            </h2>
        </div>
        
        <?= Html::a('Todos los rasgos y su descripción', ['habilidadesraza/listahabilidadesraza'], ['class' => 'btn btn-default']) ?>
        <p> </p>
        <?= ListView::widget([
                   'dataProvider' => $dataProvider,
                   'itemView' => '_razas',
                   'layout' => "\n{pager}\n{items}",
                ]);
         ?>       
    </div>

<?php else: ?>

<div class="razas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Añadir raza', ['create'], ['class' => 'btn btn-default']) ?>
        <?= Html::a('Idiomas de las razas', ['idiomasdelaraza/index'], ['class' => 'btn btn-info']) ?>
        <?= Html::a('Rasgos de las razas', ['habilidadesraza/listahabilidadesraza'], ['class' => 'btn btn-warning']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\ActionColumn'],
            
            'nombre',
            'descripcion:ntext',
            [

                    'attribute' => 'imagen',

                    'format' => 'html',

                    'label' => 'Imagen',

                    'value' => function ($data) {

                        return Html::img($data['imagen'],

                            ['width' => '100px']);

                    },
             ],
            'velocidad',
            'bonofuerza',
            'bonodestreza',
            'bonoconstitucion',
            'bonointeligencia',
            'bonosabiduria',
            'bonocarisma',
            
        ],
    ]); ?>


</div>

<?php endif; ?>

