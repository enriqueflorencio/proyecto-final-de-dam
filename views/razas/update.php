<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Razas */

$this->title = 'Modificar raza: ' . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Razas', 'url' => ['razas/listarazas']];
$this->params['breadcrumbs'][] = ['label' => $model->nombre, 'url' => ['view', 'id' => $model->id_raza]];
$this->params['breadcrumbs'][] = 'Modificar';
?>
<div class="razas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
