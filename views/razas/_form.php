<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Razas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="razas-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'descripcion')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'imagen')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'velocidad')->textInput() ?>

    <?= $form->field($model, 'bonofuerza')->textInput() ?>

    <?= $form->field($model, 'bonodestreza')->textInput() ?>

    <?= $form->field($model, 'bonoconstitucion')->textInput() ?>

    <?= $form->field($model, 'bonointeligencia')->textInput() ?>

    <?= $form->field($model, 'bonosabiduria')->textInput() ?>

    <?= $form->field($model, 'bonocarisma')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
