<?php
    use yii\helpers\Html;
    use yii\helpers\HtmlPurifier;
    use yii\widgets\ListView;
    use yii\grid\GridView;
    
    $this->title = 'Personajes';
    $this->params['breadcrumbs'][] = $this->title;
?>
<?php if(Yii::$app->user->isGuest): ?>
    <div class="well well-lg"><h2 style="text-align: center">
        <?= Html::img('@web/images/personaje.png', ['class' => 'iconosheader'], ['alt' => 'personaje']); ?> 
            Personajes
        <?= Html::img('@web/images/personaje.png', ['class' => 'iconosheader'], ['alt' => 'personaje']); ?> 
        </h2>
    </div>
    <p>
        
        <div class="dropdown">
            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
              Filtrar
              <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
              <li><?= Html::a('Recomendados', ['personajes/personajesrecomendados']) ?></li>
              <li><?= Html::a('No Recomendados', ['personajes/personajesnorecomendados']) ?></li>
              <li role="separator" class="divider"></li>
              <li><?= Html::a('Todos', ['personajes/listapersonajes']) ?></li>
            </ul>
            <?= Html::a('Añadir personaje', ['personajes/createpersonajepasos'], ['class' => 'btn btn-default']) ?>
        </div>
        
    </p>
     
    <div class="view">

        <?= ListView::widget([
                   'dataProvider' => $dataProvider,
                   'itemView' => '_personajes',
                   'layout' => "\n{pager}\n{items}",
                ]);
         ?>       
    </div>

<?php else: ?>
    
    <div class="personajes-index">

        <h1><?= Html::encode($this->title) ?></h1>

        <p>
            <?= Html::a('Añadir personaje', ['personajes/createpersonajepasos'], ['class' => 'btn btn-default']) ?>
            <?= Html::a('Objetos de los personajes', ['objetosdelpersonaje/index'], ['class' => 'btn btn-info']) ?>
            <?= Html::a('Armas de los personajes', ['armaspersonaje/index'], ['class' => 'btn btn-info']) ?>
            <?= Html::a('Clases de los personajes', ['clasesdelospersonajes/index'], ['class' => 'btn btn-info']) ?>
            <?= Html::a('Idiomas de los personajes', ['idiomaspersonaje/index'], ['class' => 'btn btn-info']) ?>
            <?= Html::a('Dotes de los personajes', ['dotesdelospersonajes/index'], ['class' => 'btn btn-info']) ?>
        </p>
        <p> 
            <?= Html::a('Lista de hechizos de los personajes', ['magiaspersonajes/index'], ['class' => 'btn btn-info']) ?>
        </p>

        
        <h2>Básico</h2>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\ActionColumn'],
                'nombre',
                [
                    'attribute' => 'recomendado',
                    'label'=>'¿Recomendado?',
                    'format'=>'raw',
                    'value' => function($model) { return $model->recomendado == 0 ? 'No' : 'Sí';},
                ],
                [
                    'attribute' => 'id_usuario',
                    'value' => 'usuario.nombre'
                 ],
                [
                    'attribute' => 'id_armadura',
                    'value' => 'armadura.nombre'
                 ],
                [
                    'attribute' => 'id_raza',
                    'value' => 'raza.nombre'
                 ],
                'ca',
                'descripción:ntext',
                [

                    'attribute' => 'imagen',

                    'format' => 'html',

                    'label' => 'Imagen',

                    'value' => function ($data) {

                        return Html::img($data['imagen'],

                            ['width' => '100px']);

                    },

                ],                                           
                'nivel',
                'percepción_pasiva',
                
            ],
        ]); ?>
        <h2> Trasfondo </h2>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                'nombre',
                [
                    'attribute' => 'vivo',
                    'label'=>'¿Vivo?',
                    'format'=>'raw',
                    'value' => function($model) { return $model->recomendado == 0 ? 'No' : 'Sí';},
                ],
                'descripción:ntext',
                'biografia:ntext',
                'edad',
                [
                    'attribute' => 'id_trasfondo',
                    'value' => 'trasfondo.nombre'
                 ],
                ],
                ]);
                ?>
        <h2> Estadísticas </h2>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                'nombre',
                'fuerza',
                'destreza',
                'constitucion',
                'inteligencia',
                'sabiduria',
                'carisma',
                ],
                ]);
                ?>
        <h2> Modificadores de las estadísticas </h2>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                'nombre',
                'modfuerza',
                'moddestreza',
                'modconstitucion',
                'modinteligencia',
                'modsabiduria',
                'modcarisma',
                ],
                ]);
                ?>
        <h2> Salvaciones </h2>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                'nombre',              
                'salvfuerza',
                'salvdestreza',
                'salvconstitucion',
                'salvinteligencia',
                'salvsabiduria',
                'salvcarisma',
                ],
                ]);
                ?>
        <h2> Habilidades</h2>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                'nombre',
                'acrobacias',
                'atletismo',
                'conoarcano',
                'engaño',
                'historia',
                'interpretacion',
                'intimidacion',
                'investigacion',
                ],
                ]);
                ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                'nombre',
                'juego_de_manos',
                'medicina',
                'naturaleza',
                'percepcion',
                'perspicacia',
                'persuasión',
                'religion',
                'sigilo',
                'supervivencia',
                'trato_animales',
                ],
                ]);
                ?>
    </div>
    
<?php endif; ?>