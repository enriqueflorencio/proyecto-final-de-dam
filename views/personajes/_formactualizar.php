<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use kartik\file\FileInput;
use kartik\select2\Select2;
use kartik\checkbox\CheckboxX;

/* @var $this yii\web\View */
/* @var $model app\models\Personajes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="personajes-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php $modelUsuario = ArrayHelper::map($modelUsuario, 'id_usuario', 'nombre'); ?>
    <?php $modelRaza = ArrayHelper::map($modelRaza, 'id_raza', 'nombre'); ?>
    <?php $modelArmadura = ArrayHelper::map($modelArmadura, 'id_armaduras', 'nombre'); ?>
    <?php $modelTrasfondo = ArrayHelper::map($modelTrasfondo, 'id_trasfondo', 'nombre'); ?>
    
    <?= $form->field($model, 'id_usuario')->dropDownList($modelUsuario,['prompt' => ''])->label('Usuarios') ?>

    <?= $form->field($model, 'id_armadura')->dropDownList($modelArmadura,['prompt' => ''])->label('Armaduras') ?>

    <?= $form->field($model, 'id_raza')->dropDownList($modelRaza,['prompt' => ''])->label('Razas') ?>

    <?= $form->field($model, 'id_trasfondo')->dropDownList($modelTrasfondo,['prompt' => ''])->label('Trasfondo') ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'imagen')->textInput()->label('Introduzca la ruta de la imagen') ?>

    <?= $form->field($model, 'vivo')->checkbox() ?>

    <?= $form->field($model, 'biografia')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'edad')->textInput() ?>

    <?= $form->field($model, 'nivel')->textInput() ?>

    <?= $form->field($model, 'alineamineto')->dropDownList([ 'Legal bueno' => 'Legal bueno', 'Neutral bueno' => 'Neutral bueno', 'Caótico bueno' => 'Caótico bueno', 'Legal neutral' => 'Legal neutral', 'Neutral' => 'Neutral', 'Caótico neutral' => 'Caótico neutral', 'Legal malvado' => 'Legal malvado', 'Neutral malvado' => 'Neutral malvado', 'Caótico malvado' => 'Caótico malvado', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'fuerza')->textInput() ?>

    <?= $form->field($model, 'destreza')->textInput() ?>

    <?= $form->field($model, 'constitucion')->textInput() ?>

    <?= $form->field($model, 'inteligencia')->textInput() ?>

    <?= $form->field($model, 'sabiduria')->textInput() ?>

    <?= $form->field($model, 'carisma')->textInput() ?>

    <?= $form->field($model, 'modfuerza')->textInput() ?>

    <?= $form->field($model, 'moddestreza')->textInput() ?>

    <?= $form->field($model, 'modconstitucion')->textInput() ?>

    <?= $form->field($model, 'modinteligencia')->textInput() ?>

    <?= $form->field($model, 'modsabiduria')->textInput() ?>

    <?= $form->field($model, 'modcarisma')->textInput() ?>

    <?= $form->field($model, 'vida')->textInput() ?>

    <?= $form->field($model, 'iniciativa')->textInput() ?>

    <?= $form->field($model, 'inspiracion')->checkbox() ?>

    <?= $form->field($model, 'ca')->textInput() ?>

    <?= $form->field($model, 'escudo')->checkbox() ?> 
    
    <?= $form->field($model, 'oro')->textInput() ?>

    <?= $form->field($model, 'plata')->textInput() ?>

    <?= $form->field($model, 'cobre')->textInput() ?>

    <?= $form->field($model, 'velocidad')->textInput() ?>

    <?= $form->field($model, 'salvfuerza')->textInput() ?>

    <?= $form->field($model, 'salvdestreza')->textInput() ?>

    <?= $form->field($model, 'salvconstitucion')->textInput() ?>

    <?= $form->field($model, 'salvinteligencia')->textInput() ?>

    <?= $form->field($model, 'salvsabiduria')->textInput() ?>

    <?= $form->field($model, 'salvcarisma')->textInput() ?>

    <?= $form->field($model, 'bono_competencia')->textInput() ?>

    <?= $form->field($model, 'acrobacias')->textInput() ?>

    <?= $form->field($model, 'atletismo')->textInput() ?>

    <?= $form->field($model, 'conoarcano')->textInput() ?>

    <?= $form->field($model, 'engaño')->textInput() ?>

    <?= $form->field($model, 'historia')->textInput() ?>

    <?= $form->field($model, 'interpretacion')->textInput() ?>

    <?= $form->field($model, 'intimidacion')->textInput() ?>

    <?= $form->field($model, 'investigacion')->textInput() ?>

    <?= $form->field($model, 'juego_de_manos')->textInput() ?>

    <?= $form->field($model, 'medicina')->textInput() ?>

    <?= $form->field($model, 'naturaleza')->textInput() ?>

    <?= $form->field($model, 'percepcion')->textInput() ?>

    <?= $form->field($model, 'perspicacia')->textInput() ?>

    <?= $form->field($model, 'persuasión')->textInput() ?>

    <?= $form->field($model, 'religion')->textInput() ?>

    <?= $form->field($model, 'sigilo')->textInput() ?>

    <?= $form->field($model, 'supervivencia')->textInput() ?>

    <?= $form->field($model, 'trato_animales')->textInput() ?>

    <?= $form->field($model, 'percepción_pasiva')->textInput() ?>

    <?= $form->field($model, 'salv_conjuto')->textInput() ?>

    <?= $form->field($model, 'mod_ataque_conjuro')->textInput() ?>

    <?= $form->field($model, 'notas')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-default']) ?>
        <?= Html::a('Clases', ['clasesdelospersonajes/create', 'id_personaje'=>$model->id_personaje], ['class' => "btn btn-default"]) ?> 
        <?= Html::a('Armas', ['armaspersonaje/create'], ['class' => "btn btn-default"]) ?> 
        <?= Html::a('Objetos', ['objetosdelpersonaje/create'], ['class' => "btn btn-default"]) ?>
        <?= Html::a('Magias', ['magiaspersonajes/create'], ['class' => "btn btn-default"]) ?> 
        <?= Html::a('Dotes', ['dotesdelospersonajes/create'], ['class' => "btn btn-default"]) ?> 
        <?= Html::a('Idiomas', ['idiomaspersonaje/create'], ['class' => "btn btn-default"]) ?> 
    </div>

    <?php ActiveForm::end(); ?>

</div>
