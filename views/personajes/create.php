<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Personajes */

$this->title = 'Añadir Personaje';
$this->params['breadcrumbs'][] = ['label' => 'Personajes', 'url' => ['personajes/listapersonajes']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="personajes-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_formcrear', [
        'model' => $model,
        'modelUsuario' => $modelUsuario,
        'modelArmadura' => $modelArmadura,
        'modelRaza' => $modelRaza,
        'modelTrasfondo' => $modelTrasfondo,
    ]) ?>

</div>
