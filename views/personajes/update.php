<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Personajes */

$this->title = 'Modificar personaje: ' . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Personajes', 'url' => ['personajes/listapersonajes']];
$this->params['breadcrumbs'][] = ['label' => $model->nombre, 'url' => ['view', 'id' => $model->id_personaje]];
$this->params['breadcrumbs'][] = 'Modificar';
?>
<div class="personajes-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_formactualizar', [
        'model' => $model,
        'modelUsuario' => $modelUsuario,
        'modelArmadura' => $modelArmadura,
        'modelRaza' => $modelRaza,
        'modelTrasfondo' => $modelTrasfondo,
    ]) ?>

</div>
