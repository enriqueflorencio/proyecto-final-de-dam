<?php

use yii\helpers\Html;
/*use yii\widgets\DetailView;*/
use kartik\detail\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Personajes */

$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Personajes', 'url' => ['personajes/listapersonajes']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="personajes-view">
    <p>
        <?= Html::a('Modificar', ['update', 'id' => $model->id_personaje], ['class' => 'btn btn-default']) ?>
        <?php if(Yii::$app->user->isGuest): ?>
        
        <?php else: ?>
            <?= Html::a('Eliminar', ['delete', 'id' => $model->id_personaje], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => '¿Estás seguro?',
                    'method' => 'post',
                ],
            ]) ?>
        <?php endif; ?>
    </p>
        <?= DetailView::widget([
        'model'=>$model,
        'hover'=>true,
        'mode'=>DetailView::MODE_VIEW,
        'panel'=>[
            'heading'=>'Ficha de ' . $model->nombre,
            'type'=>DetailView::TYPE_DANGER,
        ],
        'buttons1' => ' ',
        'attributes'=>[
            
            [
                'group'=>true,
                'label'=>'Información básica',
                'rowOptions'=>['class'=>'btn-default']
            ],
                [
                    'columns' => [
                        [
                            'attribute'=>'nombre', 
                            'label'=>'Nombre',
                            'displayOnly'=>true,
                            'valueColOptions'=>['style'=>'width:30%']
                        ],
                        [
                            'attribute'=>'raza', 
                            'format'=>'raw', 
                            'value'=>$model->raza->nombre,
                            'valueColOptions'=>['style'=>'width:30%'], 
                            'displayOnly'=>true
                        ],
                    ],
                ],
                [
                    'columns' => [
                        [
                            'attribute'=>'trasfondo', 
                            'label'=>'Trasfondo',
                            'value' =>$model->trasfondo->nombre,
                            'displayOnly'=>true,
                            'valueColOptions'=>['style'=>'width:30%']
                        ],
                        [
                            'attribute'=>'alineamineto', 
                            'label' => 'Alineamiento',
                            'format'=>'raw', 
                            'valueColOptions'=>['style'=>'width:30%'], 
                            'displayOnly'=>true
                        ],
                    ],
                ],
                [
                    'columns' => [
                        [
                            'attribute'=>'nivel',
                            'displayOnly'=>true,
                            'valueColOptions'=>['style'=>'width:30%']
                        ],
                        [
                            'attribute'=>'bono_competencia', 
                            'format'=>'raw', 
                            'valueColOptions'=>['style'=>'width:30%'], 
                            'displayOnly'=>true
                        ],
                    ],
                ],
                [
                    'columns' => [
                        [
                            'attribute' => 'recomendado',
                            'format'=>'raw',
                            'value' => $model->recomendado ? '<span class="badge badge-success">Sí</span>' : '<span class="badge badge-danger">No</span>',
                            'valueColOptions'=>['style'=>'width:30%']
                        ],
                        [
                            'attribute' => 'inspiracion',
                            'format'=>'raw',
                            'value' => $model->inspiracion ? '<span class="badge badge-success">Sí</span>' : '<span class="badge badge-danger">No</span>',
                            'valueColOptions'=>['style'=>'width:30%']
                        ],
                    ],
                ],
                [
                    'columns' => [
                        [
                            'attribute' => 'vivo',
                            'format'=>'raw',
                            'value' => $model->vivo ? '<span class="badge badge-success">Sí</span>' : '<span class="badge badge-danger">No</span>',
                            'valueColOptions'=>['style'=>'width:30%']
                        ],
                        [
                            'attribute'=>'id_usuario', 
                            'label' => 'Creado por:',
                            'format'=>'raw', 
                            'value'=>$model->usuario->nombre,
                            'valueColOptions'=>['style'=>'width:30%'], 
                            'displayOnly'=>true
                        ],
                    ],
                ],
            
            [
                'group'=>true,
                'label'=>'Estadísticas',
                'rowOptions'=>['class'=>'btn-default']
            ],
                [
                    'columns' => [
                        [
                            'attribute'=>'fuerza', 
                            'label'=>'Fuerza',
                            'displayOnly'=>true,
                            'valueColOptions'=>['style'=>'width:30%']
                        ],
                        [
                            'attribute'=>'modfuerza', 
                            'label' => 'Modificador',
                            'format'=>'raw', 
                            'valueColOptions'=>['style'=>'width:30%'], 
                            'displayOnly'=>true
                        ],
                    ],
                ],
            [
                    'columns' => [
                        [
                            'attribute'=>'destreza', 
                            'label'=>'Destreza',
                            'displayOnly'=>true,
                            'valueColOptions'=>['style'=>'width:30%']
                        ],
                        [
                            'attribute'=>'moddestreza', 
                            'label' => 'Modificador',
                            'format'=>'raw', 
                            'valueColOptions'=>['style'=>'width:30%'], 
                            'displayOnly'=>true
                        ],
                    ],
                ],
            [
                    'columns' => [
                        [
                            'attribute'=>'constitucion', 
                            'label'=>'Constitución',
                            'displayOnly'=>true,
                            'valueColOptions'=>['style'=>'width:30%']
                        ],
                        [
                            'attribute'=>'modconstitucion', 
                            'label' => 'Modificador',
                            'format'=>'raw', 
                            'valueColOptions'=>['style'=>'width:30%'], 
                            'displayOnly'=>true
                        ],
                    ],
                ],
            [
                    'columns' => [
                        [
                            'attribute'=>'inteligencia', 
                            'displayOnly'=>true,
                            'valueColOptions'=>['style'=>'width:30%']
                        ],
                        [
                            'attribute'=>'modinteligencia', 
                            'label' => 'Modificador',
                            'format'=>'raw', 
                            'valueColOptions'=>['style'=>'width:30%'], 
                            'displayOnly'=>true
                        ],
                    ],
                ],
            [
                    'columns' => [
                        [
                            'attribute'=>'sabiduria', 
                            'displayOnly'=>true,
                            'valueColOptions'=>['style'=>'width:30%']
                        ],
                        [
                            'attribute'=>'modsabiduria', 
                            'label' => 'Modificador',
                            'format'=>'raw', 
                            'valueColOptions'=>['style'=>'width:30%'], 
                            'displayOnly'=>true
                        ],
                    ],
                ],
            [
                    'columns' => [
                        [
                            'attribute'=>'carisma', 
                            'displayOnly'=>true,
                            'valueColOptions'=>['style'=>'width:30%']
                        ],
                        [
                            'attribute'=>'modcarisma', 
                            'label' => 'Modificador',
                            'format'=>'raw', 
                            'valueColOptions'=>['style'=>'width:30%'], 
                            'displayOnly'=>true
                        ],
                    ],
                ],
            
            [
                'group'=>true,
                'label'=>'Biografía',
                'rowOptions'=>['class'=>'btn-default']
            ],
                    [
                        'columns' => [
                            [
                                'attribute'=>'edad',
                                'displayOnly'=>true,
                                'valueColOptions'=>['style'=>'width:30%']
                            ],
                            [
                                'attribute'=>'descripción', 
                                'format'=>'raw', 
                                'valueColOptions'=>['style'=>'width:30%'], 
                                'displayOnly'=>true
                            ],
                        ],
                    ],
                [
                        'columns' => [
                            [
                                'attribute'=>'imagen',
                                'value'=>$model->imagen,
                                'format' => ['image',['width'=>'200','height'=>'200']],
                            ],
                            [
                                    'attribute'=>'biografía',
                                    'displayOnly'=>true,
                                    'valueColOptions'=>['style'=>'width:30%']
                            ],
                        ],
                    ],
            
            [
                'group'=>true,
                'label'=>'Salvaciones',
                'rowOptions'=>['class'=>'btn-default']
            ],
            
                [
                     'columns' => [
                        [
                          'attribute'=>'salvfuerza',
                          'displayOnly'=>true,
                          'valueColOptions'=>['style'=>'width:30%']
                        ],
                        [
                          'attribute'=>'salvdestreza',
                          'displayOnly'=>true,
                          'valueColOptions'=>['style'=>'width:30%']
                        ],
                     ],
                ],
                [
                     'columns' => [
                        [
                          'attribute'=>'salvconstitucion',
                          'displayOnly'=>true,
                          'valueColOptions'=>['style'=>'width:30%']
                        ],
                        [
                          'attribute'=>'salvinteligencia',
                          'displayOnly'=>true,
                          'valueColOptions'=>['style'=>'width:30%']
                        ],
                     ],
                ],
                [
                     'columns' => [
                        [
                          'attribute'=>'salvsabiduria',
                          'displayOnly'=>true,
                          'valueColOptions'=>['style'=>'width:30%']
                        ],
                        [
                          'attribute'=>'salvcarisma',
                          'displayOnly'=>true,
                          'valueColOptions'=>['style'=>'width:30%']
                        ],
                     ],
                ],
            
            [
                'group'=>true,
                'label'=>'Habilidades',
                'rowOptions'=>['class'=>'btn-default']
            ],
                [
                    'columns' => [
                        [
                            'attribute'=>'acrobacias',
                            'displayOnly'=>true,
                            'valueColOptions'=>['style'=>'width:30%']
                        ],
                        [
                            'attribute'=>'atletismo',
                            'displayOnly'=>true,
                            'valueColOptions'=>['style'=>'width:30%']
                        ],
                    ],
                ],
                [
                    'columns' => [
                        [
                            'attribute'=>'conoarcano',
                            'displayOnly'=>true,
                            'valueColOptions'=>['style'=>'width:30%']
                        ],
                        [
                            'attribute'=>'engaño',
                            'displayOnly'=>true,
                            'valueColOptions'=>['style'=>'width:30%']
                        ],
                    ],
                ],
                [
                    'columns' => [
                        [
                            'attribute'=>'historia',
                            'displayOnly'=>true,
                            'valueColOptions'=>['style'=>'width:30%']
                        ],
                        [
                            'attribute'=>'interpretacion',
                            'displayOnly'=>true,
                            'valueColOptions'=>['style'=>'width:30%']
                        ],
                    ],
                ],
                [
                    'columns' => [
                        [
                            'attribute'=>'intimidacion',
                            'displayOnly'=>true,
                            'valueColOptions'=>['style'=>'width:30%']
                        ],
                        [
                            'attribute'=>'investigacion',
                            'displayOnly'=>true,
                            'valueColOptions'=>['style'=>'width:30%']
                        ],
                    ],
                ],
                [
                    'columns' => [
                        [
                            'attribute'=>'juego_de_manos',
                            'displayOnly'=>true,
                            'valueColOptions'=>['style'=>'width:30%']
                        ],
                        [
                            'attribute'=>'medicina',
                            'displayOnly'=>true,
                            'valueColOptions'=>['style'=>'width:30%']
                        ],
                    ],
                ],
                [
                    'columns' => [
                        [
                            'attribute'=>'naturaleza',
                            'displayOnly'=>true,
                            'valueColOptions'=>['style'=>'width:30%']
                        ],
                        [
                            'attribute'=>'percepcion',
                            'displayOnly'=>true,
                            'valueColOptions'=>['style'=>'width:30%']
                        ],
                    ],
                ],
                [
                    'columns' => [
                        [
                            'attribute'=>'perspicacia',
                            'displayOnly'=>true,
                            'valueColOptions'=>['style'=>'width:30%']
                        ],
                        [
                            'attribute'=>'persuasión',
                            'displayOnly'=>true,
                            'valueColOptions'=>['style'=>'width:30%']
                        ],
                    ],
                ],
                [
                    'columns' => [
                        [
                            'attribute'=>'religion',
                            'displayOnly'=>true,
                            'valueColOptions'=>['style'=>'width:30%']
                        ],
                        [
                            'attribute'=>'sigilo',
                            'displayOnly'=>true,
                            'valueColOptions'=>['style'=>'width:30%']
                        ],
                    ],
                ],
                [
                    'columns' => [
                        [
                            'attribute'=>'supervivencia',
                            'displayOnly'=>true,
                            'valueColOptions'=>['style'=>'width:30%']
                        ],
                        [
                            'attribute'=>'trato_animales',
                            'displayOnly'=>true,
                            'valueColOptions'=>['style'=>'width:30%']
                        ],
                    ],
                ],
            
            [
                'group'=>true,
                'label'=>'Información importante para el combate',
                'rowOptions'=>['class'=>'btn-default']
            ],
                [
                    'columns' => [
                        [
                            'attribute'=>'velocidad',
                            'displayOnly'=>true,
                            'valueColOptions'=>['style'=>'width:30%']
                        ],
                        [
                            'attribute'=>'iniciativa',
                            'displayOnly'=>true,
                            'valueColOptions'=>['style'=>'width:30%']
                        ],
                        
                    ],
                ],
                [
                    'columns' => [
                        [
                            'attribute'=>'vida',
                            'displayOnly'=>true,
                            'valueColOptions'=>['style'=>'width:30%']
                        ],
                        [
                            'attribute'=>'ca',
                            'displayOnly'=>true,
                            'valueColOptions'=>['style'=>'width:30%']
                        ],
                        
                    ],
                ],
                [
                    'columns' => [
                        [
                            'attribute'=>'id_armadura',
                            'label' => 'Armadura',
                            'value' => $model->armadura->nombre,
                            'displayOnly'=>true,
                            'valueColOptions'=>['style'=>'width:30%']
                        ],
                        [
                            'attribute'=>'id_armadura',
                            'label' => 'Ca de la armadura',
                            'value' => $model->armadura->ca,
                            'displayOnly'=>true,
                            'valueColOptions'=>['style'=>'width:30%']
                        ],
                        
                    ],
                ],
            
            
            
            [
                'group'=>true,
                'label'=>'Clase',
                'rowOptions'=>['class'=>'btn-default']
            ],
                
                    [
                        'attribute'=>'clases',
                        'value' => $model->nombreclase,
                        'displayOnly'=>true,
                        'valueColOptions'=>['style'=>'width:30%']
                    ],
            
            [
                'group'=>true,
                'label'=>'Armas y Objetos',
                'rowOptions'=>['class'=>'btn-default']
            ],
                
                    [
                        'attribute'=>'armas',
                        'value' => $model->nombrearmas,
                        'displayOnly'=>true,
                        'valueColOptions'=>['style'=>'width:30%']
                    ],
                    [
                        'attribute'=>'objetos',
                        'value' => $model->nombreobjetos,
                        'displayOnly'=>true,
                        'valueColOptions'=>['style'=>'width:30%']
                    ],                   
                    [
                        'attribute'=>'magias',
                        'value' => $model->nombremagia,
                        'displayOnly'=>true,
                        'valueColOptions'=>['style'=>'width:30%']
                    ],
                    [
                        'attribute'=>'dotes',
                        'value' => $model->nombredote,
                        'displayOnly'=>true,
                        'valueColOptions'=>['style'=>'width:30%']
                    ],
                    [
                        'attribute'=>'idiomaspersonajes',
                        'value' => $model->nombreidiomas,
                        'label' => 'Idiomas',
                        'displayOnly'=>true,
                        'valueColOptions'=>['style'=>'width:30%']
                    ],
                    
            [
                'group'=>true,
                'label'=>'Otra información',
                'rowOptions'=>['class'=>'btn-default']
            ],
            
                [
                    'columns' => [
                        [
                            'attribute'=>'salv_conjuto',
                            'label' => 'Salvación de conjuro',
                            'displayOnly'=>true,
                            'valueColOptions'=>['style'=>'width:30%']
                        ],
                        [
                            'attribute'=>'mod_ataque_conjuro',
                            'displayOnly'=>true,
                            'valueColOptions'=>['style'=>'width:30%']
                        ],
                        
                    ],
                ],
                [
                    'columns' => [
                        [
                            'attribute'=>'percepción_pasiva',
                            'displayOnly'=>true,
                            'valueColOptions'=>['style'=>'width:30%']
                        ],
                        [
                            'attribute'=>'notas',
                            'displayOnly'=>true,
                            'valueColOptions'=>['style'=>'width:30%']
                        ],
                    ],  
                ],
        ]
    ]);?>
    
</div>
