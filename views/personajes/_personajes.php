<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

?>

<div class="col-sm-6 col-md-3">
    <div class="thumbnail personajes">
        
            <?php if ($model->imagen): ?>
            <img src="<?=$model->imagen?>" alt="imagenes de los personajes" class='imagenespersonajesyusuarios'>
             <?php else: ?>
                <?= Html::img('@web/images/imagen_prueba.png', ['alt' => 'imagenes por defecto'], ['class' => 'imagenespersonajesyusuarios ']) ?>               
             <?php endif; ?>
            <h2><?= $model->nombre?></h2>
            <h3><?= $model->raza->nombre ?></h3>
            <h3><?= implode('</h3><h3>', ArrayHelper::getColumn($model->clases, 'nombre'))?></h3>
            <h3> Nivel total: <?= $model->nivel?></h3>
            <h2><?= $model->usuario->nombre ?></h2>
            <p><?= Html::a('Toda la información', ['personajes/view', 'id' => $model->id_personaje], ['class' => 'btn btn-default']) ?></p>
        
    </div>
</div>
