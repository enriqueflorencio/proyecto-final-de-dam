<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model app\models\Personajes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="personajes-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php $modelUsuario = ArrayHelper::map($modelUsuario, 'id_usuario', 'nombre'); ?>
    <?php $modelRaza = ArrayHelper::map($modelRaza, 'id_raza', 'nombre'); ?>
    <?php $modelArmadura = ArrayHelper::map($modelArmadura, 'id_armaduras', 'nombre'); ?>
    <?php $modelTrasfondo = ArrayHelper::map($modelTrasfondo, 'id_trasfondo', 'nombre'); ?>
    
    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'id_raza')->dropDownList($modelRaza,['prompt' => ''])->label('Razas') ?>

    <?= $form->field($model, 'id_armadura')->dropDownList($modelArmadura,['prompt' => ''])->label('Armaduras') ?>

    <?= $form->field($model, 'id_trasfondo')->dropDownList($modelTrasfondo,['prompt' => ''])->label('Trasfondo') ?>
    
    <?= $form->field($model, 'id_usuario')->dropDownList($modelUsuario,['prompt' => ''])->label('Usuarios') ?>

    <?= $form->field($model, 'imagen')->textInput()->label('Introduzca la ruta de la imagen') ?>

    <?= $form->field($model, 'biografia')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'edad')->textInput() ?>

    <?= $form->field($model, 'nivel')->textInput() ?>

    <?= $form->field($model, 'alineamineto')->dropDownList([ 'Legal bueno' => 'Legal bueno', 'Neutral bueno' => 'Neutral bueno', 'Caótico bueno' => 'Caótico bueno', 'Legal neutral' => 'Legal neutral', 'Neutral' => 'Neutral', 'Caótico neutral' => 'Caótico neutral', 'Legal malvado' => 'Legal malvado', 'Neutral malvado' => 'Neutral malvado', 'Caótico malvado' => 'Caótico malvado', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'fuerza')->textInput() ?>

    <?= $form->field($model, 'destreza')->textInput() ?>

    <?= $form->field($model, 'constitucion')->textInput() ?>

    <?= $form->field($model, 'inteligencia')->textInput() ?>

    <?= $form->field($model, 'sabiduria')->textInput() ?>

    <?= $form->field($model, 'carisma')->textInput() ?>

    <?= $form->field($model, 'vida')->textInput() ?>

    <?= $form->field($model, 'inspiracion')->checkbox() ?>

    <?= $form->field($model, 'escudo')->checkbox() ?> 
    

    <div class="form-group">
        <?= Html::submitButton('Siguiente', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

