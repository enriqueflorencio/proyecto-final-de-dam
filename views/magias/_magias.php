<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

?>

<div class="col-sm-4">
    <div class="thumbnail magias">
            <h5><?= implode(', ',ArrayHelper::getColumn($model->clases, 'nombre'))?></h5>
            <h3><u><?= $model->nombre?></u></h3>
            
            <h4><i> <?= $model->escuela ?>, <?= $model->nivel_conjuro ?></i></h3>
            <p> </p>
            <h4><b>Rango:</b> <?= $model->rango ?></h4>
            <h4><b>Componentes:</b> <?= implode(', ', ArrayHelper::getColumn($model->materialesmagias, 'material'))?></h4>
            <h4><b>Duración:</b> <?= $model->duración ?></h4>
            <?php if ($model->concentracion_ritual):?>
                <h4> <?= $model->concentracion_ritual ?> </h4>
            <?php endif; ?>
            
            <p class="invisible"> d</p>
            <h6 class="texto-magia"><?= $model->descripcion ?></h6>
        
    </div>
</div>

