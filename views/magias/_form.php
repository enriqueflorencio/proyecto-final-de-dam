<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Magias */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="magias-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'descripcion')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'nivel_conjuro')->dropDownList([ 'Truco' => 'Truco', 'Nivel 1' => 'Nivel 1', 'Nivel 2' => 'Nivel 2', 'Nivel 3' => 'Nivel 3', 'Nivel 4' => 'Nivel 4', 'Nivel 5' => 'Nivel 5', 'Nivel 6' => 'Nivel 6', 'Nivel 7' => 'Nivel 7', 'Nivel 8' => 'Nivel 8', 'Nivel 9' => 'Nivel 9', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'escuela')->dropDownList([ 'Evocación' => 'Evocación', 'Necromancia' => 'Necromancia', 'Adivinación' => 'Adivinación', 'Transmutación' => 'Transmutación', 'Encantamiento' => 'Encantamiento', 'Ilusión' => 'Ilusión', 'Conjuración' => 'Conjuración', 'Abjuración' => 'Abjuración', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'rango')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tiempo_casteo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'duración')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'concentracion_ritual')->dropDownList([ 'Concentración' => 'Concentración', 'Ritual' => 'Ritual', ], ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
