<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Magias */

$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Magias', 'url' => ['magias/listamagias']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="magias-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Modificar', ['update', 'id' => $model->id_magia], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id_magia], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estás seguro?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            
            'nombre',
            'descripcion:ntext',
            'nivel_conjuro',
            'escuela',
            'rango',
            'tiempo_casteo',
            'duración',
            'concentracion_ritual',
        ],
    ]) ?>

</div>
