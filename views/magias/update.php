<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Magias */

$this->title = 'Modificar Magia: ' . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Magias', 'url' => ['magias/listamagias']];
$this->params['breadcrumbs'][] = ['label' => $model->nombre, 'url' => ['view', 'id' => $model->id_magia]];
$this->params['breadcrumbs'][] = 'Modificar';
?>
<div class="magias-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
