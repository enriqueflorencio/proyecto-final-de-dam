<?php
    use yii\helpers\Html;
    use yii\widgets\ListView;
    use yii\grid\GridView;
    
    $this->title = 'Magias';
    $this->params['breadcrumbs'][] = $this->title;
?>
<?php if(Yii::$app->user->isGuest): ?>
<div class="view">
    
    <div class="well well-lg"><h2 style="text-align: center">
        <?= Html::img('@web/images/varita.png', ['class' => 'iconosheader'], ['alt' => 'espada']); ?> 
            Magias
        <?= Html::img('@web/images/varita.png', ['class' => 'iconosheader'], ['alt' => 'espada']); ?> 
        </h2>
    </div>
    
    <?= ListView::widget([
               'dataProvider' => $dataProvider,
               'itemView' => '_magias',
               'layout' => "\n{pager}\n{items}",
            ]);
     ?>       
</div>

<?php else: ?>

<div class="magias-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Añadir Magia/Hechizo/Conjuro', ['create'], ['class' => 'btn btn-default']) ?>
        <?= Html::a('Materiales de las magias', ['materialesmagias/index'], ['class' => 'btn btn-info']) ?>
        <?= Html::a('Magias que puede utilizar una clase', ['magiasclase/index'], ['class' => 'btn btn-info']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
         
            ['class' => 'yii\grid\ActionColumn'],
            'nombre',
            'descripcion:ntext',
            'nivel_conjuro',
            'escuela',
            'rango',
            'tiempo_casteo',
            'duración',
            'concentracion_ritual',

        ],
    ]); ?>


</div>

<?php endif; ?>

