<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Magias */

$this->title = 'Añadir Magias';
$this->params['breadcrumbs'][] = ['label' => 'Magias', 'url' => ['magias/listamagias']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="magias-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
