<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Tienenrazas */

$this->title = 'Modificar rasgo de una raza: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Razas', 'url' => ['razas/listarazas']];
$this->params['breadcrumbs'][] = ['label' => 'Rasgos que tienen las razas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Modificación';
?>
<div class="tienenrazas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelRaza' => $modelRaza,
        'modelHabilidadesraza' => $modelHabilidadesraza,
    ]) ?>

</div>
