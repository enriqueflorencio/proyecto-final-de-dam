<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Tienenrazas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tienenrazas-form">

    <?php $form = ActiveForm::begin(); ?>
    <?php $modelRaza = ArrayHelper::map($modelRaza, 'id_raza', 'nombre'); ?>
    <?php $modelHabilidadesraza = ArrayHelper::map($modelHabilidadesraza, 'id_habilidadesraza', 'nombre'); ?>
    <?= $form->field($model, 'id_raza')->dropDownList($modelRaza, ['prompt' => ''])->label("Raza") ?> 

    <?= $form->field($model, 'id_habilidadesraza')->dropDownList($modelHabilidadesraza, ['prompt' => ''])->label("Habilidades de Raza") ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
