<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Rasgos que tienen las razas';
$this->params['breadcrumbs'][] = ['label' => 'Razas', 'url' => ['razas/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tienenrazas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Establecer rasgo a una raza', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Ver rasgos', ['habilidadesraza/index'], ['class' => 'btn btn-info']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            
            ['class' => 'yii\grid\ActionColumn'],
            [
                'attribute' => 'id_raza',
                'label' => 'Raza',
                'value' => 'clase.nombre'
            ],
            [
                'attribute' => 'id_habilidadesraza',
                'label' => 'Habilidades',
                'value' => 'habilidadesraza.nombre'
            ],

        ],
    ]); ?>


</div>
