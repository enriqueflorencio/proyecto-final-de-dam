<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Tienenrazas */

$this->title = 'Establecer un rasgo a una raza';
$this->params['breadcrumbs'][] = ['label' => 'Razas', 'url' => ['razas/listarazas']];
$this->params['breadcrumbs'][] = ['label' => 'Rasgos que tienen las razas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tienenrazas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelRaza' => $modelRaza,
        'modelHabilidadesraza' => $modelHabilidadesraza,
    ]) ?>

</div>
