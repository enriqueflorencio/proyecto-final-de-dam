<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Personajesesion */

$this->title = 'Añadir un personaje a una sesión';
$this->params['breadcrumbs'][] = ['label' => 'Sesiones', 'url' => ['sesiones/listasesiones']];
$this->params['breadcrumbs'][] = ['label' => 'Personaje que están en Sesiones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="personajesesion-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelPersonaje' => $modelPersonaje,
        'id_sesion' => $id_sesion,
    ]) ?>

</div>
