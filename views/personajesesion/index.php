<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Personaje que están en Sesiones';
$this->params['breadcrumbs'][] = ['label' => 'Sesiones', 'url' => ['sesiones/listasesiones']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="personajesesion-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Añadir un personaje a una sesion', ['create'], ['class' => 'btn btn-default']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
     
            ['class' => 'yii\grid\ActionColumn'],
            'id_sesion',
            [
                'attribute' => 'id_personaje',
                'label' => 'Personaje',
                'value' => 'personaje.nombre',
            ],
            

            
        ],
    ]); ?>


</div>
