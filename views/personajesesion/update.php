<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Personajesesion */

$this->title = 'Modificar un personaje a una sesión: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Sesiones', 'url' => ['sesiones/listasesiones']];
$this->params['breadcrumbs'][] = ['label' => 'Personajes en una sesion', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Modicación';
?>
<div class="personajesesion-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelPersonaje' => $modelPersonaje,
    ]) ?>

</div>
