<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Personajesesion */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Sesiones', 'url' => ['sesiones/listasesiones']];
$this->params['breadcrumbs'][] = ['label' => 'Personajes en una sesión', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="personajesesion-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Modificar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'id_personaje',
                'label' => 'Personaje',
                'value' => $model->personaje->nombre,
            ],
            'id_sesion',
        ],
    ]) ?>

</div>
