<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Tienenclase */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Clases', 'url' => ['clases/listaclases']];
$this->params['breadcrumbs'][] = ['label' => 'Habilidades propias de las clases', 'url' => ['habilidadesclase/index']];
$this->params['breadcrumbs'][] = ['label' => 'Nivel de desbloqueo dependiendo de la clase ', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="tienenclase-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Modificar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estás seguro?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'id_clase',
                'label' => 'Clase',
                'value' => $model->clase->nombre,
            ],
            [
                'attribute' => 'id_habilidadesclase',
                'label' => 'Habilidad',
                'value' => $model->habilidadesclase->nombre,
            ],
            'nivel_desbloque',
        ],
    ]) ?>

</div>
