<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Tienenclase */

$this->title = 'Establecer nivel necesario para desbloquear una habilidad de una clase';
$this->params['breadcrumbs'][] = ['label' => 'Clases', 'url' => ['clases/listaclases']];
$this->params['breadcrumbs'][] = ['label' => 'Habilidades propias de las clases', 'url' => ['habilidadesclase/index']];
$this->params['breadcrumbs'][] = ['label' => 'Nivel de desbloqueo dependiendo de la clase', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tienenclase-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelClase' => $modelClase,
        'modelHabilidadesclases' => $modelHabilidadesclases,
    ]) ?>

</div>
