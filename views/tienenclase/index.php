<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Nivel de desbloqueo de una habilidad dependiendo de la clase';
$this->params['breadcrumbs'][] = ['label' => 'Clases', 'url' => ['clases/listaclases']];
$this->params['breadcrumbs'][] = ['label' => 'Habilidades propias de las clases', 'url' => ['habilidadesclase/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tienenclase-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Establecer nivel necesario para desbloquear una habilidad de una clase', ['create'], ['class' => 'btn btn-default']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            
            
            ['class' => 'yii\grid\ActionColumn'],
            [
                'attribute' => 'id_clase',
                'label' => 'Clase',
                'value' => 'clase.nombre'
            ],
            [
                'attribute' => 'id_habilidadesclase',
                'label' => 'Habilidades',
                'value' => 'habilidadesclase.nombre'
            ],
            'nivel_desbloque',
        ],
    ]); ?>


</div>
