<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Tienenclase */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tienenclase-form">

    <?php $form = ActiveForm::begin(); ?>
    <?php $modelClase = ArrayHelper::map($modelClase, 'id_clase', 'nombre'); ?>
    <?php $modelHabilidadesclases = ArrayHelper::map($modelHabilidadesclases, 'id_habilidadesclase', 'nombre'); ?>
    <?= $form->field($model, 'id_clase')->dropDownList($modelClase, ['prompt' => ''])->label("Clase") ?> 

    <?= $form->field($model, 'id_habilidadesclase')->dropDownList($modelHabilidadesclases, ['prompt' => ''])->label("Habilidades de clase") ?> 

    <?= $form->field($model, 'nivel_desbloque')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
