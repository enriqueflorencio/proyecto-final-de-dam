<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Materialesmagias */

$this->title = 'Modificar material de una magia: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Magias', 'url' => ['magias/listamagias']];
$this->params['breadcrumbs'][] = ['label' => 'Materiales magias', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Modificar';
?>
<div class="materialesmagias-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelMagias' => $modelMagias,
    ]) ?>

</div>
