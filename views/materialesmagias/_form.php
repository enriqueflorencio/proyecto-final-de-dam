<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Materialesmagias */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="materialesmagias-form">

    <?php $form = ActiveForm::begin(); ?>
    <?php $modelMagias = ArrayHelper::map($modelMagias, 'id_magia', 'nombre'); ?>

    <?= $form->field($model, 'id_magia')->dropDownList($modelMagias, ['prompt' => ''])->label("Magias") ?>

    <?= $form->field($model, 'material')->dropDownList([ 'Verbal' => 'Verbal', 'Somático' => 'Somático', 'Material' => 'Material', ], ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
