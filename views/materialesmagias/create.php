<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Materialesmagias */

$this->title = 'Añadir material a una magia';
$this->params['breadcrumbs'][] = ['label' => 'Magias', 'url' => ['magias/listamagias']];
$this->params['breadcrumbs'][] = ['label' => 'Materiales magias', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="materialesmagias-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelMagias' => $modelMagias,

    ]) ?>

</div>
