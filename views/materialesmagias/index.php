<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Materiales magias';
$this->params['breadcrumbs'][] = ['label' => 'Magias', 'url' => ['magias/listamagias']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="materialesmagias-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Añadir material a una magia', ['create'], ['class' => 'btn btn-default']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\ActionColumn'],
            
            [
                'attribute' => 'id_magia',
                'label' => 'Magia',
                'value' => 'magia.nombre'
            ],
            'material',

            
        ],
    ]); ?>


</div>
