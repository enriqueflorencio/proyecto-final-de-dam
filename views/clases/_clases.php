<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

?>

<div class="col-sm-6">
    <div class="thumbnail clases">
            <p> </p>
            <img class="imagencirculo" src=<?= $model->imagen?>> 
            <h3><u><?= $model->nombre ?></u></h3>
            
            <h5 class="descripcion"><?= $model->descripcion ?></h5>
            <p class="invisible"> Linea invisible </p>
            <h5><b>Característica principal:</b> <?= $model->carateristica_principal ?></h5>
            <h5><b>Competencias:</b> <?= $model->competencias ?> </h5>
            <h5><b>Dado de golpe:</b> <?= $model->dado_golpe ?> </h5>
            <h5><b>Tipo:</b> <?= $model->tipo ?></h5>
            <h5><b>Nivel necesario:</b> <?= $model->nivel_minimo ?></h5>
            <h5><b>Estadistica mínima:</b>
                <?php if($model->fuerza != 0): ?>
                    <i>Fuerza: <?= $model->fuerza ?>,</i>
                <?php endif; ?>
                <?php if($model->destreza != 0): ?>
                    <i>Destreza: <?= $model->destreza ?>,</i>
                <?php endif; ?>
                <?php if($model->constitucion != 0): ?>
                    <i>Constitución: <?= $model->constitucion ?>,</i>
                <?php endif; ?>
                <?php if($model->inteligencia != 0): ?>
                    <i>Inteligencia: <?= $model->inteligencia ?>,</i>
                <?php endif; ?>
                <?php if($model->sabiduria != 0): ?>
                    <i>Sabiduría: <?= $model->sabiduria ?>,</i>
                <?php endif; ?>
                <?php if($model->carisma != 0): ?>
                    <i>Carisma: <?= $model->carisma ?>,</i>
                <?php endif; ?>
            </h5>
            <p class="botonclases">
            <?= Html::a('Habilidades desbloqueables', ['habilidadesclase/listarasgossegunclase', 'id' => $model->id_clase], ['class' => 'btn btn-default']) ?>
            </p>
            
        
    </div>
</div>

