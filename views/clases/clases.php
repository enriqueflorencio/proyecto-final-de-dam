<?php
    use yii\helpers\Html;
    use yii\widgets\ListView;
    use yii\grid\GridView;
    
    $this->title = 'Clases';
    $this->params['breadcrumbs'][] = $this->title;
?>
<?php if(Yii::$app->user->isGuest): ?>

<div class="view">
    
    <div class="well well-lg"><h2 style="text-align: center">
        <?= Html::img('@web/images/espada_clase.png', ['class' => 'iconosheader'], ['alt' => 'espada']); ?> 
            Clases
        <?= Html::img('@web/images/magia.png', ['class' => 'iconosheader'], ['alt' => 'espada']); ?> 
        </h2>
    </div>
    
    <?= ListView::widget([
               'dataProvider' => $dataProvider,
               'itemView' => '_clases',
               'layout' => "\n{pager}\n{items}",
            ]);
     ?>       
</div>

<?php else: ?>

<div class="clases-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Añadir clase', ['create'], ['class' => 'btn btn-default']) ?>
        <?= Html::a('Ver habilidad con las que son competentes las clases', ['clasecomphabilidades/index'], ['class' => 'btn btn-info']) ?>
        <?= Html::a('Ver habilidades clases', ['habilidadesclase/index'], ['class' => 'btn btn-info']) ?>
        <?= Html::a('Ver salvaciones', ['salvaciones/index'], ['class' => 'btn btn-info']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\ActionColumn'],
            'nombre',
            'descripcion:ntext',
            [

                'attribute' => 'imagen',

                'format' => 'html',

                'label' => 'Imagen',

                'value' => function ($data) {

                    return Html::img($data['imagen'],

                        ['width' => '100px']);

                },

            ],

            'carateristica_principal',
            'competencias:ntext',
            'dado_golpe',
            'tipo',
            'aptitud_mágica',
            'nivel_minimo',
            'fuerza',
            'destreza',
            'constitucion',
            'inteligencia',
            'sabiduria',
            'carisma',

            
        ],
    ]); ?>


</div>

<?php endif; ?>

