<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Idiomaspersonaje */

$this->title = 'Modificar idioma de un personaje: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Idiomas de los personajes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Modificación';
?>
<div class="idiomaspersonaje-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelPersonaje' => $modelPersonaje,
    ]) ?>

</div>
