<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Idiomaspersonaje */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="idiomaspersonaje-form">

    <?php $form = ActiveForm::begin(); ?>
    <?php $modelPersonaje = ArrayHelper::map($modelPersonaje, 'id_personaje', 'nombre'); ?>
    
    <?= $form->field($model, 'id_personaje')->dropDownList($modelPersonaje, ['prompt' => ''])->label("Personaje") ?>

    <?= $form->field($model, 'idioma')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
