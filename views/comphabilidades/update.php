<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Comphabilidades */

$this->title = 'Modificar habilidades que te da el trasfondo: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Trasfondos', 'url' => ['trasfondos/listatrasfondos']];
$this->params['breadcrumbs'][] = ['label' => 'Competencia con habilidades que te da el trasfondo', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Modificar';
?>
<div class="comphabilidades-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelTrasfondo' => $modelTrasfondo,
    ]) ?>

</div>
