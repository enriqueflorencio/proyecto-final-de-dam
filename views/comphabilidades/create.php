<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Comphabilidades */

$this->title = 'Establecer habilidad a un trasfondo';
$this->params['breadcrumbs'][] = ['label' => 'Trasfondos', 'url' => ['trasfondos/listatrasfondos']];
$this->params['breadcrumbs'][] = ['label' => 'Competencia con habilidades que te da el trasfondo', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comphabilidades-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelTrasfondo' => $modelTrasfondo, 
    ]) ?>

</div>
