<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Comphabilidades */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="comphabilidades-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?php $modelTrasfondo = ArrayHelper::map($modelTrasfondo, 'id_trasfondo', 'nombre'); ?>
    
    <?= $form->field($model, 'id_trasfondo')->dropDownList($modelTrasfondo, ['prompt'=> ''])->label('Trasfondo') ?>

    <?= $form->field($model, 'habilidad')->dropDownList(['Atletismo' => 'Atletismo', 'Conociemiento Arcano' => 'Conocimiento Arcano', 
        'Engaño' => 'Engaño', 'Historia' => 'Historia', 'Interpretación' => 'Interpretación', 'Intimidacion' => 'Intimidación', 
        'Investigación' => 'Investigación', 'Juego de manos' => 'Juego de manos', 'Medicina' => 'Medicina', 'Naturaleza' => 'Naturaleza', 
        'Percepción' => 'Percepción', 'Perspicacia' => 'Perspicacia', 'Persuasión' => 'Persuasión', 'Religión' => 'Religión',
        'Sigilo' => 'Sigilo', 'Supervivencia' => 'Supervivencia', 'Trato con animales' => 'Trato con animales'],['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
