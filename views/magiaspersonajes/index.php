<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Lista de hechizos de los personajes';
$this->params['breadcrumbs'][] = ['label' => 'Personajes', 'url' => ['personajes/listapersonajes']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="magiaspersonajes-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Añadir un hechizo a la lista de conjuros de un personaje', ['create'], ['class' => 'btn btn-default']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\ActionColumn'],
            [
                'attribute' => 'id_personaje',
                'label' => 'Personaje',
                'value' => 'personaje.nombre',
            ],
            [
                'attribute' => 'id_magia',
                'label' => 'Magia',
                'value' => 'magia.nombre',
            ],
        ],
    ]); ?>


</div>
