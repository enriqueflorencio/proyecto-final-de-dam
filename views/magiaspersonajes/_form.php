<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Magiaspersonajes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="magiaspersonajes-form">

    <?php $form = ActiveForm::begin(); ?>
    <?php $modelPersonaje = ArrayHelper::map($modelPersonaje, 'id_personaje', 'nombre'); ?>
    <?php $modelMagias = ArrayHelper::map($modelMagias, 'id_magia', 'nombre'); ?>
    
    <?= $form->field($model, 'id_magia')->dropDownList($modelMagias, ['prompt' => ''])->label("Magia") ?>

    <?= $form->field($model, 'id_personaje')->dropDownList($modelPersonaje, ['prompt' => ''])->label("Personaje") ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
