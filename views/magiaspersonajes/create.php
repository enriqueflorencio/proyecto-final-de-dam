<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Magiaspersonajes */

$this->title = 'Añadir hechizo al libro de hechizos de un personaje';
$this->params['breadcrumbs'][] = ['label' => 'Personajes', 'url' => ['personajes/listapersonajes']];
$this->params['breadcrumbs'][] = ['label' => 'Libro de hechizos de los personajes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="magiaspersonajes-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelMagias' => $modelMagias,
        'modelPersonaje' => $modelPersonaje,
    ]) ?>

</div>
