<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Magiaspersonajes */

$this->title = 'Modificar magias que tiene un personajes: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Personajes', 'url' => ['personajes/listapersonajes']];
$this->params['breadcrumbs'][] = ['label' => 'Lista de hechizos de los personajes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Modificación';
?>
<div class="magiaspersonajes-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelMagias' => $modelMagias,
        'modelPersonaje' => $modelPersonaje,
    ]) ?>

</div>
