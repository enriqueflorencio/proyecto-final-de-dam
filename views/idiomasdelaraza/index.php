<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Idiomas de la razas';
$this->params['breadcrumbs'][] = ['label' => 'Razas', 'url' => ['razas/listarazas']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="idiomasdelaraza-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Establecer que idioma hablan las razas', ['create'], ['class' => 'btn btn-default']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            
            ['class' => 'yii\grid\ActionColumn'],
            [
                'attribute' => 'id_raza',
                'label' => 'Raza',
                'value' => 'raza.nombre',
            ],
            'idioma',

            
        ],
    ]); ?>


</div>
