<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Idiomasdelaraza */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="idiomasdelaraza-form">

    <?php $form = ActiveForm::begin(); ?>
    <?php $modelRaza = ArrayHelper::map($modelRaza, 'id_raza', 'nombre'); ?>
    
    <?= $form->field($model, 'id_raza')->dropDownList($modelRaza, ['prompt' => ''])->label('Raza') ?>

    <?= $form->field($model, 'idioma')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
