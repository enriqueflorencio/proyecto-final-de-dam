<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Idiomasdelaraza */

$this->title = 'Establecer que idioma hablan las razas';
$this->params['breadcrumbs'][] = ['label' => 'Razas', 'url' => ['razas/listarazas']];
$this->params['breadcrumbs'][] = ['label' => 'Idiomas de la razas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="idiomasdelaraza-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelRaza' => $modelRaza,
    ]) ?>

</div>
