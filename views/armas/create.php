<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Armas */

$this->title = 'Añadir Arma';
$this->params['breadcrumbs'][] = ['label' => 'Armas', 'url' => ['armaduras/listaarmaduras']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="armas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
