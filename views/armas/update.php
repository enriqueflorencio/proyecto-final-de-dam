<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Armas */

$this->title = 'Modificar Arma: ' . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Armas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nombre, 'url' => ['view', 'id' => $model->id_arma]];
$this->params['breadcrumbs'][] = 'Modificar';
?>
<div class="armas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
