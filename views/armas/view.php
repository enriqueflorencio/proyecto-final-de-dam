<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Armas */

$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Armas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="armas-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Modificar', ['update', 'id' => $model->id_arma], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id_arma], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estás seguro?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'nombre',
            'imagen',
            'descripcion:ntext',
            'tipo',   
            'daño',
            [
                    'attribute' => 'magico',
                    'label'=>'¿Arma Mágica?',
                    'format'=>'raw',
                    'value' => function($model) { return $model->magico == 0 ? 'No' : 'Sí';},
            ],
            'oro',
            'plata',
            'cobre',
        ],
    ]) ?>

</div>
