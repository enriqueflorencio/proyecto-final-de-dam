<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Recomclases */

$this->title = 'Modificar reseña de una clase: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Recomendaciones', 'url' => ['site/recomendaciones']];
$this->params['breadcrumbs'][] = ['label' => 'Reseña Clases', 'url' => ['todaspuntuacionesclases']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Modificar';
?>
<div class="recomclases-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelUsuario' => $modelUsuario,
        'modelClase' => $modelClase,
    ]) ?>

</div>
