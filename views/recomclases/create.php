<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Recomclases */

$this->title = 'Añadir reseña a una clase';
$this->params['breadcrumbs'][] = ['label' => 'Recomendaciones', 'url' => ['site/recomendaciones']];
$this->params['breadcrumbs'][] = ['label' => 'Reseñas Clases', 'url' => ['todaspuntuacionesclases']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="recomclases-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelUsuario' => $modelUsuario,
        'modelClase' => $modelClase,
    ]) ?>

</div>
