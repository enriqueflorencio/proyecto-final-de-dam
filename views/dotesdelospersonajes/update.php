<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Dotesdelospersonajes */

$this->title = 'Modificar dote de un personaje: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Personajes', 'url' => ['personajes/listapersonajes']];
$this->params['breadcrumbs'][] = ['label' => 'Dotes de los personajes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Modificación';
?>
<div class="dotesdelospersonajes-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelPersonaje' => $modelPersonaje,
        'modelDote' => $modelDote, 
    ]) ?>

</div>
