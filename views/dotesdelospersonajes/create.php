<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Dotesdelospersonajes */

$this->title = 'Añadir una dote a un personaje';
$this->params['breadcrumbs'][] = ['label' => 'Personajes', 'url' => ['personajes/listapersonajes']];
$this->params['breadcrumbs'][] = ['label' => 'Dotes de los personajes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dotesdelospersonajes-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelPersonaje' => $modelPersonaje,
        'modelDote' => $modelDote, 
    ]) ?>

</div>
