<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Dotesdelospersonajes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dotesdelospersonajes-form">

    <?php $form = ActiveForm::begin(); ?>
    <?php $modelDote = ArrayHelper::map($modelDote, 'id_dote', 'nombre'); ?>
    <?php $modelPersonaje = ArrayHelper::map($modelPersonaje, 'id_personaje', 'nombre'); ?>
    <?= $form->field($model, 'id_dote')->dropDownList($modelDote, ['prompt' => ''])->label("Dote") ?>

    <?= $form->field($model, 'id_personaje')->dropDownList($modelPersonaje, ['prompt' => ''])->label("Personaje") ?> 

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
