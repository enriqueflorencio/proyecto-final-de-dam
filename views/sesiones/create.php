<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Sesiones */

$this->title = 'Establecer Sesión';
$this->params['breadcrumbs'][] = ['label' => 'Sesiones', 'url' => ['listasesiones']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sesiones-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        
    ]) ?>

</div>
