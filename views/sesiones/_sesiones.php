<?php
    use yii\helpers\ArrayHelper;
?>

<div class="col-sm-6">
    <div class="thumbnail sesiones">
            <h4 style="text-align: center;"><u>Sesión nº <?= $model->id_sesion ?></u></h4>
            <h5><b>Fecha</b> <?= $model->fecha ?></h5>
            <p><b>Duración:</b> <?= $model->duracion ?> minutos</p>

            <p><b>Personajes participante:</b></p>
            <?php if($model->personajes): ?>
                <ul>
                    <li>
                    <?= implode('</li><li>', ArrayHelper::getColumn($model->personajes, 'nombre'))?>
                    </li>
                </ul>
            <?php else: ?>
                <ul>
                    <li> No participa ningún personaje </li>
                </ul>
            <?php endif; ?>
    </div>
</div>

