<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use kartik\datetime\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Sesiones */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sesiones-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'duracion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fecha')->widget(DateTimePicker::classname(), [
	'options' => ['placeholder' => 'Establece una fecha'],
	'pluginOptions' => [
		'autoclose' => true
	]
]); ?>

    <div class="form-group">
        <?= Html::submitButton('Siguiente', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
