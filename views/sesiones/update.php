<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Sesiones */

$this->title = 'Modificar Sesión: ' . $model->id_sesion;
$this->params['breadcrumbs'][] = ['label' => 'Sesiones', 'url' => ['listasesiones']];
$this->params['breadcrumbs'][] = ['label' => $model->id_sesion, 'url' => ['view', 'id' => $model->id_sesion]];
$this->params['breadcrumbs'][] = 'Modificar';
?>
<div class="sesiones-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
