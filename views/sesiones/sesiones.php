<?php
    use yii\helpers\Html;
    use yii\widgets\ListView;
    use yii\grid\GridView;
    
    $this->title = 'Sesiones';
    $this->params['breadcrumbs'][] = $this->title;
?>
<?php if(Yii::$app->user->isGuest): ?>

<div class="view">
    
     <div class="well well-lg"><h2 style="text-align: center">
        <?= Html::img('@web/images/calendario.png', ['class' => 'iconosheader'], ['alt' => 'sesiones']); ?> 
            Sesiones
        <?= Html::img('@web/images/calendario.png', ['class' => 'iconosheader'], ['alt' => 'sesiones']); ?> 
        </h2>
    </div>
    
    <?= ListView::widget([
               'dataProvider' => $dataProvider,
               'itemView' => '_sesiones',
               'layout' => "\n{pager}\n{items}",
            ]);
     ?>       
</div>

<?php else: ?>

<div class="sesiones-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Establecer Sesión', ['create'], ['class' => 'btn btn-default']) ?>
        <?= Html::a('Ver Personajes sesion', ['/personajesesion/index'], ['class' => 'btn btn-info']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            
            ['class' => 'yii\grid\ActionColumn'],
            'id_sesion',
            'duracion',
            'num_miembros',
            'nivel_medio',
            'fecha',

        ],
    ]); ?>


</div>

<?php endif; ?>

