<?php
    
    use yii\widgets\ListView;
    use yii\helpers\Html;
    
    $this->title = 'Reseña Objetos';
    $this->params['breadcrumbs'][] = ['label' => 'Recomendaciones', 'url' => ['site/recomendaciones']];
    $this->params['breadcrumbs'][] = $this->title;
?>

<div class="view">
    <p>
        <?= Html::a('Todas las reseñas', ['todaspuntuacionesobjetos'], ['class' => 'btn btn-default']) ?>
    </p>
    <?= ListView::widget([
               'dataProvider' => $dataProvider,
               'itemView' => '_mediaobjetos',
               'layout' => "\n{pager}\n{items}",
            ]);
     ?>       
</div>

