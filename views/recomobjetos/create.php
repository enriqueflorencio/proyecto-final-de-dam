<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Recomobjetos */

    $this->title = 'Añadir reseña a un objeto';
    $this->params['breadcrumbs'][] = ['label' => 'Recomendaciones', 'url' => ['site/recomendaciones']];
    $this->params['breadcrumbs'][] = ['label' => 'Media Objetos', 'url' => ['mediaobjetos']];
    $this->params['breadcrumbs'][] = ['label' => 'Reseña Objetos', 'url' => ['todaspuntuacionesobjetos']];
    $this->params['breadcrumbs'][] = $this->title;
?>
<div class="recomobjetos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelUsuario' => $modelUsuario,
        'modelObjeto' => $modelObjeto,
    ]) ?>

</div>
