<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Recomobjetos */

$this->title = 'Modificar reseña de un objeto: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Recomendaciones', 'url' => ['site/recomendaciones']];
$this->params['breadcrumbs'][] = ['label' => 'Media Objetos', 'url' => ['mediaobjetos']];
$this->params['breadcrumbs'][] = ['label' => 'Reseña Objetos', 'url' => ['todaspuntuacionesobjetos']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Modificar';
?>
<div class="recomobjetos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelUsuario' => $modelUsuario,
        'modelObjeto' => $modelObjeto,
    ]) ?>

</div>
