<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Objetos */

$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Objetos', 'url' => ['objetos/listaobjetos']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="objetos-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Modificar', ['update', 'id' => $model->id_objeto], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id_objeto], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estás seguro?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'nombre',
            'descripcion:ntext',
            'tipo',
            [
                    'attribute' => 'mágico',
                    'label'=>'¿Arma Mágica?',
                    'format'=>'raw',
                    'value' => function($model) { return $model->mágico == 0 ? 'No' : 'Sí';},
            ],
            'oro',
            'plata',
            'cobre',
        ],
    ]) ?>

</div>
