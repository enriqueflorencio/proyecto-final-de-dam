<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Objetos */

$this->title = 'Modificar objeto: ' . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Objetos', 'url' => ['objetos/listaobjetos']];
$this->params['breadcrumbs'][] = ['label' => $model->nombre, 'url' => ['view', 'id' => $model->id_objeto]];
$this->params['breadcrumbs'][] = 'Modificar';
?>
<div class="objetos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
