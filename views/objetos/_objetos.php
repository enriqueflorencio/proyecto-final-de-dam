<?php
use yii\helpers\Html;

?>

<div class="col-sm-4">
    <div class="thumbnail objetos">
        
            <h3><u><?= $model->nombre ?></u></h3>
            <p> </p>
            <h5>
                <?= $model->descripcion ?>
            
            </h5>
            <p> </p>
            <h5><b>Tipo de objeto:</b> <?= $model->tipo ?></h5>
            <?php if ($model->mágico): ?>
                <h5><b>Mágico:</b>
                    <font color="green">Si</font>
                </h5>
            <?php else: ?>
                <h5><b>Mágico:</b>
                    <font color="red">No</font>
                </h5>
            <?php endif; ?>
            <p><b>Valor:</b> 
                <?= $model->oro ?> <?= Html::img('@web/images/moneda-oro.png', ['class' => 'iconos'], ['alt' => 'moneda de oro']); ?>
                <?= $model->plata ?> <?= Html::img('@web/images/moneda-plata.png', ['class' => 'iconos'], ['alt' => 'moneda de plata']); ?>
                <?= $model->cobre ?> <?= Html::img('@web/images/moneda-cobre.png', ['class' => 'iconos'], ['alt' => 'moneda de cobre']); ?>
            </p>

    </div>
</div>

