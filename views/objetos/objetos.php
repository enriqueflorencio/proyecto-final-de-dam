<?php
    use yii\helpers\Html;
    use yii\widgets\ListView;
    use yii\grid\GridView;
    
    $this->title = 'Objetos';
    $this->params['breadcrumbs'][] = $this->title;
?>
<?php if(Yii::$app->user->isGuest): ?>

<div class="view">
    
    <div class="well well-lg"><h2 style="text-align: center">
        <?= Html::img('@web/images/poción.png', ['class' => 'iconosheader'], ['alt' => 'espada']); ?> 
            Objetos
        <?= Html::img('@web/images/poción.png', ['class' => 'iconosheader'], ['alt' => 'espada']); ?> 
        </h2>
    </div>
    
    <div class="recordatorio">
        <div class="recordatorio-texto">
        <h4>Recordatorio:</h4>
        <h5>1 <?= Html::img('@web/images/moneda-oro.png', ['class' => 'iconos'], ['alt' => 'moneda de oro']); ?> 
            = 10 <?= Html::img('@web/images/moneda-plata.png', ['class' => 'iconos'], ['alt' => 'moneda de plata']); ?></h5>
        <h5>1 <?= Html::img('@web/images/moneda-plata.png', ['class' => 'iconos'], ['alt' => 'moneda de plata']); ?> 
            = 10 <?= Html::img('@web/images/moneda-cobre.png', ['class' => 'iconos'], ['alt' => 'moneda de cobre']); ?></h5>
        </div>
    </div>
    
    <?= ListView::widget([
               'dataProvider' => $dataProvider,
               'itemView' => '_objetos',
               'layout' => "\n{pager}\n{items}",
            ]);
     ?>       
</div>

<?php else: ?>

<div class="objetos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Añadir Objetos', ['create'], ['class' => 'btn btn-default']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            
            ['class' => 'yii\grid\ActionColumn'],
            
            'nombre',
            'descripcion:ntext',
            'tipo',
            [
                    'attribute' => 'mágico',
                    'label'=>'¿Arma Mágica?',
                    'format'=>'raw',
                    'value' => function($model) { return $model->mágico == 0 ? 'No' : 'Sí';},
            ],
            'oro',
            'plata',
            'cobre',

            
        ],
    ]); ?>


</div>


<?php endif; ?>
