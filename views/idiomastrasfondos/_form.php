<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Idiomastrasfondos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="idiomastrasfondos-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?php $modelTrasfondo = ArrayHelper::map($modelTrasfondo, 'id_trasfondo', 'nombre'); ?>

    <?= $form->field($model, 'id_trasfondo')->dropDownList($modelTrasfondo, ['prompt' => ''])->label("Trasfondo") ?>

    <?= $form->field($model, 'idioma')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
