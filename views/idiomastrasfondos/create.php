<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Idiomastrasfondos */

$this->title = 'Añadir idioma al trasfondo';
$this->params['breadcrumbs'][] = ['label' => 'Trasfondos', 'url' => ['trasfondos/listatrasfondos']];
$this->params['breadcrumbs'][] = ['label' => 'Idiomas de los trasfondos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="idiomastrasfondos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelTrasfondo' => $modelTrasfondo,
    ]) ?>

</div>
