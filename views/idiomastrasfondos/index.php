<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Idiomas de los trasfondos';
$this->params['breadcrumbs'][] = ['label' => 'Trasfondos', 'url' => ['trasfondos/listatrasfondos']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="idiomastrasfondos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Añadir idioma a los trasfondos', ['create'], ['class' => 'btn btn-default']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            
            ['class' => 'yii\grid\ActionColumn'],
            [
                'attribute' => 'id_trasfondo',
                'label' => 'Trasfondo',
                'value' => 'trasfondo.nombre'
            ],
            'idioma',

            
        ],
    ]); ?>


</div>
