<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Idiomastrasfondos */

$this->title = 'Modificar idioma de un trasfondo: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Trasfondos', 'url' => ['trasfondos/listatrasfondos']];
$this->params['breadcrumbs'][] = ['label' => 'Idiomas de los trasfondos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Modificación';
?>
<div class="idiomastrasfondos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelTrasfondo' => $modelTrasfondo,
    ]) ?>

</div>
